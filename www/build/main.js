webpackJsonp([0],{

/***/ 115:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UnarchivePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_document_document__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__scan_folder_scan_folder__ = __webpack_require__(215);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the UnarchivePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var UnarchivePage = /** @class */ (function () {
    function UnarchivePage(navCtrl, navParams, DocumentProvider, lc) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.DocumentProvider = DocumentProvider;
        this.lc = lc;
        this.documents = [];
        this.page = 0;
        this.q = '';
        this.loader = this.lc.create();
        this.loader.present();
        this.getData();
    }
    UnarchivePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad UnarchivePage');
    };
    UnarchivePage.prototype.scanFolder = function (document) {
        // console.log(JSON.stringify(this.documents));
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__scan_folder_scan_folder__["a" /* ScanFolderPage */], {
            data: this.documents
        });
    };
    UnarchivePage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        this.DocumentProvider.getUnarchive(this.page, this.q).then(function (res) {
            Object.keys(res).forEach(function (value) {
                _this.documents.push(res[value]);
                ++_this.page;
            });
            _this.page++;
            infiniteScroll.complete();
        }, function (err) {
            console.log("Failure");
        });
    };
    UnarchivePage.prototype.getData = function () {
        var _this = this;
        this.DocumentProvider.getUnarchive(this.page, this.q).then(function (res) {
            Object.keys(res).forEach(function (value) {
                _this.documents.push(res[value]);
                ++_this.page;
            });
            _this.loader.dismiss();
        }, function (err) {
            console.log("Failure");
        });
    };
    UnarchivePage.prototype.back = function () {
        this.navCtrl.pop();
    };
    UnarchivePage.prototype.getItems = function (ev) {
        var _this = this;
        // Reset items back to all of the items
        // this.getData();
        this.documents = this.all;
        var res = this.documents;
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.documents = [];
            this.page = 0;
            this.DocumentProvider.getUnarchive(this.page, val).then(function (res) {
                Object.keys(res).forEach(function (value) {
                    _this.documents.push(res[value]);
                    ++_this.page;
                });
                // this.all = res;
                _this.q = val;
                _this.loader.dismiss();
            }, function (err) {
                console.log("Failure");
            });
            /*this.documents = this.documents.filter((item) => {
              if(item.indeks !=null && item.indeks.toLowerCase().indexOf(val.toLowerCase()) > -1){
                return true;
               }else if(item.no !=null && item.no.toLowerCase().indexOf(val.toLowerCase()) > -1){
                 return true;
               }else if(item.uraian !=null && item.uraian.toLowerCase().indexOf(val.toLowerCase()) > -1){
                 return true;
               }
               console.log(item.uraian);
               console.log(item.uraian.toLowerCase().indexOf(val.toLowerCase()));
              //  typeof item.no=== 'string'&&typeof item.uraian=== 'string'){
              //   return ( || item.uraian.toLowerCase().indexOf(val.toLowerCase()) > -1||item.indeks.toLowerCase().indexOf(val.toLowerCase()) > -1);
              // }// return false;
              // return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
              return false;
            })*/
            // this.folders = matches;
        }
        else {
            this.documents = [];
            this.page = 0;
            this.q = '';
            this.getData();
        }
    };
    UnarchivePage.prototype.reset = function () {
        this.getData();
    };
    UnarchivePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-unarchive',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\unarchive\unarchive.html"*/'<ion-content>\n\n\n\n  <div class="body-wrapper menu-wrapper">\n\n\n\n	<div class="content">\n\n		\n\n		<div class="box-view" data-aos="fade-up" data-aos-duration="500">\n\n				<button class="back__button" ion-button block (click)="back()">\n\n					<img src="assets/img/left-arrow.png" alt="">\n\n				</button>\n\n			 <ion-searchbar type="text" placeholder="Search" (ionInput)="getItems($event)" (ionClear)="reset($event)" (ngModel)="searchKey"></ion-searchbar>\n\n		</div>\n\n\n\n		<div class="list-wrapper">\n\n			<ion-list>\n\n				<ion-item text-wrap *ngFor="let document of documents" >\n\n					<ion-label>\n\n						<div>\n\n							<h3>No : {{ (document.no==null) ? \'--\' : document.no}}</h3>\n\n							<p>Description : {{ document.uraian}}</p>\n\n						</div>\n\n					</ion-label>\n\n					<ion-checkbox [(ngModel)]="document.value"></ion-checkbox>\n\n				</ion-item>\n\n			</ion-list>\n\n		</div>\n\n\n\n	</div>\n\n\n\n  </div>\n\n\n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <ion-toolbar>\n\n    <button class="submit" (click)="scanFolder(document)">Lanjut Scan Folder</button>\n\n  </ion-toolbar>\n\n</ion-footer>\n\n'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\unarchive\unarchive.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__providers_document_document__["a" /* DocumentProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_document_document__["a" /* DocumentProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], UnarchivePage);
    return UnarchivePage;
}());

//# sourceMappingURL=unarchive.js.map

/***/ }),

/***/ 115:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FinishPeminjamanPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the FinishPeminjamanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FinishPeminjamanPage = /** @class */ (function () {
    function FinishPeminjamanPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    FinishPeminjamanPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FinishPeminjamanPage');
    };
    FinishPeminjamanPage.prototype.backToHome = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    FinishPeminjamanPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-finish-peminjaman',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\finish-peminjaman\finish-peminjaman.html"*/'\n\n	<div class="confirmation-page">\n\n\n\n      <div class="upper">\n\n        \n\n        <div class="big-card" data-aos="fade-up" data-aos-duration="1500">\n\n          \n\n          <img src="assets/img/check-logo.png" alt="">\n\n  \n\n          <div class="big-card-title">\n\n            <span>Great !</span>\n\n            <br>\n\n            Peminjaman akan segera diproses\n\n          </div>\n\n  \n\n        </div>\n\n  \n\n      </div>\n\n  \n\n      <div class="under">\n\n        <button class="submit" (click)="backToHome()">Selesai</button>\n\n      </div>\n\n  \n\n    </div>'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\finish-peminjaman\finish-peminjaman.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], FinishPeminjamanPage);
    return FinishPeminjamanPage;
}());

//# sourceMappingURL=finish-peminjaman.js.map

/***/ }),

/***/ 116:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers__ = __webpack_require__(323);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, user, toastCtrl, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.user = user;
        this.toastCtrl = toastCtrl;
        this.storage = storage;
        this.account = {};
    }
    LoginPage.prototype.login = function () {
        var _this = this;
        this.user.login(this.account).then(function (res) {
            _this.storage.set('session', res);
            // this.storage.set('user', res.user);
            var toast = _this.toastCtrl.create({
                message: "Selamat Datang",
                duration: 3000,
                position: 'top'
            });
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
        }, function (err) {
            var toast = _this.toastCtrl.create({
                message: "Invalid Username Or Password",
                duration: 3000,
                position: 'top'
            });
            toast.present();
        });
    };
    LoginPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad LoginPage');
        this.storage.get('session').then(function (val) {
            if (val != null) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
            }
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\login\login.html"*/'<ion-content>\n	<div class="body-wrapper is-white">\n		\n		<div class="bg-shape-gradient" data-aos="fade-down" data-aos-duration="1500">\n			<img src="assets/img/gradient-bg.png" alt="">\n		</div>\n\n		<div class="content">\n			\n			<div class="header-text" data-aos="fade-down" data-aos-duration="1500">\n				<img src="assets/img/xpert-logo.png" alt="" class="logo">\n			</div>\n\n			<div class="form-wrapper" data-aos="fade-down" data-aos-duration="1500">\n				<div class="title">LOG <span>IN</span></div>\n\n				<form class="form">\n\n					<fieldset class="form-group">\n						<ion-input type="text" placeholder="Username" name="username" [(ngModel)]="account.username"></ion-input>\n						<span class="icon"><i class="fas fa-user"></i></span>\n					</fieldset>\n					<fieldset class="form-group">\n						<ion-input type="password" placeholder="Password" name="password" [(ngModel)]="account.password"></ion-input>\n						<span class="icon"><i class="fas fa-key"></i></span>\n					</fieldset>\n					<!-- <fieldset class="form-group wrap">\n							<div class="checkbox-wrapper">\n								<input type="checkbox">\n								<span class="checkbox-caption">Remember <span class="is-bold">me</span></span>\n								<a href="#">forgot <span class="is-bold">Password</span></a>\n							</div>\n					</fieldset> -->\n					<fieldset class="form-group is-centerize">\n						<button class="submit" (click)="login()">Login</button>\n					</fieldset>\n				</form>\n			</div>\n\n			<!-- <div class="footer is-transparent">\n				<p>Don\'t have an account? <a href="#">Sign Up</a></p>\n			</div> -->\n\n		</div>\n\n		<div class="bg-shape-half" data-aos="fade-up" data-aos-duration="1500">\n			<img src="assets/img/half-bg.png" alt="">\n		</div>\n\n	</div>\n</ion-content>'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\login\login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 117:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LemariBoxPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__list_folder_by_box_list_folder_by_box__ = __webpack_require__(235);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the LemariBoxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LemariBoxPage = /** @class */ (function () {
    function LemariBoxPage(navCtrl, navParams, BoxProvider, lc) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.BoxProvider = BoxProvider;
        this.lc = lc;
        this.lemari = this.navParams.get('lemari');
        this.loader = this.lc.create();
        this.loader.present();
        this.getData();
    }
    LemariBoxPage.prototype.next = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__list_folder_by_box_list_folder_by_box__["a" /* ListFolderByBoxPage */], {
            lemari: id
        });
    };
    LemariBoxPage.prototype.getData = function () {
        var _this = this;
        console.log(this.lemari);
        this.BoxProvider.getBoxNew(this.lemari).then(function (res) {
            _this.boxes = res;
            _this.loader.dismiss();
        }, function (err) {
            console.log(JSON.stringify(err));
            console.log("Failure");
        });
    };
    LemariBoxPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LemariBoxPage');
    };
    LemariBoxPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    LemariBoxPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-lemari-box',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\lemari-box\lemari-box.html"*/'<ion-content>\n\n\n\n  <div class="body-wrapper menu-wrapper">\n\n\n\n	<div class="content">\n\n		\n\n		<div class="box-view" data-aos="fade-up" data-aos-duration="500">\n\n				<button class="back__button" ion-button block (click)="back()">\n\n					<img src="assets/img/left-arrow.png" alt="">\n\n				</button>\n\n				<ion-searchbar type="text" placeholder="Search" (ionInput)="getItems($event)" (ionClear)="reset($event)" (ngModel)="searchKey"></ion-searchbar>\n\n		</div>\n\n\n\n		<div class="list-wrapper">\n\n			<ion-list>\n\n				<ion-item *ngFor="let box of boxes" (click) ="next(box.id)" >\n\n					<ion-label item-end class="list-box">\n\n                        <img src="assets/img/box-logo.png" class="box-img">\n\n                        <div>\n\n\n\n                            <h3>Kode : {{box.code}}</h3>\n\n                            <p>Record Center : {{ box.record_center}}</p>\n\n                        </div>\n\n                    </ion-label>\n\n				</ion-item>\n\n			</ion-list>\n\n		</div>\n\n<!-- \n\n		<div class="footer">\n\n			<button class="submit" (click)="scanFolder(document)">Lanjut</button>\n\n		</div> -->\n\n\n\n	</div>\n\n\n\n</div>\n\n<!-- <ion-fab right bottom>\n\n	<button ion-fab color="blue" (click)="scan()"><ion-icon name="camera"></ion-icon></button>\n\n</ion-fab> -->\n\n</ion-content>\n\n'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\lemari-box\lemari-box.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__providers_box_box__["a" /* BoxProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_box_box__["a" /* BoxProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], LemariBoxPage);
    return LemariBoxPage;
}());

//# sourceMappingURL=lemari-box.js.map

/***/ }),

/***/ 128:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 128;

/***/ }),

/***/ 16:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__unarchive_unarchive__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__assign_folder_assign_folder__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__assign_box_assign_box__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__dokumen_dokumen__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__berkas_berkas__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pemberkasan_pemberkasan__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__peminjaman_peminjaman__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__login_login__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__lemari_box_lemari_box__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__search_by_scan_search_by_scan__ = __webpack_require__(237);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};














var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, storage, navParams, barcode) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.navParams = navParams;
        this.barcode = barcode;
        this.storage.get('session').then(function (val) {
            if (val == null) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__login_login__["a" /* LoginPage */]);
            }
            else {
                console.log(val);
                _this.user = val.user;
            }
        });
    }
    HomePage.prototype.encodeData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.barcode.encode(this.barcode.Encode.TEXT_TYPE, this.dataToEncode)];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.scanBarcode = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var options, _a, error_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        options = {
                            prompt: 'Point your camera at a barcode',
                            torchOn: false
                        };
                        _a = this;
                        return [4 /*yield*/, this.barcode.scan(options).then(function (res) {
                                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_12__lemari_box_lemari_box__["a" /* LemariBoxPage */], {
                                    lemari: res.text
                                });
                                return res;
                            }, function (err) {
                                return err;
                            })];
                    case 1:
                        _a.result = _b.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_2 = _b.sent();
                        console.error(error_2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.lemari = function () {
        this.scanBarcode();
    };
    HomePage.prototype.search = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__search_by_scan_search_by_scan__["a" /* SearchByScanPage */]);
    };
    HomePage.prototype.entry = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__unarchive_unarchive__["a" /* UnarchivePage */]);
    };
    HomePage.prototype.asignfolder = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__assign_folder_assign_folder__["a" /* AssignFolderPage */]);
    };
    HomePage.prototype.asignbox = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__assign_box_assign_box__["a" /* AssignBoxPage */]);
    };
    HomePage.prototype.pemberkasan = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pemberkasan_pemberkasan__["a" /* PemberkasanPage */]);
    };
    HomePage.prototype.dokumen = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__dokumen_dokumen__["a" /* DokumenPage */]);
    };
    HomePage.prototype.berkas = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__berkas_berkas__["a" /* BerkasPage */]);
    };
    HomePage.prototype.peminjaman = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__peminjaman_peminjaman__["a" /* PeminjamanPage */]);
    };
    HomePage.prototype.searchfolder = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__search_folder_search_folder__["a" /* SearchFolderPage */]);
    };
    HomePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HomePage');
    };
    HomePage.prototype.logout = function () {
        this.storage.clear();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__login_login__["a" /* LoginPage */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\home\home.html"*/'<ion-content>\n\n\n\n	<div class="body-wrapper menu-wrapper">\n\n		\n\n		<div class="bg-shape-yellow" data-aos="fade-down" data-aos-duration="1500">\n\n			<img src="assets/img/yellow-bg.png" alt="">\n\n		</div>\n\n\n\n		<div class="content">\n\n			<button ion-button color="light" clear class="logoutIcon"  (click)="logout()" > \n\n				<ion-icon name="exit"></ion-icon>Logout\n\n			</button>\n\n			<div class="header-text" data-aos="fade-down" data-aos-duration="1500">\n\n				\n\n				<span>Selamat Datang {{user?.name}}</span>\n\n\n\n				<br>\n\n\n\n				<span class="is-bold">Apa yang ingin anda lakukan hari ini?</span>\n\n				\n\n			</div>\n\n\n\n			<div class="card-wrapper">\n\n				<div class="menu-card" (click)="search()" data-aos="zoom-in-up" data-aos-duration="1000" *ngIf="(user?.id_role==2||user?.id_role==3)">\n\n						<img src="assets/img/search.png" alt="" class="logo-icon">\n\n						<div class="title-card is-bold">\n\n							Search By Scan\n\n						</div>\n\n					</div>\n\n				<div class="menu-card" (click)="entry()" data-aos="zoom-in-up" data-aos-duration="1000" *ngIf="(user?.id_role==2||user?.id_role==3)">\n\n					<img src="assets/img/entry-logo.png" alt="" class="logo-icon">\n\n					<div class="title-card is-bold">\n\n						Entry\n\n					</div>\n\n				</div>\n\n				<div class="menu-card" (click)="asignfolder()" data-aos="zoom-in-up" data-aos-duration="1000" *ngIf="(user?.id_role==2||user?.id_role==3)">\n\n					<img src="assets/img/folder-logo.png" alt="" class="logo-icon">\n\n					<div class="title-card is-bold">\n\n						Assign Folder\n\n					</div>\n\n				</div>\n\n				<div class="menu-card" (click)="asignbox()" data-aos="zoom-in-up" data-aos-duration="1000" *ngIf="(user?.id_role==2||user?.id_role==3)">\n\n					<img src="assets/img/box-logo.png" alt="" class="logo-icon">\n\n					<div class="title-card is-bold">\n\n						Assign Box\n\n					</div>\n\n				</div>\n\n				<div class="menu-card" (click)="pemberkasan()" data-aos="zoom-in-up" data-aos-duration="1000" *ngIf="(user?.id_role==2||user?.id_role==3)">\n\n					<img src="assets/img/pemberkasan.png" alt="" class="logo-icon">\n\n					<div class="title-card is-bold">\n\n						Pemberkasan\n\n					</div>\n\n				</div>\n\n				<div class="menu-card" (click)="dokumen()" data-aos="zoom-in-up" data-aos-duration="1000" *ngIf="(user?.id_role==2||user?.id_role==3)">\n\n					<img src="assets/img/file.png" alt="" class="logo-icon">\n\n					<div class="title-card is-bold">\n\n						Dokumen\n\n					</div>\n\n				</div>\n\n				<div class="menu-card" (click)="berkas()" data-aos="zoom-in-up" data-aos-duration="1000" *ngIf="(user?.id_role==2||user?.id_role==3)">\n\n					<img src="assets/img/dokumen.png" alt="" class="logo-icon">\n\n					<div class="title-card is-bold">\n\n						Berkas\n\n					</div>\n\n				</div>\n\n				<div class="menu-card" (click)="peminjaman()" data-aos="zoom-in-up" data-aos-duration="1000" >\n\n					<img src="assets/img/peminjaman.png" alt="" class="logo-icon">\n\n					<div class="title-card is-bold">\n\n						Peminjaman\n\n					</div>\n\n				</div>\n\n				<div class="menu-card" (click)="lemari()" data-aos="zoom-in-up" data-aos-duration="1000" >\n\n					<img src="assets/img/lemari.png" alt="" class="logo-icon">\n\n					<div class="title-card is-bold">\n\n						Lemari\n\n					</div>\n\n				</div>\n\n			</div>\n\n\n\n		</div>\n\n\n\n		<div class="bg-shape-green" data-aos="fade-left" data-aos-duration="1500">\n\n			<img src="assets/img/green-bg.png" alt="">\n\n		</div>\n\n\n\n	</div>\n\n\n\n</ion-content>'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__["a" /* BarcodeScanner */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 169:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 169;

/***/ }),

/***/ 21:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DocumentProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_global__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DocumentProvider = /** @class */ (function () {
    function DocumentProvider(client, http, storage) {
        this.client = client;
        this.http = http;
        this.storage = storage;
        this.apiUrl = __WEBPACK_IMPORTED_MODULE_3__app_global__["a" /* GlobalVariable */].BASE_API_URL;
        this.rows = [];
        this.token = '';
        console.log('Hello DocumentProvider Provider');
    }
    DocumentProvider.prototype.getDocument = function (q) {
        var _this = this;
        if (q === void 0) { q = null; }
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                // headers.append('', );
                headers.append('Access-Control-Allow-Origin', '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
                _this.http.get(_this.apiUrl + 'dokumen/user/' + val.user.id_user, { headers: headers })
                    .subscribe(function (res) {
                    resolve(res);
                }, function (err) {
                    console.log(err);
                    reject(err);
                });
            });
        });
    };
    DocumentProvider.prototype.getDokumenByFolder = function (folder, q) {
        var _this = this;
        if (q === void 0) { q = null; }
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                // headers.append('', );
                headers.append('Access-Control-Allow-Origin', '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
                _this.http.get(_this.apiUrl + 'dokumen/id_folder/' + folder, { headers: headers })
                    .subscribe(function (res) {
                    resolve(res);
                }, function (err) {
                    console.log(err);
                    reject(err);
                });
            });
        });
    };
    DocumentProvider.prototype.getUnarchive = function (page, q) {
        var _this = this;
        if (q === void 0) { q = ''; }
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                // headers.append('', );
                headers.append('Access-Control-Allow-Origin', '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
                var url = _this.apiUrl + 'newDocument/user/' + val.user.id_user + '/page/' + page;
                if (q != '') {
                    url += '/no/' + q;
                    url += '/uraian/' + q;
                }
                _this.http.get(url, { headers: headers })
                    .subscribe(function (res) {
                    resolve(res);
                }, function (err) {
                    console.log(err);
                    reject(err);
                });
            });
        });
    };
    DocumentProvider.prototype.getDocumentAll = function () {
        var _this = this;
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                // headers.append('', );
                headers.append('Access-Control-Allow-Origin', '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
                _this.http.get(_this.apiUrl + 'dokumen/', { headers: headers })
                    .subscribe(function (res) {
                    resolve(res);
                }, function (err) {
                    console.log(err);
                    reject(err);
                });
            });
        });
    };
    DocumentProvider.prototype.postEntry = function (data) {
        var _this = this;
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                headers.append('Access-Control-Allow-Origin', '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
                var listIdDokumen = [];
                // console.log(data);
                data.documents.forEach(function (item) {
                    if (item.value == true) {
                        listIdDokumen.push(item.id);
                    }
                });
                // console.log(JSON.stringify(data.documents)) ;
                var data_send = { id_dokumen: listIdDokumen, id_folder: data.folder.text, id_box: data.box.text };
                // console.log(JSON.stringify(listIdDokumen)); 
                _this.http.post(_this.apiUrl + 'dokumen', JSON.stringify(data_send), { headers: headers })
                    .subscribe(function (res) {
                    // console.log(JSON.stringify(res));
                    // console.log(JSON.stringify(this.rows));
                    resolve(res);
                }, function (err) {
                    // console.log(JSON.stringify(err));
                    reject(err);
                });
            });
        });
    };
    DocumentProvider.prototype.getAllDokumen = function (page, q) {
        var _this = this;
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                // headers.append('', );
                headers.append('Access-Control-Allow-Origin', '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
                var url = _this.apiUrl + 'dokumen/page/' + page + '/user/' + val.user.id_user;
                if (q != '') {
                    url += '/no/' + q;
                    url += '/uraian/' + q;
                    url += '/id_folder/' + q;
                    url += '/tahun/' + q;
                    url += '/id_berkas/' + q;
                    url += '/name/' + q;
                    url += '/id_box/' + q;
                    url += '/id_cabinet/' + q;
                }
                _this.http.get(url, { headers: headers })
                    .subscribe(function (res) {
                    // console.log(res);
                    resolve(res);
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    DocumentProvider.prototype.getListDokumen = function (page, q) {
        var _this = this;
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                // headers.append('', );
                headers.append('Access-Control-Allow-Origin', '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
                var url = _this.apiUrl + 'dokumen/page/' + page + '/user/' + val.user.id_user;
                if (q != '') {
                    url += '/no/' + q;
                    url += '/uraian/' + q;
                    url += '/id_folder/' + q;
                    url += '/tahun/' + q;
                    url += '/id_berkas/' + q;
                    url += '/name/' + q;
                    url += '/id_box/' + q;
                    url += '/id_cabinet/' + q;
                }
                _this.http.get(url, { headers: headers })
                    .subscribe(function (res) {
                    // console.log(res);
                    resolve(res);
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    DocumentProvider.prototype.getListDokumenByBerkas = function (id) {
        var _this = this;
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                // let token = this.storage.get('token');
                //let token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6IndpZG9kbyIsImlkX3VzZXIiOiIzNSIsIm5hbWUiOiJXaWRvZG8gUmlzbmFudHlvIiwiaWRfcm9sZSI6IjMiLCJpZF9kaXZpc2lvbiI6IjEiLCJpZF9yYyI6IjEiLCJpYXQiOjE1NDk0MzUzMzcsImV4cCI6MTU1MDA0MDEzN30.aHF8R3lfyqgnffsNWtKEn7HxVoOvL0fQpthQABy9A6k';
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                // headers.append('', );
                headers.append('Access-Control-Allow-Origin', '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
                _this.http.get(_this.apiUrl + 'dokumen/id_berkas/' + id, { headers: headers })
                    .subscribe(function (res) {
                    console.log(res);
                    resolve(res);
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    DocumentProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClientModule */],
            __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
    ], DocumentProvider);
    return DocumentProvider;
}());

//# sourceMappingURL=document.js.map

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Asignfolder2Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__asignfolder3_asignfolder3__ = __webpack_require__(213);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the Asignfolder2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Asignfolder2Page = /** @class */ (function () {
    function Asignfolder2Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    Asignfolder2Page.prototype.asignfolder3 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__asignfolder3_asignfolder3__["a" /* Asignfolder3Page */]);
    };
    Asignfolder2Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Asignfolder2Page');
    };
    Asignfolder2Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-asignfolder2',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\asignfolder2\asignfolder2.html"*/'<!--\n  Generated template for the Asignfolder2Page page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n	<div class="scanner-page" data-aos="fade-up" data-aos-duration="500">\n		\n		<div class="scanner-header" data-aos="fade-down" data-aos-duration="1500">\n			<div class="upper">\n				<a class="button-back">\n					<span class="icon"><i class="fas fa-arrow-left"></i></span>\n					Back\n				</a>\n				<div class="button-group">\n					<a href="#">\n						<span class="icon"><i class="fas fa-image"></i></span>\n						<span class="icon"><i class="fas fa-bolt"></i></span>\n					</a>\n				</div>\n			</div>\n			<div class="under">\n				<p>Point your camera towards the QR code <br> to scan</p>\n			</div>\n		</div>\n\n		<div class="scanner-footer">\n			<div class="upper">\n				Scanning <span class="is-bold">Folder</span>\n			</div>\n			<div class="under">\n				<button class="submit" (click)="asignfolder3()">Selanjutnya</button>\n			</div>\n		</div>\n\n	</div>\n\n'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\asignfolder2\asignfolder2.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], Asignfolder2Page);
    return Asignfolder2Page;
}());

//# sourceMappingURL=asignfolder2.js.map

/***/ }),

/***/ 213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Asignfolder3Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the Asignfolder3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Asignfolder3Page = /** @class */ (function () {
    function Asignfolder3Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    Asignfolder3Page.prototype.homesweethome = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    Asignfolder3Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Asignfolder3Page');
    };
    Asignfolder3Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-asignfolder3',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\asignfolder3\asignfolder3.html"*/'\n	<div class="confirmation-page">\n\n		<div class="upper">\n			\n			<div class="big-card" data-aos="fade-up" data-aos-duration="1500">\n				\n				<img src="assets/img/folder-logo.png" alt="">\n				<img src="assets/img/check-logo.png" alt="" class="check-icon">\n\n				<div class="big-card-title">\n					<span>Great !</span>\n					<br>\n					Folder telah ditambahkan\n				</div>\n\n			</div>\n\n		</div>\n\n		<div class="under">\n			<button class="submit" (click)="homesweethome()">Selesai</button>\n		</div>\n\n	</div>'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\asignfolder3\asignfolder3.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], Asignfolder3Page);
    return Asignfolder3Page;
}());

//# sourceMappingURL=asignfolder3.js.map

/***/ }),

/***/ 215:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScanFolderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__scan_box_scan_box__ = __webpack_require__(216);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




/**
 * Generated class for the ScanFolderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ScanFolderPage = /** @class */ (function () {
    function ScanFolderPage(navCtrl, navParams, barcode) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.barcode = barcode;
        this.data = { 'documents': {}, 'folder': this.folder };
        this.isCapture = true;
        this.data.documents = this.navParams.get('data');
        // console.log(JSON.stringify(this.data));
    }
    ScanFolderPage.prototype.encodeData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.barcode.encode(this.barcode.Encode.TEXT_TYPE, this.dataToEncode)];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ScanFolderPage.prototype.scanBarcode = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var options, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        options = {
                            prompt: 'Point your camera at a barcode',
                            torchOn: false
                        };
                        return [4 /*yield*/, this.barcode.scan(options).then(function (res) {
                                // this.folder = res;
                                _this.data.folder = res;
                                _this.isCapture = true;
                                return res;
                            }, function (err) {
                                console.log('Gagal Scan');
                                return err;
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_2 = _a.sent();
                        console.error(error_2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ScanFolderPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ScanFolderPage');
    };
    ScanFolderPage.prototype.scanBox = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__scan_box_scan_box__["a" /* ScanBoxPage */], {
            data: this.data
        });
    };
    ScanFolderPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    ScanFolderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-scan-folder',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\scan-folder\scan-folder.html"*/'<div class="scanner-page" data-aos="fade-up" data-aos-duration="500">\n\n		\n\n	<div class="scanner-header" data-aos="fade-down" data-aos-duration="1500">\n\n		<div class="upper">\n\n			<!-- <a class="button-back">\n\n				<span class="icon"><i class="fas fa-arrow-left"></i></span>\n\n				Back\n\n			</a>\n\n			<div class="button-group">\n\n				<a href="#">\n\n					<span class="icon"><i class="fas fa-image"></i></span>\n\n					<span class="icon"><i class="fas fa-bolt"></i></span>\n\n				</a>\n\n			</div>\n\n		</div>\n\n		<div class="under">\n\n			<p>Point your camera towards the QR code <br> to scan</p>\n\n		</div>\n\n	</div> -->\n\n	\n\n	<ion-row>\n\n			<button class="back__button" ion-button block (click)="back()">\n\n				<img src="assets/img/left-arrow.png" alt="">\n\n			</button>\n\n		</ion-row>\n\n		<div class="under">\n\n			<ion-row>\n\n				<p>Point your camera towards the QR code <br> to scan</p>\n\n			</ion-row>\n\n		</div>\n\n		</div>\n\n	</div>\n\n\n\n	<ion-content padding>\n\n		<ion-card *ngIf="result">\n\n		  <ion-card-content>\n\n			<p>ID Folder : {{result.text}}</p>\n\n		  </ion-card-content>\n\n		</ion-card>\n\n	  \n\n		<ion-row>\n\n		  <button ion-button block (click)="scanBarcode()">SCAN FOLDER</button>\n\n		</ion-row> \n\n		\n\n</ion-content>\n\n\n\n	<div class="scanner-footer">\n\n		<div class="under" *ngIf="isCapture">\n\n			<button class="submit" (click)="scanBox(document)">Lanjut Scan Box</button>\n\n		</div>\n\n	</div>\n\n\n\n</div>\n\n\n\n'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\scan-folder\scan-folder.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__["a" /* BarcodeScanner */]])
    ], ScanFolderPage);
    return ScanFolderPage;
}());

//# sourceMappingURL=scan-folder.js.map

/***/ }),

/***/ 216:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScanBoxPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__entry3new_entry3new__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_document_document__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__unarchive_unarchive__ = __webpack_require__(114);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






/**
 * Generated class for the ScanBoxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ScanBoxPage = /** @class */ (function () {
    function ScanBoxPage(navCtrl, navParams, barcode, lc, DocumentProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.barcode = barcode;
        this.lc = lc;
        this.DocumentProvider = DocumentProvider;
        this.data = { 'documents': {}, 'folder': {}, 'box': this.box };
        this.isCapture = true;
        var data = this.navParams.get('data');
        if (typeof data != 'undefined') {
            this.data.documents = data.documents;
            this.data.folder = data.folder;
            // console.log(JSON.stringify(this.data));
        }
        else {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__unarchive_unarchive__["a" /* UnarchivePage */]);
        }
    }
    ScanBoxPage.prototype.encodeData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.barcode.encode(this.barcode.Encode.TEXT_TYPE, this.dataToEncode)];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ScanBoxPage.prototype.scanBarcode = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var options, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        options = {
                            prompt: 'Point your camera at a barcode',
                            torchOn: false
                        };
                        return [4 /*yield*/, this.barcode.scan(options).then(function (res) {
                                _this.data.box = res;
                                var loading = _this.lc.create();
                                loading.present();
                                console.log(_this.data);
                                _this.DocumentProvider.postEntry(_this.data).then(function (result) {
                                    loading.dismiss();
                                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__entry3new_entry3new__["a" /* Entry3newPage */]);
                                }, function (error) {
                                    console.log(JSON.stringify(_this.data));
                                });
                                return res;
                            }, function (err) {
                                var loading = _this.lc.create();
                                loading.present();
                                _this.DocumentProvider.postEntry(_this.data).then(function (result) {
                                    loading.dismiss();
                                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__entry3new_entry3new__["a" /* Entry3newPage */]);
                                }, function (error) {
                                    console.log(error);
                                });
                                return err;
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_2 = _a.sent();
                        // this.data.box.text = '1';
                        this.isCapture = true;
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ScanBoxPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ScanBoxPage');
    };
    ScanBoxPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    ScanBoxPage.prototype.selesai2 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__entry3new_entry3new__["a" /* Entry3newPage */]);
    };
    ScanBoxPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-scan-box',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\scan-box\scan-box.html"*/'<div class="scanner-page" data-aos="fade-up" data-aos-duration="500">\n\n		\n\n	<div class="scanner-header" data-aos="fade-down" data-aos-duration="1500">\n\n		<div class="upper">\n\n				<ion-row>\n\n				<button class="back__button" ion-button block (click)="back()">\n\n					<img src="assets/img/left-arrow.png" alt="">\n\n				</button>\n\n			</ion-row>\n\n			<div class="under">\n\n				<ion-row>\n\n					<p>Point your camera towards the QR code <br> to scan</p>\n\n				</ion-row>\n\n			</div>\n\n			</div>\n\n		</div>\n\n	\n\n\n\n	<ion-content padding>\n\n		<ion-card *ngIf="result">\n\n		  <ion-card-content>\n\n			<p>Text: {{result.text}}</p>\n\n			<p>Format: {{result.format}}</p>\n\n			<p>Cancelled: {{result.cancelled}}</p>\n\n		  </ion-card-content>\n\n		</ion-card>\n\n	  \n\n		<ion-row>\n\n		  <button ion-button block (click)="scanBarcode()">SCAN BOX</button>\n\n		</ion-row>\n\n		\n\n</ion-content>\n\n\n\n	<div class="scanner-footer">\n\n		<div class="under">\n\n			<button class="submit" (click)="selesai2()">Selesai</button>\n\n		</div>\n\n	</div>\n\n</div>'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\scan-box\scan-box.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_document_document__["a" /* DocumentProvider */]])
    ], ScanBoxPage);
    return ScanBoxPage;
}());

//# sourceMappingURL=scan-box.js.map

/***/ }),

/***/ 217:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AssignFolderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_folder_folder__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__scan_assign_folder_scan_assign_folder__ = __webpack_require__(218);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the AssignFolderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AssignFolderPage = /** @class */ (function () {
    function AssignFolderPage(navCtrl, navParams, FolderProvider, lc) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.FolderProvider = FolderProvider;
        this.lc = lc;
        this.folders = [];
        this.page = 0;
        this.q = '';
        this.loader = this.lc.create();
        this.loader.present();
        this.getData();
    }
    AssignFolderPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AssignFolderPage');
    };
    AssignFolderPage.prototype.getData = function () {
        var _this = this;
        this.FolderProvider.getFolder(this.page, this.q).then(function (res) {
            Object.keys(res).forEach(function (value) {
                _this.folders.push(res[value]);
                ++_this.page;
            });
            _this.all = res;
            _this.loader.dismiss();
        }, function (err) {
            console.log("Failure");
        });
    };
    AssignFolderPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        this.FolderProvider.getFolder(this.page, this.q).then(function (res) {
            Object.keys(res).forEach(function (value) {
                _this.folders.push(res[value]);
                ++_this.page;
            });
            // this.page++;
            infiniteScroll.complete();
        }, function (err) {
            console.log("Failure");
        });
    };
    AssignFolderPage.prototype.scanAssignFolder = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__scan_assign_folder_scan_assign_folder__["a" /* ScanAssignFolderPage */], {
            data: this.folders
        });
    };
    AssignFolderPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    AssignFolderPage.prototype.getItems = function (ev) {
        var _this = this;
        // Reset items back to all of the items
        // this.getData();
        // this.folders = this.all;
        var res = this.folders;
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.FolderProvider.getFolder(0, val).then(function (res) {
                _this.folders = [];
                _this.page = 0;
                Object.keys(res).forEach(function (value) {
                    _this.folders.push(res[value]);
                    ++_this.page;
                });
                // this.all = res;
                _this.q = val;
                _this.loader.dismiss();
            }, function (err) {
                console.log("Failure");
            });
            /*this.folders = this.folders.filter((item) => {
      
              return (item.code.toLowerCase().indexOf(val.toLowerCase()) > -1);
              // return false;
              // return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })*/
            // this.folders = matches;
        }
        else {
            this.page = 0;
            this.q = '';
        }
    };
    AssignFolderPage.prototype.reset = function () {
        this.getData();
    };
    AssignFolderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-assign-folder',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\assign-folder\assign-folder.html"*/'<ion-content>\n\n\n\n  <div class="body-wrapper menu-wrapper">\n\n  \n\n    <div class="content">\n\n      \n\n        <div class="box-view" data-aos="fade-up" data-aos-duration="500">\n\n            <button class="back__button" ion-button block (click)="back()">\n\n                <img src="assets/img/left-arrow.png" alt="">\n\n            </button>\n\n            <ion-searchbar type="text" placeholder="Search" (ionInput)="getItems($event)" (ionClear)="reset($event)" (ngModel)="searchKey"></ion-searchbar>\n\n            <!-- <div class="search-box">\n\n                \n\n                <span class="icon">\n\n                    <i class="fas fa-search"></i>\n\n                </span>\n\n            </div> -->\n\n        </div>\n\n  \n\n        <div class="list-wrapper">\n\n            <ion-list>\n\n                <ion-item *ngFor="let folder of folders" >\n\n                   <ion-checkbox [(ngModel)]="folder.value" item-start></ion-checkbox>\n\n                     \n\n                      <ion-label item-end class="list-folder">\n\n                        <img src="assets/img/folder-logo.png" class="folder-img">\n\n                        <div>\n\n                            \n\n                            <h3>Kode Folder : {{folder.code}}</h3>\n\n                            <p>Kode Box : {{ folder.box}}</p>\n\n                            <p>Record Center : {{ folder.record_center}}</p>\n\n                        </div>\n\n                    </ion-label>\n\n                </ion-item>\n\n            </ion-list>\n\n        </div>\n\n        <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n            <ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n        </ion-infinite-scroll>\n\n    </div>\n\n  \n\n</div>\n\n  \n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <ion-toolbar>\n\n    <button class="submit" (click)="scanAssignFolder(folder)">Lanjut</button>\n\n  </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\assign-folder\assign-folder.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__providers_folder_folder__["a" /* FolderProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_folder_folder__["a" /* FolderProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], AssignFolderPage);
    return AssignFolderPage;
}());

//# sourceMappingURL=assign-folder.js.map

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScanAssignFolderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__finish_assign_folder_finish_assign_folder__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__entry3new_entry3new__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_folder_folder__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






/**
 * Generated class for the ScanAssignFolderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ScanAssignFolderPage = /** @class */ (function () {
    function ScanAssignFolderPage(navCtrl, navParams, barcode, lc, FolderProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.barcode = barcode;
        this.lc = lc;
        this.FolderProvider = FolderProvider;
        this.folder = this.navParams.get('data');
    }
    ScanAssignFolderPage.prototype.encodeData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.barcode.encode(this.barcode.Encode.TEXT_TYPE, this.dataToEncode)];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ScanAssignFolderPage.prototype.scanBarcode = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var options, _a, error_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        options = {
                            prompt: 'Point your camera at a barcode',
                            torchOn: false
                        };
                        _a = this;
                        return [4 /*yield*/, this.barcode.scan(options).then(function (res) {
                                var loading = _this.lc.create();
                                var data = { 'folder': _this.folder, 'box': res.text };
                                loading.present();
                                _this.FolderProvider.postEntry(data).then(function (result) {
                                    loading.dismiss();
                                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__entry3new_entry3new__["a" /* Entry3newPage */]);
                                }, function (error) {
                                });
                                return res;
                            }, function (err) {
                                return err;
                            })];
                    case 1:
                        _a.result = _b.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_2 = _b.sent();
                        console.error(error_2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ScanAssignFolderPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ScanAssignFolderPage');
    };
    ScanAssignFolderPage.prototype.finish = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__finish_assign_folder_finish_assign_folder__["a" /* FinishAssignFolderPage */]);
    };
    ScanAssignFolderPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    ScanAssignFolderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-scan-assign-folder',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\scan-assign-folder\scan-assign-folder.html"*/'<div class="scanner-page" data-aos="fade-up" data-aos-duration="500">\n\n		\n\n	<div class="scanner-header" data-aos="fade-down" data-aos-duration="1500">\n\n		<div class="upper">\n\n			<ion-row>\n\n		  <button class="back__button" ion-button block (click)="back()">\n\n				<img src="assets/img/left-arrow.png" alt="">\n\n			</button>\n\n		</ion-row>\n\n		<div class="under">\n\n			<ion-row>\n\n				<p>Point your camera towards the QR code <br> to scan</p>\n\n			</ion-row>\n\n		</div>\n\n		</div>\n\n	</div>\n\n\n\n\n\n	<ion-content padding>\n\n		<ion-card *ngIf="result">\n\n		  <ion-card-content>\n\n			<p>Text: {{result.text}}</p>\n\n			<p>Format: {{result.format}}</p>\n\n			<p>Cancelled: {{result.cancelled}}</p>\n\n		  </ion-card-content>\n\n		</ion-card>\n\n	  \n\n		<ion-row>\n\n		  <button ion-button block (click)="scanBarcode()">SCAN BOX</button>\n\n		</ion-row>\n\n		\n\n</ion-content>\n\n\n\n	<div class="scanner-footer">\n\n		<div class="under">\n\n			<button class="submit" (click)="finish()">Selesai</button>\n\n		</div>\n\n	</div>\n\n</div>'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\scan-assign-folder\scan-assign-folder.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_folder_folder__["a" /* FolderProvider */]])
    ], ScanAssignFolderPage);
    return ScanAssignFolderPage;
}());

//# sourceMappingURL=scan-assign-folder.js.map

/***/ }),

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FinishAssignFolderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the FinishAssignFolderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FinishAssignFolderPage = /** @class */ (function () {
    function FinishAssignFolderPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    FinishAssignFolderPage.prototype.backToHome = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    FinishAssignFolderPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FinishAssignFolderPage');
    };
    FinishAssignFolderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-finish-assign-folder',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\finish-assign-folder\finish-assign-folder.html"*/'\n\n	<div class="confirmation-page">\n\n\n\n		<div class="upper">\n\n			\n\n			<div class="big-card" data-aos="fade-up" data-aos-duration="1500">\n\n				\n\n				<img src="assets/img/folder-logo.png" alt="">\n\n				<img src="assets/img/check-logo.png" alt="" class="check-icon">\n\n\n\n				<div class="big-card-title">\n\n					<span>Great !</span>\n\n					<br>\n\n					Folder telah ditambahkan\n\n				</div>\n\n\n\n			</div>\n\n\n\n		</div>\n\n\n\n		<div class="under">\n\n			<button class="submit" (click)="backToHome()">Selesai</button>\n\n		</div>\n\n\n\n	</div>'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\finish-assign-folder\finish-assign-folder.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], FinishAssignFolderPage);
    return FinishAssignFolderPage;
}());

//# sourceMappingURL=finish-assign-folder.js.map

/***/ }),

/***/ 220:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AssignBoxPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_box_box__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__scan_assign_lemari_scan_assign_lemari__ = __webpack_require__(221);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the AssignBoxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AssignBoxPage = /** @class */ (function () {
    function AssignBoxPage(navCtrl, navParams, BoxProvider, lc) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.BoxProvider = BoxProvider;
        this.lc = lc;
        this.boxes = [];
        this.page = 0;
        this.q = '';
        var loader = this.lc.create();
        loader.present();
        this.getData(loader);
    }
    AssignBoxPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AssignBoxPage');
    };
    AssignBoxPage.prototype.getData = function (lc) {
        var _this = this;
        this.BoxProvider.getBox(this.page, this.q).then(function (res) {
            Object.keys(res).forEach(function (value) {
                _this.boxes.push(res[value]);
                ++_this.page;
            });
            _this.all = res;
            // this.loader.dismiss();
            lc.dismiss();
        }, function (err) {
            console.log("Failure");
        });
    };
    AssignBoxPage.prototype.scanAssignBox = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__scan_assign_lemari_scan_assign_lemari__["a" /* ScanAssignLemariPage */], {
            data: this.boxes
        });
    };
    AssignBoxPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    AssignBoxPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        this.BoxProvider.getBox(this.page, this.q).then(function (res) {
            Object.keys(res).forEach(function (value) {
                _this.boxes.push(res[value]);
                ++_this.page;
            });
            // this.page++;
            infiniteScroll.complete();
        }, function (err) {
            console.log("Failure");
        });
    };
    AssignBoxPage.prototype.getItems = function (ev) {
        var _this = this;
        // Reset items back to all of the items
        // this.getData();
        // this.folders = this.all;
        var res = this.boxes;
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.BoxProvider.getBox(0, val).then(function (res) {
                _this.boxes = [];
                _this.page = 0;
                Object.keys(res).forEach(function (value) {
                    _this.boxes.push(res[value]);
                    ++_this.page;
                });
                // this.all = res;
                _this.q = val;
                _this.loader.dismiss();
            }, function (err) {
                console.log("Failure");
            });
            /*this.folders = this.folders.filter((item) => {
      
              return (item.code.toLowerCase().indexOf(val.toLowerCase()) > -1);
              // return false;
              // return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })*/
            // this.folders = matches;
        }
        else {
            this.page = 0;
            this.q = '';
        }
    };
    AssignBoxPage.prototype.reset = function () {
        this.getData(this.lc);
    };
    AssignBoxPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-assign-box',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\assign-box\assign-box.html"*/'<ion-content>\n\n\n\n  <div class="body-wrapper menu-wrapper">\n\n  \n\n    <div class="content">\n\n      \n\n        <div class="box-view" data-aos="fade-up" data-aos-duration="500">\n\n            <button class="back__button" ion-button block (click)="back()">\n\n                <img src="assets/img/left-arrow.png" alt="">\n\n            </button>\n\n            <ion-searchbar type="text" placeholder="Search" (ionInput)="getItems($event)" (ionClear)="reset($event)" (ngModel)="searchKey"></ion-searchbar>\n\n            </div>\n\n  \n\n        <div class="list-wrapper">\n\n            <ion-list>\n\n               \n\n                <ion-item *ngFor="let box of boxes" >\n\n                     <ion-checkbox [(ngModel)]="box.value" item-start></ion-checkbox>\n\n                    <ion-label item-end class="list-box">\n\n                        <img src="assets/img/box-logo.png" class="box-img">\n\n                        <div>\n\n\n\n                            <h3>{{box.code}}</h3>\n\n                            <p>{{ box.record_center}}</p>\n\n                        </div>\n\n                    </ion-label>\n\n                    \n\n                </ion-item>\n\n            </ion-list>\n\n        </div>\n\n         <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n            <ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n        </ion-infinite-scroll>\n\n    </div>\n\n  \n\n</div>\n\n  \n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <ion-toolbar>\n\n    <button class="submit" (click)="scanAssignBox(box)">Lanjut</button>\n\n  </ion-toolbar>\n\n</ion-footer>\n\n  '/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\assign-box\assign-box.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__providers_box_box__["a" /* BoxProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_box_box__["a" /* BoxProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], AssignBoxPage);
    return AssignBoxPage;
}());

//# sourceMappingURL=assign-box.js.map

/***/ }),

/***/ 221:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScanAssignLemariPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__finish_assign_box_finish_assign_box__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_box_box__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





/**
 * Generated class for the ScanAssignLemariPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ScanAssignLemariPage = /** @class */ (function () {
    function ScanAssignLemariPage(navCtrl, navParams, barcode, alertCtrl, boxProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.barcode = barcode;
        this.alertCtrl = alertCtrl;
        this.boxProvider = boxProvider;
        this.boxes = this.navParams.get('data');
    }
    ScanAssignLemariPage.prototype.encodeData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.barcode.encode(this.barcode.Encode.TEXT_TYPE, this.dataToEncode)];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ScanAssignLemariPage.prototype.scanBarcode = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var options, _a, error_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        options = {
                            prompt: 'Point your camera at a barcode',
                            torchOn: false
                        };
                        _a = this;
                        return [4 /*yield*/, this.barcode.scan(options).then(function (res) {
                                _this.presentPrompt(res);
                                return res;
                            }, function (err) {
                                // this.presentPrompt(err);
                                // console.log(err);
                                return err;
                            })];
                    case 1:
                        _a.result = _b.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_2 = _b.sent();
                        console.error(error_2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ScanAssignLemariPage.prototype.presentPrompt = function (res) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Masukkan Level Box',
            inputs: [
                {
                    name: 'level',
                    placeholder: 'Level'
                }
            ],
            buttons: [
                {
                    text: 'Submit',
                    handler: function (_data) {
                        // console.log(_data);
                        _this.boxProvider.postEntry(_this.boxes, res.text, _data.level).then(function (result) {
                            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__finish_assign_box_finish_assign_box__["a" /* FinishAssignBoxPage */]);
                        }, function (err) {
                            // console.log(JSON.stringify(err));
                            return err;
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    ScanAssignLemariPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ScanAssignLemariPage');
    };
    ScanAssignLemariPage.prototype.finish = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__finish_assign_box_finish_assign_box__["a" /* FinishAssignBoxPage */]);
    };
    ScanAssignLemariPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    ScanAssignLemariPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-scan-assign-lemari',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\scan-assign-lemari\scan-assign-lemari.html"*/'<div class="scanner-page" data-aos="fade-up" data-aos-duration="500">\n\n		\n\n	<div class="scanner-header" data-aos="fade-down" data-aos-duration="1500">\n\n		<div class="upper">\n\n			<ion-row>\n\n		  <button class="back__button" ion-button block (click)="back()">\n\n				<img src="assets/img/left-arrow.png" alt="">\n\n			</button>\n\n		</ion-row>\n\n		<div class="under">\n\n			<ion-row>\n\n				<p>Point your camera towards the QR code <br> to scan</p>\n\n			</ion-row>\n\n		</div>\n\n		</div>\n\n	</div>\n\n\n\n	<ion-content padding>\n\n		\n\n		<ion-card *ngIf="result">\n\n		  <ion-card-content>\n\n			<p>Text: {{result.text}}</p>\n\n			<p>Format: {{result.format}}</p>\n\n			<p>Cancelled: {{result.cancelled}}</p>\n\n		  </ion-card-content>\n\n		</ion-card>\n\n	  \n\n		<ion-row>\n\n		  <button ion-button block (click)="scanBarcode()">SCAN LEMARI</button>\n\n		</ion-row>\n\n		\n\n</ion-content>\n\n\n\n	<div class="scanner-footer">\n\n		<div class="under">\n\n			<button class="submit" (click)="finish()">Selesai</button>\n\n		</div>\n\n	</div>\n\n</div>'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\scan-assign-lemari\scan-assign-lemari.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__["a" /* BarcodeScanner */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__providers_box_box__["a" /* BoxProvider */]])
    ], ScanAssignLemariPage);
    return ScanAssignLemariPage;
}());

//# sourceMappingURL=scan-assign-lemari.js.map

/***/ }),

/***/ 222:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FinishAssignBoxPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the FinishAssignBoxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FinishAssignBoxPage = /** @class */ (function () {
    function FinishAssignBoxPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    FinishAssignBoxPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FinishAssignBoxPage');
    };
    FinishAssignBoxPage.prototype.backToHome = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    FinishAssignBoxPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-finish-assign-box',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\finish-assign-box\finish-assign-box.html"*/'\n\n	<div class="confirmation-page">\n\n\n\n		<div class="upper">\n\n			\n\n			<div class="big-card" data-aos="fade-up" data-aos-duration="1500">\n\n				\n\n				<img src="assets/img/box-logo.png" alt="">\n\n				<img src="assets/img/check-logo.png" alt="" class="check-icon">\n\n\n\n				<div class="big-card-title">\n\n					<span>Great !</span>\n\n					<br>\n\n					Box telah ditambahkan\n\n				</div>\n\n\n\n			</div>\n\n\n\n		</div>\n\n\n\n		<div class="under">\n\n			<button class="submit" (click)="backToHome()">Selesai</button>\n\n		</div>\n\n\n\n	</div>'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\finish-assign-box\finish-assign-box.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], FinishAssignBoxPage);
    return FinishAssignBoxPage;
}());

//# sourceMappingURL=finish-assign-box.js.map

/***/ }),

/***/ 223:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DokumenPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_document_document__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { FileTransfer,  FileTransferObject } from '@ionic-native/file-transfer';
// import { File } from '@ionic-native/file';
// import { GlobalVariable } from '../../app/global';
/**
 * Generated class for the DokumenPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DokumenPage = /** @class */ (function () {
    function DokumenPage(navCtrl, navParams, DocumentProvider, 
        // private transfer: FileTransfer, 
        // private file: File,
        lc) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.DocumentProvider = DocumentProvider;
        this.lc = lc;
        this.documents = [];
        this.page = 0;
        this.q = '';
        this.file_url = 'http://dox.pgnmas.co.id/assets/lampiran/dokumen_file/';
        this.loader = this.lc.create();
        this.loader.present();
        this.getData();
    }
    DokumenPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DokumenPage');
    };
    DokumenPage.prototype.getData = function () {
        var _this = this;
        this.DocumentProvider.getAllDokumen(this.page, this.q).then(function (res) {
            Object.keys(res).forEach(function (value) {
                _this.documents.push(res[value]);
                ++_this.page;
            });
            _this.all = res;
            _this.loader.dismiss();
        }, function (err) {
            console.log("Failure");
        });
    };
    DokumenPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    DokumenPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        this.DocumentProvider.getAllDokumen(this.page, this.q).then(function (res) {
            Object.keys(res).forEach(function (value) {
                _this.documents.push(res[value]);
                ++_this.page;
            });
            infiniteScroll.complete();
        }, function (err) {
            console.log("Failure");
        });
    };
    // download(document){
    //   const fileTransfer: FileTransferObject = this.transfer.create();
    //   const url = this.file_url+document.dokumen_file;
    //   fileTransfer.download(url, this.file.dataDirectory + 'file.pdf').then((entry) => {
    //        console.log('download complete: ' + entry.toURL());
    //       }, (error) => {
    //         console.log(url);
    //         console.log(error);
    //       });
    //     }
    DokumenPage.prototype.getItems = function (ev) {
        var _this = this;
        this.documents = this.all;
        var res = this.documents;
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.DocumentProvider.getAllDokumen(0, val).then(function (res) {
                _this.documents = [];
                _this.page = 0;
                Object.keys(res).forEach(function (value) {
                    _this.documents.push(res[value]);
                    ++_this.page;
                });
                // this.all = res;
                _this.q = val;
                _this.loader.dismiss();
            }, function (err) {
                console.log("Failure");
            });
        }
        else {
            this.page = 0;
            this.q = '';
        }
    };
    DokumenPage.prototype.reset = function () {
        this.getData();
    };
    DokumenPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-dokumen',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\dokumen\dokumen.html"*/'<ion-content>\n\n\n\n  <div class="body-wrapper menu-wrapper">\n\n\n\n	<div class="content">\n\n		\n\n		<div class="box-view" data-aos="fade-up" data-aos-duration="500">\n\n				<button class="back__button" ion-button block (click)="back()">\n\n					<img src="assets/img/left-arrow.png" alt="">\n\n				</button>\n\n				<ion-searchbar type="text" placeholder="Search" (ionInput)="getItems($event)" (ionClear)="reset($event)" (ngModel)="searchKey"></ion-searchbar>\n\n		</div>\n\n\n\n		<div class="list-wrapper">\n\n			<ion-list>\n\n				<ion-item *ngFor="let document of documents" >\n\n					<ion-label class="label__list">\n\n						<ion-avatar item-start>\n\n					  		<img src="assets/img/file.png" alt="">\n\n					  	</ion-avatar>\n\n						<div class="icon__text">\n\n							<h2>No : {{ (document.no==null) ? \'--\' : document.no}}</h2>\n\n							<h3>Nama Pelaksana : {{ document.name }}</h3>\n\n							<h3>Tahun : {{ document.tahun }}</h3>\n\n							<p ion-text color="primary" class="indexed" *ngIf="document.indeks != null">Indeks : {{ document.indeks}}</p>\n\n							\n\n							<h3>Folder : {{ document.code_folder }}</h3>\n\n							<h3>Box : {{ document.code_box }}</h3>\n\n\n\n							<h3>Lemari : {{ document.code_cabinet }}</h3>\n\n							<p>Uraian : {{ document.uraian}}</p>\n\n						</div>\n\n						<button ion-button class="downloadBtn" color="primary" (click)="download(document)" *ngIf="(document.dokumen_file!=null&&document.dokumen_file!=\'\')">\n\n							<ion-icon name="document"></ion-icon>\n\n						</button>\n\n						<br class="clear">\n\n					</ion-label>\n\n				</ion-item>\n\n			</ion-list>\n\n		</div>\n\n<!-- \n\n		<div class="footer">\n\n			<button class="submit" (click)="scanFolder(document)">Lanjut</button>\n\n		</div> -->\n\n\n\n	</div>\n\n	<ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n   		<ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n 	</ion-infinite-scroll>\n\n\n\n</div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\dokumen\dokumen.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__providers_document_document__["a" /* DocumentProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_document_document__["a" /* DocumentProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], DokumenPage);
    return DokumenPage;
}());

//# sourceMappingURL=dokumen.js.map

/***/ }),

/***/ 224:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BerkasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_berkas_berkas__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__list_dokumen_berkas_list_dokumen_berkas__ = __webpack_require__(225);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the BerkasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BerkasPage = /** @class */ (function () {
    function BerkasPage(navCtrl, navParams, BerkasProvider, lc) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.BerkasProvider = BerkasProvider;
        this.lc = lc;
        this.loader = this.lc.create();
        this.loader.present();
        this.getData();
    }
    BerkasPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BerkasPage');
    };
    BerkasPage.prototype.getData = function () {
        var _this = this;
        this.BerkasProvider.getBerkasAll().then(function (res) {
            console.log(res);
            _this.archive = res;
            _this.all = res;
            _this.loader.dismiss();
        }, function (err) {
            console.log("Failure");
        });
    };
    BerkasPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    BerkasPage.prototype.next = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__list_dokumen_berkas_list_dokumen_berkas__["a" /* ListDokumenBerkasPage */], {
            id: id
        });
    };
    BerkasPage.prototype.getItems = function (ev) {
        this.archive = this.all;
        var res = this.archive;
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.archive = this.archive.filter(function (item) {
                if (item.klasifikasi != null && item.klasifikasi.toLowerCase().indexOf(val.toLowerCase()) > -1) {
                    return true;
                }
                else if (item.indeks != null && item.indeks.toLowerCase().indexOf(val.toLowerCase()) > -1) {
                    return true;
                }
                return false;
            });
        }
        else {
            this.archive = this.all;
        }
    };
    BerkasPage.prototype.reset = function () {
        this.getData();
    };
    BerkasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-berkas',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\berkas\berkas.html"*/'<ion-content>\n\n\n\n  <div class="body-wrapper menu-wrapper">\n\n\n\n	<div class="content">\n\n		\n\n		<div class="box-view" data-aos="fade-up" data-aos-duration="500">\n\n				<button class="back__button" ion-button block (click)="back()">\n\n					<img src="assets/img/left-arrow.png" alt="">\n\n				</button>\n\n			<ion-searchbar type="text" placeholder="Search" (ionInput)="getItems($event)" (ionClear)="reset($event)" (ngModel)="searchKey"></ion-searchbar>\n\n		</div>\n\n\n\n		<div class="list-wrapper">\n\n			<ion-list>\n\n				<ion-item *ngFor="let berkas of archive" (click)="next(berkas.id)">\n\n					<ion-label class="label__list">\n\n						<div class="icon__wrapper">\n\n					  		<img src="assets/img/dokumen.png" alt="">\n\n					  	</div>\n\n						<div>\n\n							<h3>No : {{ (berkas.klasifikasi==null) ? \'--\' : berkas.klasifikasi}}</h3>\n\n							<p>Description : {{ berkas.indeks}}</p>\n\n						</div>\n\n					</ion-label>\n\n					<!-- <ion-checkbox [(ngModel)]="berkas.value"></ion-checkbox> -->\n\n				</ion-item>\n\n			</ion-list>\n\n		</div>\n\n\n\n		<!-- <div class="footer">\n\n			<button class="submit" (click)="listDokumen()">Lanjut</button>\n\n		</div> -->\n\n\n\n	</div>\n\n\n\n</div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\berkas\berkas.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__providers_berkas_berkas__["a" /* BerkasProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_berkas_berkas__["a" /* BerkasProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], BerkasPage);
    return BerkasPage;
}());

//# sourceMappingURL=berkas.js.map

/***/ }),

/***/ 225:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListDokumenBerkasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_document_document__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { FileTransfer,  FileTransferObject } from '@ionic-native/file-transfer';
//import { File } from '@ionic-native/file';
/**
 * Generated class for the ListDokumenBerkasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ListDokumenBerkasPage = /** @class */ (function () {
    function ListDokumenBerkasPage(navCtrl, navParams, DocumentProvider, 
        // private transfer: FileTransfer, 
        // private file: File,
        lc) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.DocumentProvider = DocumentProvider;
        this.lc = lc;
        this.file_url = 'http://dox.pgnmas.co.id/assets/lampiran/dokumen_file/';
        this.Loader = this.lc.create();
        this.Loader.present();
        this.getData();
    }
    ListDokumenBerkasPage.prototype.getData = function () {
        var _this = this;
        this.DocumentProvider.getListDokumenByBerkas(this.navParams.get('id')).then(function (res) {
            console.log(res);
            _this.documents = res;
            _this.Loader.dismiss();
        }, function (err) {
            console.log("Failure");
        });
    };
    ListDokumenBerkasPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ListDokumenBerkasPage');
    };
    ListDokumenBerkasPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    ListDokumenBerkasPage.prototype.download = function (document) {
        // const fileTransfer: FileTransferObject = this.transfer.create();
        // const url = this.file_url+document.dokumen_file;
        // fileTransfer.download(url, this.file.dataDirectory + 'file.pdf').then((entry) => {
        //      console.log('download complete: ' + entry.toURL());
        //     }, (error) => {
        //       console.log(url);
        //       console.log(error);
        //     });
    };
    ListDokumenBerkasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list-dokumen-berkas',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\list-dokumen-berkas\list-dokumen-berkas.html"*/'<ion-content>\n\n\n\n  <div class="body-wrapper menu-wrapper">\n\n\n\n	<div class="content">\n\n		\n\n		<div class="box-view" data-aos="fade-up" data-aos-duration="500">\n\n				<button class="back__button" ion-button block (click)="back()">\n\n					<img src="assets/img/left-arrow.png" alt="">\n\n				</button>\n\n			<div class="search-box">\n\n				<input type="text" placeholder="Search">\n\n				<span class="icon">\n\n					<i class="fas fa-search"></i>\n\n				</span>\n\n			</div>\n\n		</div>\n\n\n\n		<div class="list-wrapper">\n\n			<ion-list>\n\n				<ion-item *ngFor="let document of documents" >\n\n					<ion-label class="label__list">\n\n						<div class="icon__wrapper">\n\n					  		<img src="assets/img/dokumen.png" alt="">\n\n					  	</div>\n\n						<div class="icon__text">\n\n							<h2>No : {{ (document.no==null) ? \'--\' : document.no}}</h2>\n\n							<h3>Folder : {{ document.code_folder }}</h3>\n\n							<h3>Box : {{ document.code_box }}</h3>\n\n							<h3>Lemari : {{ document.code_cabinet }}</h3>\n\n							<p>Description : {{ document.uraian}}</p>\n\n						</div>\n\n					<button ion-button class="downloadBtn" color="primary" (click)="download(document)" *ngIf="(document.dokumen_file!=null&&document.dokumen_file!=\'\')">\n\n						<ion-icon name="document"></ion-icon>\n\n					</button>\n\n					</ion-label>\n\n				</ion-item>\n\n			</ion-list>\n\n		</div>\n\n<!-- \n\n		<div class="footer">\n\n			<button class="submit" (click)="scanFolder(document)">Lanjut</button>\n\n		</div> -->\n\n\n\n	</div>\n\n\n\n</div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\list-dokumen-berkas\list-dokumen-berkas.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__providers_document_document__["a" /* DocumentProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_document_document__["a" /* DocumentProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], ListDokumenBerkasPage);
    return ListDokumenBerkasPage;
}());

//# sourceMappingURL=list-dokumen-berkas.js.map

/***/ }),

/***/ 226:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PemberkasanPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_berkas_berkas__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_jquery__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_jquery__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__list_dokumen_list_dokumen__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_home__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the PemberkasanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PemberkasanPage = /** @class */ (function () {
    function PemberkasanPage(navCtrl, navParams, berkasProvider, lc) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.berkasProvider = berkasProvider;
        this.lc = lc;
        this.page = 0;
        this.q = '';
        this.loader = this.lc.create();
        this.loader.present();
        this.getData();
    }
    PemberkasanPage.prototype.getData = function () {
        var _this = this;
        console.log('test');
        this.berkasProvider.getBerkas().then(function (res) {
            console.log(res);
            _this.berkas = res;
            _this.all = res;
            _this.loader.dismiss();
        }, function (err) {
            console.log(err);
        });
    };
    PemberkasanPage.prototype.openDocumentList = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__list_dokumen_list_dokumen__["a" /* ListDokumenPage */], {
            id_berkas: id
        });
    };
    PemberkasanPage.prototype.ngAfterViewInit = function () {
        __WEBPACK_IMPORTED_MODULE_3_jquery__(document).ready(function () {
            __WEBPACK_IMPORTED_MODULE_3_jquery__('.info-trigger-js').click(function (e) {
                e.stopPropagation();
                __WEBPACK_IMPORTED_MODULE_3_jquery__(this).toggleClass('active');
                __WEBPACK_IMPORTED_MODULE_3_jquery__('.info-overlay').toggleClass('active');
                __WEBPACK_IMPORTED_MODULE_3_jquery__('.overlay-bg').toggleClass('active');
            });
        });
        __WEBPACK_IMPORTED_MODULE_3_jquery__('page-pemberkasan').mouseup(function (e) {
            var container = __WEBPACK_IMPORTED_MODULE_3_jquery__('.info-overlay');
            // if the target of the click isn't the container nor a descendant of the container
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.removeClass('active');
                __WEBPACK_IMPORTED_MODULE_3_jquery__('.overlay-bg').removeClass('active');
                __WEBPACK_IMPORTED_MODULE_3_jquery__('.info-trigger-js').removeClass('active');
            }
        });
    };
    PemberkasanPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PemberkasanPage');
    };
    PemberkasanPage.prototype.back = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]);
    };
    PemberkasanPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        // this.getData();
        this.berkas = this.all;
        var res = this.berkas;
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.berkas = this.berkas.filter(function (item) {
                return (item.indeks.toLowerCase().indexOf(val.toLowerCase()) > -1);
                // return false;
                // return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
            // this.folders = matches;
        }
    };
    PemberkasanPage.prototype.reset = function () {
        this.getData();
    };
    PemberkasanPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-pemberkasan',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\pemberkasan\pemberkasan.html"*/'<ion-content>\n\n\n\n	<div class="body-wrapper menu-wrapper">\n\n  \n\n	  <div class="content">\n\n		  \n\n		  <div class="box-view" data-aos="fade-up" data-aos-duration="500">\n\n				  <button class="back__button" ion-button block (click)="back()">\n\n					  <img src="assets/img/left-arrow.png" alt="">\n\n				  </button>\n\n			   <ion-searchbar type="text" placeholder="Search" (ionInput)="getItems($event)" (ionClear)="reset($event)" (ngModel)="searchKey"></ion-searchbar>\n\n		  </div>\n\n  \n\n		  <div class="list-wrapper">\n\n			  <ion-list>\n\n\n\n				  <button ion-item text-wrap *ngFor="let _berkas of berkas" (click)="openDocumentList(_berkas.id)">\n\n					  <ion-label class="label__list">\n\n					  	<div class="icon__wrapper">\n\n					  		<img src="assets/img/pemberkasan.png" alt="">\n\n					  	</div>\n\n						<div>\n\n						  <h3>Indeks : {{ _berkas.indeks}}</h3>\n\n						  <p>Date : {{ _berkas.date}}</p>\n\n						</div>\n\n						<br class="clear">\n\n					  </ion-label>\n\n				  </button>\n\n			  </ion-list>\n\n		  </div>\n\n  \n\n		  <!-- <div class="footer">\n\n			  <button class="submit" (click)="listDokumen(berkas)">Lanjut</button>\n\n		  </div> -->\n\n  \n\n	  </div>\n\n  \n\n  </div>\n\n  \n\n  </ion-content>\n\n  '/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\pemberkasan\pemberkasan.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__providers_berkas_berkas__["a" /* BerkasProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_berkas_berkas__["a" /* BerkasProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], PemberkasanPage);
    return PemberkasanPage;
}());

//# sourceMappingURL=pemberkasan.js.map

/***/ }),

/***/ 227:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListDokumenPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_document_document__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__scan_folder_pemberkasan_scan_folder_pemberkasan__ = __webpack_require__(228);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ListDokumenPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ListDokumenPage = /** @class */ (function () {
    function ListDokumenPage(navCtrl, navParams, DocumentProvider, lc) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.DocumentProvider = DocumentProvider;
        this.lc = lc;
        this.documents = [];
        this.page = 0;
        this.q = '';
        this.loader = this.lc.create();
        this.loader.present();
        this.getData();
    }
    ListDokumenPage.prototype.getData = function () {
        var _this = this;
        this.DocumentProvider.getListDokumen(this.page, this.q).then(function (res) {
            console.log(res);
            Object.keys(res).forEach(function (value) {
                _this.documents.push(res[value]);
                ++_this.page;
            });
            _this.all = res;
            _this.loader.dismiss();
        }, function (err) {
            console.log("Failure");
        });
    };
    ListDokumenPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        this.DocumentProvider.getListDokumen(this.page, this.q).then(function (res) {
            Object.keys(res).forEach(function (value) {
                _this.documents.push(res[value]);
                ++_this.page;
            });
            // this.page++;
            infiniteScroll.complete();
        }, function (err) {
            console.log("Failure");
        });
    };
    ListDokumenPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ListDokumenPage');
    };
    ListDokumenPage.prototype.scanFolder = function () {
        var data = {
            id_berkas: this.navParams.get('id_berkas'),
            document: this.documents
        };
        console.log(data);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__scan_folder_pemberkasan_scan_folder_pemberkasan__["a" /* ScanFolderPemberkasanPage */], data);
    };
    ListDokumenPage.prototype.getItems = function (ev) {
        var _this = this;
        // Reset items back to all of the items
        // this.getData();
        this.documents = this.all;
        var res = this.documents;
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.DocumentProvider.getListDokumen(0, val).then(function (res) {
                _this.documents = [];
                _this.page = 0;
                Object.keys(res).forEach(function (value) {
                    _this.documents.push(res[value]);
                    ++_this.page;
                });
                // this.all = res;
                _this.q = val;
                _this.loader.dismiss();
            }, function (err) {
                console.log("Failure");
            });
            /*this.documents = this.documents.filter((item) => {
      
              if(item.keterangan !=null && item.keterangan.toLowerCase().indexOf(val.toLowerCase()) > -1){
                return true;
               }else if(item.no !=null && item.no.toLowerCase().indexOf(val.toLowerCase()) > -1){
                 return true;
               }else if(item.uraian !=null && item.uraian.toLowerCase().indexOf(val.toLowerCase()) > -1){
                 return true;
               }
               console.log(item.uraian);
               console.log(item.uraian.toLowerCase().indexOf(val.toLowerCase()));
              //  typeof item.no=== 'string'&&typeof item.uraian=== 'string'){
              //   return ( || item.uraian.toLowerCase().indexOf(val.toLowerCase()) > -1||item.indeks.toLowerCase().indexOf(val.toLowerCase()) > -1);
              // }// return false;
              // return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
              return false;
              // return false;
              // return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
            // this.folders = matches;*/
        }
        else {
            this.page = 0;
            this.q = '';
        }
    };
    ListDokumenPage.prototype.reset = function () {
        this.getData();
    };
    ListDokumenPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    ListDokumenPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list-dokumen',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\list-dokumen\list-dokumen.html"*/'<ion-content>\n\n\n\n  <div class="body-wrapper menu-wrapper">\n\n\n\n	<div class="content">\n\n		\n\n		<div class="box-view" data-aos="fade-up" data-aos-duration="500">\n\n				<button class="back__button" ion-button block (click)="back()">\n\n					<img src="assets/img/left-arrow.png" alt="">\n\n				</button>\n\n			 <ion-searchbar type="text" placeholder="Search" (ionInput)="getItems($event)" (ionClear)="reset($event)" (ngModel)="searchKey"></ion-searchbar>\n\n		</div>\n\n\n\n		<div class="list-wrapper">\n\n			<ion-list>\n\n				<ion-item text-wrap *ngFor="let document of documents" >\n\n					<ion-label>\n\n						<div>\n\n							<h2>No : {{ (document.no==null) ? \'--\' : document.no}}</h2>\n\n							<p ion-text color="primary" class="indexed" *ngIf="document.indeks != null">Indeks : {{ document.indeks}}</p>\n\n							<h3>Folder : {{ document.code_folder }}</h3>\n\n							<h3>Box : {{ document.code_box }}</h3>\n\n\n\n							\n\n							<h3>Lemari : {{ document.code_cabinet }}</h3>\n\n							<p>Description : {{ document.uraian}}</p>\n\n\n\n							<!-- <p>{{ documen.code_box }}</p> -->\n\n						</div>\n\n					</ion-label>\n\n					<ion-checkbox [(ngModel)]="document.value"></ion-checkbox>\n\n					<!-- <button ion-button color="light" (click)="download()">\n\n						<ion-icon name="document"></ion-icon>\n\n					</button> -->\n\n					\n\n				</ion-item>\n\n			</ion-list>\n\n		</div>\n\n		<ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n	   		<ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n	 	</ion-infinite-scroll>\n\n	</div>\n\n\n\n</div>\n\n\n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <ion-toolbar>\n\n    <button class="submit" (click)="scanFolder(document)">Lanjut</button>\n\n  </ion-toolbar>\n\n</ion-footer>\n\n'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\list-dokumen\list-dokumen.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__providers_document_document__["a" /* DocumentProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_document_document__["a" /* DocumentProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], ListDokumenPage);
    return ListDokumenPage;
}());

//# sourceMappingURL=list-dokumen.js.map

/***/ }),

/***/ 228:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScanFolderPemberkasanPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__scan_box_pemberkasan_scan_box_pemberkasan__ = __webpack_require__(229);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




/**
 * Generated class for the ScanFolderPemberkasanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ScanFolderPemberkasanPage = /** @class */ (function () {
    function ScanFolderPemberkasanPage(navCtrl, navParams, barcode) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.barcode = barcode;
        this.isCapture = true;
        // console.log(JSON.stringify(this.navParams));
    }
    ScanFolderPemberkasanPage.prototype.encodeData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.barcode.encode(this.barcode.Encode.TEXT_TYPE, this.dataToEncode)];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ScanFolderPemberkasanPage.prototype.scanBarcode = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var options, _a, error_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        options = {
                            prompt: 'Point your camera at a barcode',
                            torchOn: false
                        };
                        _a = this;
                        return [4 /*yield*/, this.barcode.scan(options).then(function (res) {
                                _this.isCapture = true;
                                _this.result = res;
                                return res;
                            }, function (err) {
                                _this.isCapture = true;
                                return err;
                            })];
                    case 1:
                        _a.result = _b.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_2 = _b.sent();
                        console.error(error_2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ScanFolderPemberkasanPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ScanFolderPemberkasanPage');
    };
    ScanFolderPemberkasanPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    ScanFolderPemberkasanPage.prototype.scanBox = function () {
        var data = {
            id_folder: this.result.text,
            id_berkas: this.navParams.get('id_berkas'),
            document: this.navParams.get('document')
        };
        console.log(data);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__scan_box_pemberkasan_scan_box_pemberkasan__["a" /* ScanBoxPemberkasanPage */], data);
    };
    ScanFolderPemberkasanPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-scan-folder-pemberkasan',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\scan-folder-pemberkasan\scan-folder-pemberkasan.html"*/'<div class="scanner-page" data-aos="fade-up" data-aos-duration="500">\n\n		\n\n	<div class="scanner-header" data-aos="fade-down" data-aos-duration="1500">\n\n		<div class="upper">\n\n			<!-- <a class="button-back">\n\n				<span class="icon"><i class="fas fa-arrow-left"></i></span>\n\n				Back\n\n			</a>\n\n			<div class="button-group">\n\n				<a href="#">\n\n					<span class="icon"><i class="fas fa-image"></i></span>\n\n					<span class="icon"><i class="fas fa-bolt"></i></span>\n\n				</a>\n\n			</div>\n\n		</div>\n\n		<div class="under">\n\n			<p>Point your camera towards the QR code <br> to scan</p>\n\n		</div>\n\n	</div> -->\n\n	\n\n	<ion-row>\n\n			<button class="back__button" ion-button block (click)="back()">\n\n				<img src="assets/img/left-arrow.png" alt="">\n\n			</button>\n\n		</ion-row>\n\n		<div class="under">\n\n			<ion-row>\n\n				<p>Point your camera towards the QR code <br> to scan</p>\n\n			</ion-row>\n\n		</div>\n\n		</div>\n\n	</div>\n\n\n\n	<ion-content padding>\n\n		<ion-card *ngIf="result">\n\n		  <ion-card-content>\n\n			<p>Text: {{result.text}}</p>\n\n			<p>Format: {{result.format}}</p>\n\n			<p>Cancelled: {{result.cancelled}}</p>\n\n		  </ion-card-content>\n\n		</ion-card>\n\n	  \n\n		<ion-row>\n\n		  <button ion-button block (click)="scanBarcode()">SCAN FOLDER</button>\n\n		</ion-row>\n\n		\n\n</ion-content>\n\n\n\n	<div class="scanner-footer" *ngIf="isCapture">\n\n		<div class="under">\n\n			<button class="submit" (click)="scanBox()">Lanjut Scan Box</button>\n\n		</div>\n\n	</div>\n\n\n\n</div>\n\n\n\n'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\scan-folder-pemberkasan\scan-folder-pemberkasan.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__["a" /* BarcodeScanner */]])
    ], ScanFolderPemberkasanPage);
    return ScanFolderPemberkasanPage;
}());

//# sourceMappingURL=scan-folder-pemberkasan.js.map

/***/ }),

/***/ 229:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScanBoxPemberkasanPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_berkas_berkas__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__entry3new_entry3new__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





/**
 * Generated class for the ScanBoxPemberkasanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ScanBoxPemberkasanPage = /** @class */ (function () {
    // isCapture: boolean = true;
    function ScanBoxPemberkasanPage(navCtrl, navParams, barcode, berkasProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.barcode = barcode;
        this.berkasProvider = berkasProvider;
    }
    ScanBoxPemberkasanPage.prototype.encodeData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.barcode.encode(this.barcode.Encode.TEXT_TYPE, this.dataToEncode)];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ScanBoxPemberkasanPage.prototype.scanBarcode = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var options, _a, error_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        options = {
                            prompt: 'Point your camera at a barcode',
                            torchOn: false
                        };
                        // console.log(JSON.stringify(this.navParams));
                        _a = this;
                        return [4 /*yield*/, this.barcode.scan(options).then(function (res) {
                                var data = {
                                    id_box: res.text,
                                    id_folder: _this.navParams.get('id_folder'),
                                    id_berkas: _this.navParams.get('id_berkas'),
                                    document: _this.navParams.get('document')
                                };
                                _this.berkasProvider.archiving(data).then(function (res) {
                                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__entry3new_entry3new__["a" /* Entry3newPage */]);
                                }, function (err) {
                                    console.log(JSON.stringify(err));
                                    console.log("Failure");
                                });
                            }, function (err) {
                                return err;
                            })];
                    case 1:
                        // console.log(JSON.stringify(this.navParams));
                        _a.result = _b.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_2 = _b.sent();
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ScanBoxPemberkasanPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ScanBoxPemberkasanPage');
    };
    ScanBoxPemberkasanPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    ScanBoxPemberkasanPage.prototype.finish = function () {
    };
    ScanBoxPemberkasanPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-scan-box-pemberkasan',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\scan-box-pemberkasan\scan-box-pemberkasan.html"*/'<div class="scanner-page" data-aos="fade-up" data-aos-duration="500">\n\n		\n\n	<div class="scanner-header" data-aos="fade-down" data-aos-duration="1500">\n\n		<div class="upper">\n\n				<ion-row>\n\n				<button class="back__button" ion-button block (click)="back()">\n\n					<img src="assets/img/left-arrow.png" alt="">\n\n				</button>\n\n			</ion-row>\n\n			<div class="under">\n\n				<ion-row>\n\n					<p>Point your camera towards the QR code <br> to scan</p>\n\n				</ion-row>\n\n			</div>\n\n			</div>\n\n		</div>\n\n	\n\n\n\n	<ion-content padding>\n\n		<ion-card *ngIf="result">\n\n		  <ion-card-content>\n\n			<p>Text: {{result.text}}</p>\n\n			<p>Format: {{result.format}}</p>\n\n			<p>Cancelled: {{result.cancelled}}</p>\n\n		  </ion-card-content>\n\n		</ion-card>\n\n	  \n\n		<ion-row>\n\n		  <button ion-button block (click)="scanBarcode()">SCAN BOX</button>\n\n		</ion-row>\n\n		\n\n</ion-content>\n\n\n\n	<div class="scanner-footer">\n\n		<div class="under">\n\n			<button class="submit" (click)="finish()">Selesai</button>\n\n		</div>\n\n	</div>\n\n</div>'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\scan-box-pemberkasan\scan-box-pemberkasan.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__["a" /* BarcodeScanner */], __WEBPACK_IMPORTED_MODULE_2__providers_berkas_berkas__["a" /* BerkasProvider */]])
    ], ScanBoxPemberkasanPage);
    return ScanBoxPemberkasanPage;
}());

//# sourceMappingURL=scan-box-pemberkasan.js.map

/***/ }),

/***/ 230:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PeminjamanPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__form_peminjaman_form_peminjaman__ = __webpack_require__(231);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__peminjaman_detail_peminjaman_detail__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_peminjaman_peminjaman__ = __webpack_require__(61);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the PeminjamanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PeminjamanPage = /** @class */ (function () {
    function PeminjamanPage(navCtrl, navParams, peminjamanProvider, lc) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.peminjamanProvider = peminjamanProvider;
        this.lc = lc;
        this.loader = this.lc.create();
        this.loader.present();
        this.getData();
    }
    PeminjamanPage.prototype.getData = function () {
        var _this = this;
        this.peminjamanProvider.getPeminjaman().then(function (res) {
            _this.peminjaman = res;
            _this.all = res;
            _this.loader.dismiss();
        }, function (err) {
            console.log("Failure");
        });
    };
    PeminjamanPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PeminjamanPage');
    };
    PeminjamanPage.prototype.back = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
    };
    PeminjamanPage.prototype.form = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__form_peminjaman_form_peminjaman__["a" /* FormPeminjamanPage */]);
    };
    PeminjamanPage.prototype.openDetail = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__peminjaman_detail_peminjaman_detail__["a" /* PeminjamanDetailPage */], {
            id: id
        });
    };
    PeminjamanPage.prototype.getItems = function (ev) {
        this.peminjaman = this.all;
        var res = this.peminjaman;
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.peminjaman = this.peminjaman.filter(function (item) {
                if (item.keperluan != null && item.keperluan.toLowerCase().indexOf(val.toLowerCase()) > -1) {
                    return true;
                }
                else if (item.date != null && item.date.toLowerCase().indexOf(val.toLowerCase()) > -1) {
                    return true;
                }
                else if (item.return_date != null && item.return_date.toLowerCase().indexOf(val.toLowerCase()) > -1) {
                    return true;
                }
                else if (item.remark != null && item.remark.toLowerCase().indexOf(val.toLowerCase()) > -1) {
                    return true;
                }
                return false;
            });
        }
        else {
            this.peminjaman = this.all;
        }
    };
    PeminjamanPage.prototype.reset = function () {
        this.getData();
    };
    PeminjamanPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-peminjaman',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\peminjaman\peminjaman.html"*/'<ion-content>\n\n\n\n	<div class="body-wrapper menu-wrapper">\n\n  \n\n	  <div class="content">\n\n		  \n\n		  <div class="box-view" data-aos="fade-up" data-aos-duration="500">\n\n				  <button class="back__button" ion-button block (click)="back()">\n\n					  <img src="assets/img/left-arrow.png" alt="">\n\n				  </button>\n\n				  <ion-searchbar type="text" placeholder="Search" (ionInput)="getItems($event)" (ionClear)="reset($event)" (ngModel)="searchKey"></ion-searchbar>\n\n		  </div>\n\n  \n\n		  <div class="list-wrapper">\n\n			  <ion-list>\n\n				  <ion-item *ngFor="let _peminjaman of peminjaman" (click)="openDetail(_peminjaman.id)">\n\n					  <ion-label class="label__list">\n\n						<div class="icon__wrapper">\n\n					  		<img src="assets/img/peminjaman.png" alt="">\n\n					  	</div>\n\n						<div class="icon__text">\n\n							<h3>Keperluan : {{ _peminjaman.keperluan}}</h3>\n\n							<p>Tanggal Pinjam : {{ _peminjaman.date}}</p>\n\n							<p>Tanggal Pengembalian : {{ _peminjaman.return_date}}</p>\n\n							<p>Catatan : {{ _peminjaman.remark}}</p>\n\n						</div>\n\n					  </ion-label>\n\n				  </ion-item>\n\n			  </ion-list>\n\n		  </div>\n\n		  \n\n		  <!-- <div class="footer">\n\n			  <button class="submit" (click)="listDokumen(berkas)">Lanjut</button>\n\n		  </div> -->\n\n  \n\n	  </div>\n\n  \n\n  </div>\n\n  <ion-fab right bottom>\n\n	<button ion-fab color="blue" (click)="form()"><ion-icon name="add"></ion-icon></button>\n\n  </ion-fab>\n\n  </ion-content>\n\n  '/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\peminjaman\peminjaman.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5__providers_peminjaman_peminjaman__["a" /* PeminjamanProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], PeminjamanPage);
    return PeminjamanPage;
}());

//# sourceMappingURL=peminjaman.js.map

/***/ }),

/***/ 231:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormPeminjamanPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__finish_peminjaman_finish_peminjaman__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__list_dokumen_new_list_dokumen_new__ = __webpack_require__(232);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the FormPeminjamanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FormPeminjamanPage = /** @class */ (function () {
    function FormPeminjamanPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.peminjaman = {};
    }
    FormPeminjamanPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FormPeminjamanPage');
    };
    FormPeminjamanPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    FormPeminjamanPage.prototype.submit = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__finish_peminjaman_finish_peminjaman__["a" /* FinishPeminjamanPage */]);
    };
    FormPeminjamanPage.prototype.next = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__list_dokumen_new_list_dokumen_new__["a" /* ListDokumenNewPage */], {
            peminjaman: this.peminjaman
        });
    };
    FormPeminjamanPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-form-peminjaman',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\form-peminjaman\form-peminjaman.html"*/'<ion-content>\n\n\n\n	<div class="body-wrapper menu-wrapper">\n\n  \n\n	  <div class="content">\n\n		  \n\n		  <div class="box-view" data-aos="fade-up" data-aos-duration="500">\n\n				  <button class="back__button" ion-button block (click)="back()">\n\n					  <img src="assets/img/left-arrow.png" alt="">\n\n				  </button>\n\n				  <div class="search-box">\n\n					  <ion-title>Form Peminjaman</ion-title>\n\n				</div>\n\n			  <!-- \n\n				  <input type="text" placeholder="Search">\n\n				  <span class="icon">\n\n					  <i class="fas fa-search"></i>\n\n				  </span>\n\n			   -->\n\n		  </div>\n\n		<ion-item style="margin-top: 15px">	\n\n				<ion-input placeholder="Keperluan" name="keperluan" [(ngModel)]="peminjaman.keperluan"></ion-input>\n\n		</ion-item>\n\n		<br>\n\n		<ion-item>	\n\n				<ion-input placeholder="Jml. Hari Peminjaman" type="number" name="date" [(ngModel)]="peminjaman.date"></ion-input>\n\n		</ion-item>\n\n		<br>\n\n		<ion-item>	\n\n				<ion-textarea placeholder="Catatan" name="remark" [(ngModel)]="peminjaman.remark"></ion-textarea>\n\n		</ion-item>\n\n  \n\n	  </div>\n\n  \n\n  </div>\n\n  </ion-content>\n\n\n\n<ion-footer>\n\n  <ion-toolbar>\n\n    <button class="submit" (click)="next()">Lanjut</button>\n\n  </ion-toolbar>\n\n</ion-footer>\n\n  '/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\form-peminjaman\form-peminjaman.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], FormPeminjamanPage);
    return FormPeminjamanPage;
}());

//# sourceMappingURL=form-peminjaman.js.map

/***/ }),

/***/ 232:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListDokumenNewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_document_document__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_peminjaman_peminjaman__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__finish_peminjaman_finish_peminjaman__ = __webpack_require__(115);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ListDokumenNewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ListDokumenNewPage = /** @class */ (function () {
    function ListDokumenNewPage(navCtrl, navParams, DocumentProvider, peminjamanProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.DocumentProvider = DocumentProvider;
        this.peminjamanProvider = peminjamanProvider;
        this.getData();
    }
    ListDokumenNewPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ListDokumenNewPage');
    };
    ListDokumenNewPage.prototype.getData = function () {
        var _this = this;
        this.DocumentProvider.getDocumentAll().then(function (res) {
            console.log(res);
            _this.documents = res;
            _this.all = res;
        }, function (err) {
            console.log("Failure");
        });
    };
    ListDokumenNewPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    ListDokumenNewPage.prototype.submit = function () {
        var _this = this;
        var data = {
            peminjaman: this.navParams.get('peminjaman'),
            documents: this.documents
        };
        this.peminjamanProvider.proses(data).then(function (res) {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__finish_peminjaman_finish_peminjaman__["a" /* FinishPeminjamanPage */]);
        }, function (err) {
        });
    };
    ListDokumenNewPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        // this.getData();
        this.documents = this.all;
        var res = this.documents;
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.documents = this.documents.filter(function (item) {
                if (item.keterangan != null && item.keterangan.toLowerCase().indexOf(val.toLowerCase()) > -1) {
                    return true;
                }
                else if (item.no != null && item.no.toLowerCase().indexOf(val.toLowerCase()) > -1) {
                    return true;
                }
                else if (item.uraian != null && item.uraian.toLowerCase().indexOf(val.toLowerCase()) > -1) {
                    return true;
                }
                console.log(item.uraian);
                console.log(item.uraian.toLowerCase().indexOf(val.toLowerCase()));
                //  typeof item.no=== 'string'&&typeof item.uraian=== 'string'){
                //   return ( || item.uraian.toLowerCase().indexOf(val.toLowerCase()) > -1||item.indeks.toLowerCase().indexOf(val.toLowerCase()) > -1);
                // }// return false;
                // return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
                return false;
                // return false;
                // return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
            // this.folders = matches;
        }
    };
    ListDokumenNewPage.prototype.reset = function () {
        this.getData();
    };
    ListDokumenNewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list-dokumen-new',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\list-dokumen-new\list-dokumen-new.html"*/'<ion-content>\n\n\n\n  <div class="body-wrapper menu-wrapper">\n\n\n\n	<div class="content">\n\n		\n\n		<div class="box-view" data-aos="fade-up" data-aos-duration="500">\n\n				<button class="back__button" ion-button block (click)="back()">\n\n					<img src="assets/img/left-arrow.png" alt="">\n\n				</button>\n\n			<ion-searchbar type="text" placeholder="Search" (ionInput)="getItems($event)" (ionClear)="reset($event)" (ngModel)="searchKey"></ion-searchbar>\n\n		</div>\n\n\n\n		<div class="list-wrapper">\n\n			<ion-list>\n\n				<ion-item *ngFor="let document of documents" >\n\n					<ion-label>\n\n						<div>\n\n							<h3>No : {{ (document.no==null) ? \'--\' : document.no}}</h3>\n\n							<p>Description : {{ document.uraian}}</p>\n\n						</div>\n\n					</ion-label>\n\n					<ion-checkbox [(ngModel)]="document.value"></ion-checkbox>\n\n				</ion-item>\n\n			</ion-list>\n\n		</div>\n\n\n\n		<div class="footer">\n\n			<button class="submit" (click)="submit()">Lanjut</button>\n\n		</div>\n\n\n\n	</div>\n\n\n\n</div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\list-dokumen-new\list-dokumen-new.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__providers_document_document__["a" /* DocumentProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_document_document__["a" /* DocumentProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_peminjaman_peminjaman__["a" /* PeminjamanProvider */]])
    ], ListDokumenNewPage);
    return ListDokumenNewPage;
}());

//# sourceMappingURL=list-dokumen-new.js.map

/***/ }),

/***/ 233:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PeminjamanDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_peminjaman_peminjaman__ = __webpack_require__(61);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the PeminjamanDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PeminjamanDetailPage = /** @class */ (function () {
    function PeminjamanDetailPage(navCtrl, navParams, peminjamanProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.peminjamanProvider = peminjamanProvider;
        this.getData();
    }
    PeminjamanDetailPage.prototype.getData = function () {
        var _this = this;
        this.peminjamanProvider.getPeminjamanDetail(this.navParams.get('id')).then(function (res) {
            console.log(res);
            _this.detail = res;
        }, function (err) {
            console.log("Failure");
        });
    };
    PeminjamanDetailPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    PeminjamanDetailPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PeminjamanDetailPage');
    };
    PeminjamanDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-peminjaman-detail',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\peminjaman-detail\peminjaman-detail.html"*/'<ion-content>\n\n\n\n	<div class="body-wrapper menu-wrapper">\n\n  \n\n	  <div class="content">\n\n		  \n\n		  <div class="box-view" data-aos="fade-up" data-aos-duration="500">\n\n				  <button class="back__button" ion-button block (click)="back()">\n\n					  <img src="assets/img/left-arrow.png" alt="">\n\n				  </button>\n\n			  <div class="search-box">\n\n				  <input type="text" placeholder="Search">\n\n				  <span class="icon">\n\n					  <i class="fas fa-search"></i>\n\n				  </span>\n\n			  </div>\n\n		  </div>\n\n  \n\n		  <div class="list-wrapper">\n\n			  <ion-list>\n\n				  <ion-item *ngFor="let details of detail">\n\n					  <ion-label class="label__list">\n\n						<div class="icon__wrapper">\n\n					  		<img src="assets/img/peminjaman.png" alt="">\n\n					  	</div>\n\n						<div class="icon__text">\n\n						  	<h3>No : {{ (details.no==null) ? \'--\' : details.no}}</h3>\n\n							<p>Description : {{ details.uraian}}</p>\n\n						</div>\n\n					  </ion-label>\n\n				  </ion-item>\n\n			  </ion-list>\n\n		  </div>\n\n		  \n\n		  <!-- <div class="footer">\n\n			  <button class="submit" (click)="listDokumen(berkas)">Lanjut</button>\n\n		  </div> -->\n\n  \n\n	  </div>\n\n  \n\n  </div>\n\n  </ion-content>\n\n  '/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\peminjaman-detail\peminjaman-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_peminjaman_peminjaman__["a" /* PeminjamanProvider */]])
    ], PeminjamanDetailPage);
    return PeminjamanDetailPage;
}());

//# sourceMappingURL=peminjaman-detail.js.map

/***/ }),

/***/ 234:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_global__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var UserProvider = /** @class */ (function () {
    function UserProvider(http) {
        this.http = http;
        this.apiUrl = __WEBPACK_IMPORTED_MODULE_2__app_global__["a" /* GlobalVariable */].BASE_API_LOGIN;
    }
    UserProvider.prototype.login = function (account) {
        var _this = this;
        console.log(account);
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' });
            _this.http.post(_this.apiUrl + 'login', account, { headers: headers })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    UserProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], UserProvider);
    return UserProvider;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 235:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListFolderByBoxPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_folder_folder__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__list_dokumen_by_folder_list_dokumen_by_folder__ = __webpack_require__(236);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ListFolderByBoxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ListFolderByBoxPage = /** @class */ (function () {
    function ListFolderByBoxPage(navCtrl, navParams, FolderProvider, lc) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.FolderProvider = FolderProvider;
        this.lc = lc;
        this.folders = [];
        this.page = 0;
        this.q = '';
        this.loader = this.lc.create();
        this.loader.present();
        this.box = this.navParams.get('box');
        this.getData();
    }
    ListFolderByBoxPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AssignFolderPage');
    };
    ListFolderByBoxPage.prototype.getData = function () {
        var _this = this;
        this.FolderProvider.getFolderByBox(this.box, this.page, this.q).then(function (res) {
            Object.keys(res).forEach(function (value) {
                _this.folders.push(res[value]);
                ++_this.page;
            });
            _this.all = res;
            _this.loader.dismiss();
        }, function (err) {
            console.log("Failure");
        });
    };
    ListFolderByBoxPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        this.FolderProvider.getFolderByBox(this.box, this.page, this.q).then(function (res) {
            Object.keys(res).forEach(function (value) {
                _this.folders.push(res[value]);
                ++_this.page;
            });
            // this.page++;
            infiniteScroll.complete();
        }, function (err) {
            console.log("Failure");
        });
    };
    ListFolderByBoxPage.prototype.next = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__list_dokumen_by_folder_list_dokumen_by_folder__["a" /* ListDokumenByFolderPage */], {
            folder: id
        });
    };
    ListFolderByBoxPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    ListFolderByBoxPage.prototype.getItems = function (ev) {
        var _this = this;
        // Reset items back to all of the items
        // this.getData();
        // this.folders = this.all;
        var res = this.folders;
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.FolderProvider.getFolderByBox(this.box, 0, val).then(function (res) {
                _this.folders = [];
                _this.page = 0;
                Object.keys(res).forEach(function (value) {
                    _this.folders.push(res[value]);
                    ++_this.page;
                });
                // this.all = res;
                _this.q = val;
                _this.loader.dismiss();
            }, function (err) {
                console.log("Failure");
            });
            /*this.folders = this.folders.filter((item) => {
      
              return (item.code.toLowerCase().indexOf(val.toLowerCase()) > -1);
              // return false;
              // return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })*/
            // this.folders = matches;
        }
        else {
            this.page = 0;
            this.q = '';
        }
    };
    ListFolderByBoxPage.prototype.reset = function () {
        this.getData();
    };
    ListFolderByBoxPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list-folder-by-box',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\list-folder-by-box\list-folder-by-box.html"*/'<ion-content>\n\n  <div class="body-wrapper menu-wrapper">\n  \n    <div class="content">\n      \n        <div class="box-view" data-aos="fade-up" data-aos-duration="500">\n            <button class="back__button" ion-button block (click)="back()">\n                <img src="assets/img/left-arrow.png" alt="">\n            </button>\n            <ion-searchbar type="text" placeholder="Search" (ionInput)="getItems($event)" (ionClear)="reset($event)" (ngModel)="searchKey"></ion-searchbar>\n            <!-- <div class="search-box">\n                \n                <span class="icon">\n                    <i class="fas fa-search"></i>\n                </span>\n            </div> -->\n        </div>\n  \n        <div class="list-wrapper">\n            <ion-list>\n                <ion-item *ngFor="let folder of folders" (click) ="next(folder.id)">\n                  \n                      <ion-label item-end class="list-folder">\n                        <img src="assets/img/folder-logo.png" class="folder-img">\n                        <div>\n                            \n                            <h3>Kode Folder : {{folder.code}}</h3>\n                            <p>Record Center : {{ folder.record_center}}</p>\n                        </div>\n                    </ion-label>\n                </ion-item>\n            </ion-list>\n        </div>\n        <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n            <ion-infinite-scroll-content></ion-infinite-scroll-content>\n        </ion-infinite-scroll>\n    </div>\n  \n</div>\n  \n</ion-content>\n\n<ion-footer>\n\n</ion-footer>'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\list-folder-by-box\list-folder-by-box.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_folder_folder__["a" /* FolderProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], ListFolderByBoxPage);
    return ListFolderByBoxPage;
}());

//# sourceMappingURL=list-folder-by-box.js.map

/***/ }),

/***/ 236:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListDokumenByFolderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_document_document__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



// import { FileTransfer,  FileTransferObject } from '@ionic-native/file-transfer';
//import { File } from '@ionic-native/file';
/**
 * Generated class for the ListDokumenByFolderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ListDokumenByFolderPage = /** @class */ (function () {
    function ListDokumenByFolderPage(navCtrl, DocumentProvider, navParams, /*private transfer: FileTransfer,
          private file: File,*/ lc) {
        this.navCtrl = navCtrl;
        this.DocumentProvider = DocumentProvider;
        this.navParams = navParams;
        this.lc = lc;
        this.documents = [];
        this.page = 0;
        this.q = '';
        this.file_url = 'http://dox.pgnmas.co.id/assets/lampiran/dokumen_file/';
        this.loader = this.lc.create();
        this.loader.present();
        this.folder = this.navParams.get('folder');
        this.getData();
    }
    ListDokumenByFolderPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DokumenPage');
    };
    ListDokumenByFolderPage.prototype.getData = function () {
        var _this = this;
        this.DocumentProvider.getDokumenByFolder(this.folder, this.q).then(function (res) {
            Object.keys(res).forEach(function (value) {
                _this.documents.push(res[value]);
                ++_this.page;
            });
            _this.all = res;
            _this.loader.dismiss();
        }, function (err) {
            console.log("Failure");
        });
    };
    ListDokumenByFolderPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    ListDokumenByFolderPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        this.DocumentProvider.getDokumenByFolder(this.folder, this.q).then(function (res) {
            Object.keys(res).forEach(function (value) {
                _this.documents.push(res[value]);
                ++_this.page;
            });
            infiniteScroll.complete();
        }, function (err) {
            console.log("Failure");
        });
    };
    ListDokumenByFolderPage.prototype.download = function (document) {
        // const fileTransfer: FileTransferObject = this.transfer.create();
        // const url = this.file_url+document.dokumen_file;
        // fileTransfer.download(url, this.file.dataDirectory + 'file.pdf').then((entry) => {
        //      console.log('download complete: ' + entry.toURL());
        //     }, (error) => {
        //       console.log(url);
        //       console.log(error);
        //     });
    };
    ListDokumenByFolderPage.prototype.getItems = function (ev) {
        var _this = this;
        this.documents = this.all;
        var res = this.documents;
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.DocumentProvider.getDokumenByFolder(this.folder, val).then(function (res) {
                _this.documents = [];
                _this.page = 0;
                Object.keys(res).forEach(function (value) {
                    _this.documents.push(res[value]);
                    ++_this.page;
                });
                // this.all = res;
                _this.q = val;
                _this.loader.dismiss();
            }, function (err) {
                console.log("Failure");
            });
        }
        else {
            this.page = 0;
            this.q = '';
        }
    };
    ListDokumenByFolderPage.prototype.reset = function () {
        this.getData();
    };
    ListDokumenByFolderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list-dokumen-by-folder',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\list-dokumen-by-folder\list-dokumen-by-folder.html"*/'<ion-content>\n\n  <div class="body-wrapper menu-wrapper">\n\n	<div class="content">\n		\n		<div class="box-view" data-aos="fade-up" data-aos-duration="500">\n				<button class="back__button" ion-button block (click)="back()">\n					<img src="assets/img/left-arrow.png" alt="">\n				</button>\n				<ion-searchbar type="text" placeholder="Search" (ionInput)="getItems($event)" (ionClear)="reset($event)" (ngModel)="searchKey"></ion-searchbar>\n		</div>\n\n		<div class="list-wrapper">\n			<ion-list>\n				<ion-item *ngFor="let document of documents" >\n					<ion-label class="label__list">\n						<ion-avatar item-start>\n					  		<img src="assets/img/file.png" alt="">\n					  	</ion-avatar>\n						<div class="icon__text">\n							<h2>No : {{ (document.no==null) ? \'--\' : document.no}}</h2>\n							<p ion-text color="primary" class="indexed" *ngIf="document.indeks != null">Indeks : {{ document.indeks}}</p>\n							\n							<h3>Folder : {{ document.code_folder }}</h3>\n							<h3>Box : {{ document.code_box }}</h3>\n\n							<h3>Lemari : {{ document.code_cabinet }}</h3>\n							<p>Uraian : {{ document.uraian}}</p>\n						</div>\n						<button ion-button class="downloadBtn" color="primary" (click)="download(document)" *ngIf="(document.dokumen_file!=null&&document.dokumen_file!=\'\')">\n							<ion-icon name="document"></ion-icon>\n						</button>\n						<br class="clear">\n					</ion-label>\n				</ion-item>\n			</ion-list>\n		</div>\n<!-- \n		<div class="footer">\n			<button class="submit" (click)="scanFolder(document)">Lanjut</button>\n		</div> -->\n\n	</div>\n	<ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n   		<ion-infinite-scroll-content></ion-infinite-scroll-content>\n 	</ion-infinite-scroll>\n\n</div>\n\n</ion-content>\n'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\list-dokumen-by-folder\list-dokumen-by-folder.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_document_document__["a" /* DocumentProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], ListDokumenByFolderPage);
    return ListDokumenByFolderPage;
}());

//# sourceMappingURL=list-dokumen-by-folder.js.map

/***/ }),

/***/ 237:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchByScanPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




/**
 * Generated class for the SearchByScanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SearchByScanPage = /** @class */ (function () {
    function SearchByScanPage(navCtrl, navParams, barcode) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.barcode = barcode;
    }
    SearchByScanPage.prototype.encodeData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.barcode.encode(this.barcode.Encode.TEXT_TYPE, this.dataToEncode)];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SearchByScanPage.prototype.scanBarcode = function () {
        return __awaiter(this, void 0, void 0, function () {
            var options, _a, error_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        options = {
                            prompt: 'Point your camera at a barcode',
                            torchOn: false
                        };
                        _a = this;
                        return [4 /*yield*/, this.barcode.scan(options)];
                    case 1:
                        _a.result = _b.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_2 = _b.sent();
                        console.error(error_2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SearchByScanPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SearchByScanPage');
    };
    SearchByScanPage.prototype.finish = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
    };
    SearchByScanPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    SearchByScanPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-search-by-scan',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\search-by-scan\search-by-scan.html"*/'<div class="scanner-page" data-aos="fade-up" data-aos-duration="500">\n		\n	<div class="scanner-header" data-aos="fade-down" data-aos-duration="1500">\n		<div class="upper">\n			<ion-row>\n		  <button class="back__button" ion-button block (click)="back()">\n				<img src="assets/img/left-arrow.png" alt="">\n			</button>\n		</ion-row>\n		<div class="under">\n			<ion-row>\n				<p>Point your camera towards the QR code <br> to scan</p>\n			</ion-row>\n		</div>\n		</div>\n	</div>\n\n	<ion-content padding>\n		\n		<ion-card *ngIf="result">\n		  <ion-card-content>\n			<p>Text: {{result.text}}</p>\n			<p>Nomor: {{result.id}}</p>\n			<!-- <p>Text: {{result.text}}</p> -->\n			<!-- <p>Format: {{result.format}}</p> -->\n			<!-- <p>Cancelled: {{result.cancelled}}</p> -->\n		  </ion-card-content>\n		</ion-card>\n	  \n		<ion-row>\n		  <button ion-button block (click)="scanBarcode()">SCAN THE BARCODE</button>\n		</ion-row>\n		\n</ion-content>\n\n	<div class="scanner-footer">\n		<div class="under">\n			<button class="submit" (click)="finish()">Selesai</button>\n		</div>\n	</div>\n</div>'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\search-by-scan\search-by-scan.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__["a" /* BarcodeScanner */]])
    ], SearchByScanPage);
    return SearchByScanPage;
}());

//# sourceMappingURL=search-by-scan.js.map

/***/ }),

/***/ 238:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Asignfolder2aPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__asignfolder2_asignfolder2__ = __webpack_require__(212);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the Asignfolder2aPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Asignfolder2aPage = /** @class */ (function () {
    function Asignfolder2aPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    Asignfolder2aPage.prototype.asignfolder2 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__asignfolder2_asignfolder2__["a" /* Asignfolder2Page */]);
    };
    Asignfolder2aPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Asignfolder2aPage');
    };
    Asignfolder2aPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-asignfolder2a',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\asignfolder2a\asignfolder2a.html"*/'\n\n	<div class="body-wrapper menu-wrapper">\n\n		<div class="content">\n			\n			<div class="box-view" data-aos="fade-up" data-aos-duration="500">\n				<div class="search-box">\n					<input type="text" placeholder="Search">\n					<span class="icon">\n						<i class="fas fa-search"></i>\n					</span>\n				</div>\n			</div>\n\n					<div class="list-wrapper">\n					<ul>\n						<li class="slideTrigger">\n						<div class="list">\n							<!-- <img src="../source/img/file-logo.png" alt="" class="logo-icon"> -->\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</div>\n						<div class="behind">\n							<span class="icon"><i class="fas fa-check"></i></span>\n						</div>\n					</li>\n					<li>\n						<ion-list class="list" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n						<li>\n						<ion-list class="list" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n						<li>\n						<ion-list class="list" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n						<li>\n						<ion-list class="list" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n						<li>\n						<ion-list class="list" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n						<li>\n						<ion-list class="list" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n						<li>\n						<ion-list class="list" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n					</ul>\n						</div>\n\n			<div class="footer">\n				<button class="submit" (click)="asignfolder2()">Selesai</button>\n			</div>\n\n		</div>\n\n	</div>\n'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\asignfolder2a\asignfolder2a.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], Asignfolder2aPage);
    return Asignfolder2aPage;
}());

//# sourceMappingURL=asignfolder2a.js.map

/***/ }),

/***/ 239:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Asignbox2Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__asignbox3_asignbox3__ = __webpack_require__(240);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the Asignbox2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Asignbox2Page = /** @class */ (function () {
    function Asignbox2Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    Asignbox2Page.prototype.asignbox3 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__asignbox3_asignbox3__["a" /* Asignbox3Page */]);
    };
    Asignbox2Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Asignbox2Page');
    };
    Asignbox2Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-asignbox2',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\asignbox2\asignbox2.html"*/'	<div class="scanner-page" data-aos="fade-up" data-aos-duration="500">\n		\n		<div class="scanner-header" data-aos="fade-down" data-aos-duration="1500">\n			<div class="upper">\n				<a class="button-back">\n					<span class="icon"><i class="fas fa-arrow-left"></i></span>\n					Back\n				</a>\n				<div class="button-group">\n					<a href="#">\n						<span class="icon"><i class="fas fa-image"></i></span>\n						<span class="icon"><i class="fas fa-bolt"></i></span>\n					</a>\n				</div>\n			</div>\n			<div class="under">\n				<p>Point your camera towards the QR code <br> to scan</p>\n			</div>\n		</div>\n\n		<div class="scanner-footer">\n			<div class="upper">\n				Scanning <span class="is-bold">Box</span>\n			</div>\n			<div class="under">\n				<button class="submit" (click)="asignbox3()">Selanjutnya</button>\n			</div>\n		</div>\n\n	</div>\n\n'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\asignbox2\asignbox2.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], Asignbox2Page);
    return Asignbox2Page;
}());

//# sourceMappingURL=asignbox2.js.map

/***/ }),

/***/ 240:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Asignbox3Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the Asignbox3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Asignbox3Page = /** @class */ (function () {
    function Asignbox3Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    Asignbox3Page.prototype.homesweethomee = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    Asignbox3Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Asignbox3Page');
    };
    Asignbox3Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-asignbox3',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\asignbox3\asignbox3.html"*/'\n	<div class="confirmation-page">\n\n		<div class="upper">\n			\n			<div class="big-card" data-aos="fade-up" data-aos-duration="1500">\n				\n				<img src="assets/img/folder-logo.png" alt="">\n				<img src="assets/img/check-logo.png" alt="" class="check-icon">\n\n				<div class="big-card-title">\n					<span>Great !</span>\n					<br>\n					Folder telah ditambahkan\n				</div>\n\n			</div>\n\n		</div>\n\n		<div class="under">\n			<button class="submit" (click)="homesweethomee()">Selesai</button>\n		</div>\n\n	</div>'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\asignbox3\asignbox3.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], Asignbox3Page);
    return Asignbox3Page;
}());

//# sourceMappingURL=asignbox3.js.map

/***/ }),

/***/ 241:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Asignbox2aPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__asignbox2_asignbox2__ = __webpack_require__(239);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the Asignbox2aPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Asignbox2aPage = /** @class */ (function () {
    function Asignbox2aPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    Asignbox2aPage.prototype.asignbox2 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__asignbox2_asignbox2__["a" /* Asignbox2Page */]);
    };
    Asignbox2aPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Asignbox2aPage');
    };
    Asignbox2aPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-asignbox2a',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\asignbox2a\asignbox2a.html"*/'\n\n	<div class="body-wrapper menu-wrapper">\n\n		<div class="content">\n			\n			<div class="box-view" data-aos="fade-up" data-aos-duration="500">\n				<div class="search-box">\n					<input type="text" placeholder="Search">\n					<span class="icon">\n						<i class="fas fa-search"></i>\n					</span>\n				</div>\n			</div>\n\n					<div class="list-wrapper">\n					<ul>\n						<li class="slideTrigger">\n						<div class="list">\n							<!-- <img src="../source/img/file-logo.png" alt="" class="logo-icon"> -->\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</div>\n						<div class="behind">\n							<span class="icon"><i class="fas fa-check"></i></span>\n						</div>\n					</li>\n					<li>\n						<ion-list class="list" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n						<li>\n						<ion-list class="list" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n						<li>\n						<ion-list class="list" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n						<li>\n						<ion-list class="list" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n						<li>\n						<ion-list class="list" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n						<li>\n						<ion-list class="list" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n						<li>\n						<ion-list class="list" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n					</ul>\n						</div>\n\n			<div class="footer">\n				<button class="submit" (click)="asignbox2()">Selesai</button>\n			</div>\n\n		</div>\n\n	</div>\n'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\asignbox2a\asignbox2a.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], Asignbox2aPage);
    return Asignbox2aPage;
}());

//# sourceMappingURL=asignbox2a.js.map

/***/ }),

/***/ 242:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EntryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_document_document__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the EntryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EntryPage = /** @class */ (function () {
    function EntryPage(navCtrl, navParams, DocumentProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.DocumentProvider = DocumentProvider;
        this.getData();
    }
    EntryPage.prototype.openPage = function (page) {
        this.navCtrl.push(page);
    };
    EntryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EntryPage');
    };
    EntryPage.prototype.getData = function () {
        var _this = this;
        this.DocumentProvider.getDocument().then(function (res) {
            console.log(res);
            _this.document = res;
        }, function (err) {
            console.log("Failure");
        });
    };
    EntryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-entry',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\entry\entry.html"*/'	\n	\n\n	<div class="body-wrapper menu-wrapper">\n			\n		<div class="content">\n			\n			<div class="box" data-aos="fade-down" data-aos-duration="1000">\n				<div class="search-box">\n					<input type="text" placeholder="Search">\n					<span class="icon">\n						<i class="fas fa-search"></i>\n					</span>\n\n				</div>\n\n			</div>\n\n	<!-- 		<div class="card-wrapper pull-up" data-aos="fade-up" data-aos-duration="1000">\n						\n	 -->			<div class="card-wrapper">\n					<ul>\n						<li>\n						<ion-list class="card" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<img class="pagee" src="assets/img/file-logo.png" alt="">\n								\n									Laporan Keuangan\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n\n						<li>\n						<ion-list class="card" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<img class="pagee" src="assets/img/file-logo.png" alt="">\n								\n									Laporan Keuangan\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n					</ul>\n						</div>\n\n			<div class="footer">\n				<button class="submit" (click)="entry2()">Berkas</button>\n			</div>\n\n\n\n		</div>\n\n	</div>'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\entry\entry.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__providers_document_document__["a" /* DocumentProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_document_document__["a" /* DocumentProvider */]])
    ], EntryPage);
    return EntryPage;
}());

//# sourceMappingURL=entry.js.map

/***/ }),

/***/ 243:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Entry3Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__entry_entry__ = __webpack_require__(242);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var Entry3Page = /** @class */ (function () {
    function Entry3Page(navCtrl, navParams, barcode) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.barcode = barcode;
    }
    Entry3Page.prototype.encodeData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.barcode.encode(this.barcode.Encode.TEXT_TYPE, this.dataToEncode)];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    Entry3Page.prototype.scanBarcode = function () {
        return __awaiter(this, void 0, void 0, function () {
            var options, _a, error_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        options = {
                            prompt: 'Point your camera at a barcode',
                            torchOn: true
                        };
                        _a = this;
                        return [4 /*yield*/, this.barcode.scan(options)];
                    case 1:
                        _a.result = _b.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_2 = _b.sent();
                        console.error(error_2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    Entry3Page.prototype.entry = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__entry_entry__["a" /* EntryPage */]);
    };
    Entry3Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Entry3Page');
    };
    Entry3Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-entry3',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\entry3\entry3.html"*/'<div class="scanner-page" data-aos="fade-up" data-aos-duration="500">\n\n		\n\n	<div class="scanner-header" data-aos="fade-down" data-aos-duration="1500">\n\n		<div class="upper">\n\n			<a class="button-back">\n\n				<span class="icon"><i class="fas fa-arrow-left"></i></span>\n\n				Back\n\n			</a>\n\n			<div class="button-group">\n\n				<a href="#">\n\n					<span class="icon"><i class="fas fa-image"></i></span>\n\n					<span class="icon"><i class="fas fa-bolt"></i></span>\n\n				</a>\n\n			</div>\n\n		</div>\n\n		<div class="under">\n\n			<p>Point your camera towards the QR code <br> to scan</p>\n\n		</div>\n\n	</div>\n\n\n\n	<ion-content padding>\n\n\n\n		<ion-card *ngIf="result">\n\n		  <ion-card-content>\n\n			<p>Text: {{result.text}}</p>\n\n			<p>Format: {{result.format}}</p>\n\n			<p>Cancelled: {{result.cancelled}}</p>\n\n		  </ion-card-content>\n\n		</ion-card>\n\n	  \n\n		<ion-row>\n\n		  <button ion-button block (click)="scanBarcode()">Scan</button>\n\n		</ion-row>\n\n		\n\n</ion-content>\n\n\n\n	<div class="scanner-footer">\n\n		<div class="upper">\n\n			Scanning <span class="is-bold">Folder</span>\n\n		</div>\n\n		<div class="under">\n\n			<button class="submit" (click)="entry()">Selesai</button>\n\n		</div>\n\n	</div>\n\n\n\n</div>\n\n\n\n'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\entry3\entry3.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__["a" /* BarcodeScanner */]])
    ], Entry3Page);
    return Entry3Page;
}());

//# sourceMappingURL=lemari.js.map

/***/ }),

/***/ 244:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LemariProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_global__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
  Generated class for the LemariProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var LemariProvider = /** @class */ (function () {
    function LemariProvider(client, http, storage) {
        this.client = client;
        this.http = http;
        this.storage = storage;
        this.apiUrl = __WEBPACK_IMPORTED_MODULE_3__app_global__["a" /* GlobalVariable */].BASE_API_URL;
        this.token = '';
        console.log('Hello LemariProvider Provider');
    }
    LemariProvider.prototype.getLemari = function () {
        var _this = this;
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                // let token = this.storage.get('token');
                //let token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6IndpZG9kbyIsImlkX3VzZXIiOiIzNSIsIm5hbWUiOiJXaWRvZG8gUmlzbmFudHlvIiwiaWRfcm9sZSI6IjMiLCJpZF9kaXZpc2lvbiI6IjEiLCJpZF9yYyI6IjEiLCJpYXQiOjE1NDk0MzUzMzcsImV4cCI6MTU1MDA0MDEzN30.aHF8R3lfyqgnffsNWtKEn7HxVoOvL0fQpthQABy9A6k';
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                // headers.append('', );
                // headers.append('Access-Control-Allow-Origin' , '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
                _this.http.get(_this.apiUrl + 'lemari/' + val.user.id_user, { headers: headers })
                    .subscribe(function (res) {
                    resolve(res);
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    LemariProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClientModule */],
            __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
    ], LemariProvider);
    return LemariProvider;
}());

//# sourceMappingURL=lemari.js.map

/***/ }),

/***/ 245:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(247);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(267);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 267:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_test_test__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common_http__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_asignfolder2_asignfolder2__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_asignfolder2a_asignfolder2a__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_asignbox2_asignbox2__ = __webpack_require__(239);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_asignbox2a_asignbox2a__ = __webpack_require__(241);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_asignfolder3_asignfolder3__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__app_component__ = __webpack_require__(324);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_home_home__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_entry_entry__ = __webpack_require__(242);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_login_login__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_asignfolder_asignfolder__ = __webpack_require__(325);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_asignbox_asignbox__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_asignbox3_asignbox3__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_entry2_entry2__ = __webpack_require__(327);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_entry3_entry3__ = __webpack_require__(243);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_pemberkasan_pemberkasan__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_entry3new_entry3new__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_list_dokumen_list_dokumen__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__providers_document_document__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_dokumen_dokumen__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_assign_box_assign_box__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_assign_folder_assign_folder__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_unarchive_unarchive__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_scan_box_scan_box__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_scan_folder_scan_folder__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__ionic_native_barcode_scanner__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__providers_user_user__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_scan_assign_folder_scan_assign_folder__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__providers_folder_folder__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_peminjaman_peminjaman__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_peminjaman_detail_peminjaman_detail__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__providers_box_box__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_scan_assign_box_scan_assign_box__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__pages_finish_assign_folder_finish_assign_folder__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_finish_assign_box_finish_assign_box__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__providers_berkas_berkas__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_scan_folder_pemberkasan_scan_folder_pemberkasan__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_scan_box_pemberkasan_scan_box_pemberkasan__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_form_peminjaman_form_peminjaman__ = __webpack_require__(231);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_finish_peminjaman_finish_peminjaman__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pages_list_dokumen_new_list_dokumen_new__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__pages_berkas_berkas__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__pages_scan_lemari_scan_lemari__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__providers_peminjaman_peminjaman__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__pages_scan_assign_lemari_scan_assign_lemari__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__pages_list_dokumen_berkas_list_dokumen_berkas__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__pages_lemari_lemari__ = __webpack_require__(330);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__providers_lemari_lemari__ = __webpack_require__(244);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__pages_lemari_box_lemari_box__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__pages_list_folder_by_box_list_folder_by_box__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__pages_list_dokumen_by_folder_list_dokumen_by_folder__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__pages_search_by_scan_search_by_scan__ = __webpack_require__(237);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















































//import { FileTransfer,  FileTransferObject } from '@ionic-native/file-transfer';


// import { File } from '@ionic-native/file';








var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_13__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_entry_entry__["a" /* EntryPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_asignfolder2_asignfolder2__["a" /* Asignfolder2Page */],
                __WEBPACK_IMPORTED_MODULE_8__pages_asignfolder2a_asignfolder2a__["a" /* Asignfolder2aPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_asignfolder_asignfolder__["a" /* AsignfolderPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_asignbox_asignbox__["a" /* AsignboxPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_asignbox2_asignbox2__["a" /* Asignbox2Page */],
                __WEBPACK_IMPORTED_MODULE_10__pages_asignbox2a_asignbox2a__["a" /* Asignbox2aPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_pemberkasan_pemberkasan__["a" /* PemberkasanPage */],
                __WEBPACK_IMPORTED_MODULE_49__pages_scan_lemari_scan_lemari__["a" /* ScanLemariPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_test_test__["a" /* TestPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_entry2_entry2__["a" /* Entry2Page */],
                __WEBPACK_IMPORTED_MODULE_51__pages_scan_assign_lemari_scan_assign_lemari__["a" /* ScanAssignLemariPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_entry3_entry3__["a" /* Entry3Page */],
                __WEBPACK_IMPORTED_MODULE_11__pages_asignfolder3_asignfolder3__["a" /* Asignfolder3Page */],
                __WEBPACK_IMPORTED_MODULE_18__pages_asignbox3_asignbox3__["a" /* Asignbox3Page */],
                __WEBPACK_IMPORTED_MODULE_22__pages_entry3new_entry3new__["a" /* Entry3newPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_unarchive_unarchive__["a" /* UnarchivePage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_scan_box_scan_box__["a" /* ScanBoxPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_scan_folder_scan_folder__["a" /* ScanFolderPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_assign_folder_assign_folder__["a" /* AssignFolderPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_scan_assign_folder_scan_assign_folder__["a" /* ScanAssignFolderPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_assign_box_assign_box__["a" /* AssignBoxPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_list_dokumen_new_list_dokumen_new__["a" /* ListDokumenNewPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_scan_assign_box_scan_assign_box__["a" /* ScanAssignBoxPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_finish_assign_box_finish_assign_box__["a" /* FinishAssignBoxPage */],
                __WEBPACK_IMPORTED_MODULE_40__pages_finish_assign_folder_finish_assign_folder__["a" /* FinishAssignFolderPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_dokumen_dokumen__["a" /* DokumenPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_list_dokumen_list_dokumen__["a" /* ListDokumenPage */],
                __WEBPACK_IMPORTED_MODULE_55__pages_lemari_box_lemari_box__["a" /* LemariBoxPage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_scan_folder_pemberkasan_scan_folder_pemberkasan__["a" /* ScanFolderPemberkasanPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_scan_box_pemberkasan_scan_box_pemberkasan__["a" /* ScanBoxPemberkasanPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_peminjaman_peminjaman__["a" /* PeminjamanPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_form_peminjaman_form_peminjaman__["a" /* FormPeminjamanPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_finish_peminjaman_finish_peminjaman__["a" /* FinishPeminjamanPage */],
                __WEBPACK_IMPORTED_MODULE_48__pages_berkas_berkas__["a" /* BerkasPage */],
                __WEBPACK_IMPORTED_MODULE_53__pages_lemari_lemari__["a" /* LemariPage */],
                __WEBPACK_IMPORTED_MODULE_52__pages_list_dokumen_berkas_list_dokumen_berkas__["a" /* ListDokumenBerkasPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_peminjaman_detail_peminjaman_detail__["a" /* PeminjamanDetailPage */],
                __WEBPACK_IMPORTED_MODULE_56__pages_list_folder_by_box_list_folder_by_box__["a" /* ListFolderByBoxPage */],
                __WEBPACK_IMPORTED_MODULE_57__pages_list_dokumen_by_folder_list_dokumen_by_folder__["a" /* ListDokumenByFolderPage */],
                __WEBPACK_IMPORTED_MODULE_58__pages_search_by_scan_search_by_scan__["a" /* SearchByScanPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { component: __WEBPACK_IMPORTED_MODULE_15__pages_login_login__["a" /* LoginPage */], name: 'LoginPage', segment: 'login' },
                        { component: __WEBPACK_IMPORTED_MODULE_13__pages_home_home__["a" /* HomePage */], name: 'Home', segment: 'home' },
                        { component: __WEBPACK_IMPORTED_MODULE_28__pages_unarchive_unarchive__["a" /* UnarchivePage */], name: 'Unarchive', segment: 'unarchive' },
                        { component: __WEBPACK_IMPORTED_MODULE_26__pages_assign_box_assign_box__["a" /* AssignBoxPage */], name: 'Assign Box', segment: 'assign-box' },
                        { component: __WEBPACK_IMPORTED_MODULE_53__pages_lemari_lemari__["a" /* LemariPage */], name: 'Lemari', segment: 'lemari' },
                        { component: __WEBPACK_IMPORTED_MODULE_27__pages_assign_folder_assign_folder__["a" /* AssignFolderPage */], name: 'Assign Folder', segment: 'assign-folder' },
                        { component: __WEBPACK_IMPORTED_MODULE_49__pages_scan_lemari_scan_lemari__["a" /* ScanLemariPage */], name: 'Scan Lemari', segment: 'scanLemari' },
                        { component: __WEBPACK_IMPORTED_MODULE_21__pages_pemberkasan_pemberkasan__["a" /* PemberkasanPage */], name: 'Pemberkasan', segment: 'pemberkasan' },
                        { component: __WEBPACK_IMPORTED_MODULE_25__pages_dokumen_dokumen__["a" /* DokumenPage */], name: 'Dokumen', segment: 'dokumen' },
                        { component: __WEBPACK_IMPORTED_MODULE_48__pages_berkas_berkas__["a" /* BerkasPage */], name: 'Berkas', segment: 'berkas' },
                        { component: __WEBPACK_IMPORTED_MODULE_36__pages_peminjaman_peminjaman__["a" /* PeminjamanPage */], name: 'Peminjaman', segment: 'peminjaman' },
                        { component: __WEBPACK_IMPORTED_MODULE_37__pages_peminjaman_detail_peminjaman_detail__["a" /* PeminjamanDetailPage */], name: 'Detail Peminjaman', segment: 'detailPeminjaman' },
                        { component: __WEBPACK_IMPORTED_MODULE_45__pages_form_peminjaman_form_peminjaman__["a" /* FormPeminjamanPage */], name: 'Form Peminjaman', segment: 'formPeminjaman' },
                        { component: __WEBPACK_IMPORTED_MODULE_30__pages_scan_folder_scan_folder__["a" /* ScanFolderPage */], name: 'Scan Folder', segment: 'scanfolder' },
                        { component: __WEBPACK_IMPORTED_MODULE_34__pages_scan_assign_folder_scan_assign_folder__["a" /* ScanAssignFolderPage */], name: 'Scan Assign Folder', segment: 'scanassignfolder', defaultHistory: [__WEBPACK_IMPORTED_MODULE_27__pages_assign_folder_assign_folder__["a" /* AssignFolderPage */]] },
                        { component: __WEBPACK_IMPORTED_MODULE_29__pages_scan_box_scan_box__["a" /* ScanBoxPage */], name: 'Scan Box', segment: 'scanbox', defaultHistory: [__WEBPACK_IMPORTED_MODULE_28__pages_unarchive_unarchive__["a" /* UnarchivePage */]] },
                        { component: __WEBPACK_IMPORTED_MODULE_23__pages_list_dokumen_list_dokumen__["a" /* ListDokumenPage */], name: 'List Unarchive Doccument', segment: 'unarchiveDocument', defaultHistory: [__WEBPACK_IMPORTED_MODULE_21__pages_pemberkasan_pemberkasan__["a" /* PemberkasanPage */]] },
                        { component: __WEBPACK_IMPORTED_MODULE_44__pages_scan_box_pemberkasan_scan_box_pemberkasan__["a" /* ScanBoxPemberkasanPage */], name: 'Scan Box Pemberkasan ', segment: 'scanboxpemberkasan', defaultHistory: [__WEBPACK_IMPORTED_MODULE_21__pages_pemberkasan_pemberkasan__["a" /* PemberkasanPage */]] },
                        { component: __WEBPACK_IMPORTED_MODULE_43__pages_scan_folder_pemberkasan_scan_folder_pemberkasan__["a" /* ScanFolderPemberkasanPage */], name: 'Scan Folder Pemberkasan ', segment: 'scanfolderpemberkasan', defaultHistory: [__WEBPACK_IMPORTED_MODULE_21__pages_pemberkasan_pemberkasan__["a" /* PemberkasanPage */]] },
                        { component: __WEBPACK_IMPORTED_MODULE_58__pages_search_by_scan_search_by_scan__["a" /* SearchByScanPage */], name: 'Search By Scan', segment: 'search-by-scan' },
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_6__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_33__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */])
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_13__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_entry_entry__["a" /* EntryPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_unarchive_unarchive__["a" /* UnarchivePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_asignfolder2_asignfolder2__["a" /* Asignfolder2Page */],
                __WEBPACK_IMPORTED_MODULE_8__pages_asignfolder2a_asignfolder2a__["a" /* Asignfolder2aPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_asignbox3_asignbox3__["a" /* Asignbox3Page */],
                __WEBPACK_IMPORTED_MODULE_11__pages_asignfolder3_asignfolder3__["a" /* Asignfolder3Page */],
                __WEBPACK_IMPORTED_MODULE_16__pages_asignfolder_asignfolder__["a" /* AsignfolderPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_asignbox_asignbox__["a" /* AsignboxPage */],
                __WEBPACK_IMPORTED_MODULE_51__pages_scan_assign_lemari_scan_assign_lemari__["a" /* ScanAssignLemariPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_asignbox2_asignbox2__["a" /* Asignbox2Page */],
                __WEBPACK_IMPORTED_MODULE_10__pages_asignbox2a_asignbox2a__["a" /* Asignbox2aPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_test_test__["a" /* TestPage */],
                __WEBPACK_IMPORTED_MODULE_55__pages_lemari_box_lemari_box__["a" /* LemariBoxPage */],
                __WEBPACK_IMPORTED_MODULE_49__pages_scan_lemari_scan_lemari__["a" /* ScanLemariPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_list_dokumen_new_list_dokumen_new__["a" /* ListDokumenNewPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_entry2_entry2__["a" /* Entry2Page */],
                __WEBPACK_IMPORTED_MODULE_52__pages_list_dokumen_berkas_list_dokumen_berkas__["a" /* ListDokumenBerkasPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_entry3_entry3__["a" /* Entry3Page */],
                __WEBPACK_IMPORTED_MODULE_22__pages_entry3new_entry3new__["a" /* Entry3newPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_scan_box_scan_box__["a" /* ScanBoxPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_scan_folder_scan_folder__["a" /* ScanFolderPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_assign_folder_assign_folder__["a" /* AssignFolderPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_scan_assign_folder_scan_assign_folder__["a" /* ScanAssignFolderPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_list_dokumen_list_dokumen__["a" /* ListDokumenPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_assign_box_assign_box__["a" /* AssignBoxPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_scan_assign_box_scan_assign_box__["a" /* ScanAssignBoxPage */],
                __WEBPACK_IMPORTED_MODULE_40__pages_finish_assign_folder_finish_assign_folder__["a" /* FinishAssignFolderPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_finish_assign_box_finish_assign_box__["a" /* FinishAssignBoxPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_dokumen_dokumen__["a" /* DokumenPage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_scan_folder_pemberkasan_scan_folder_pemberkasan__["a" /* ScanFolderPemberkasanPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_scan_box_pemberkasan_scan_box_pemberkasan__["a" /* ScanBoxPemberkasanPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_peminjaman_peminjaman__["a" /* PeminjamanPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_form_peminjaman_form_peminjaman__["a" /* FormPeminjamanPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_peminjaman_detail_peminjaman_detail__["a" /* PeminjamanDetailPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_finish_peminjaman_finish_peminjaman__["a" /* FinishPeminjamanPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_pemberkasan_pemberkasan__["a" /* PemberkasanPage */],
                __WEBPACK_IMPORTED_MODULE_48__pages_berkas_berkas__["a" /* BerkasPage */],
                __WEBPACK_IMPORTED_MODULE_53__pages_lemari_lemari__["a" /* LemariPage */],
                __WEBPACK_IMPORTED_MODULE_57__pages_list_dokumen_by_folder_list_dokumen_by_folder__["a" /* ListDokumenByFolderPage */],
                __WEBPACK_IMPORTED_MODULE_56__pages_list_folder_by_box_list_folder_by_box__["a" /* ListFolderByBoxPage */],
                __WEBPACK_IMPORTED_MODULE_58__pages_search_by_scan_search_by_scan__["a" /* SearchByScanPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_24__providers_document_document__["a" /* DocumentProvider */],
                __WEBPACK_IMPORTED_MODULE_31__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
                __WEBPACK_IMPORTED_MODULE_32__providers_user_user__["a" /* UserProvider */],
                __WEBPACK_IMPORTED_MODULE_35__providers_folder_folder__["a" /* FolderProvider */],
                __WEBPACK_IMPORTED_MODULE_38__providers_box_box__["a" /* BoxProvider */],
                __WEBPACK_IMPORTED_MODULE_42__providers_berkas_berkas__["a" /* BerkasProvider */],
                __WEBPACK_IMPORTED_MODULE_50__providers_peminjaman_peminjaman__["a" /* PeminjamanProvider */],
                //FileTransfer,
                // File,
                __WEBPACK_IMPORTED_MODULE_54__providers_lemari_lemari__["a" /* LemariProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 316:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TestPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the TestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TestPage = /** @class */ (function () {
    function TestPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    TestPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TestPage');
    };
    TestPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-test',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\test\test.html"*/'<!--\n  Generated template for the TestPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>test</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n	<ion-card>\n<ion-list>\n  <ion-item-sliding #item>\n    <ion-item>\n      Item\n    </ion-item>\n    <ion-item-options side="right">\n      <ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n    </ion-item-options>\n  </ion-item-sliding>\n\n</ion-list>\n</ion-card>\n</ion-content>\n'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\test\test.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], TestPage);
    return TestPage;
}());

//# sourceMappingURL=test.js.map

/***/ }),

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__user_user__ = __webpack_require__(234);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__user_user__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ 325:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(116);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// import { route } from '@angular/route';
var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 326:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AsignfolderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__asignfolder2a_asignfolder2a__ = __webpack_require__(238);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the AsignfolderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AsignfolderPage = /** @class */ (function () {
    function AsignfolderPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AsignfolderPage.prototype.asignfolder2a = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__asignfolder2a_asignfolder2a__["a" /* Asignfolder2aPage */]);
    };
    AsignfolderPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AsignfolderPage');
    };
    AsignfolderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-asignfolder',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\asignfolder\asignfolder.html"*/'	\n\n	\n\n\n\n	<div class="body-wrapper menu-wrapper">\n\n			\n\n		<div class="content">\n\n			\n\n			<div class="box" data-aos="fade-down" data-aos-duration="1000">\n\n				<div class="search-box">\n\n					<input type="text" placeholder="Search">\n\n					<span class="icon">\n\n						<i class="fas fa-search"></i>\n\n					</span>\n\n\n\n				</div>\n\n\n\n			</div>\n\n\n\n	<!-- 		<div class="card-wrapper pull-up" data-aos="fade-up" data-aos-duration="1000">\n\n						\n\n	 -->			<div class="card-wrapper">\n\n					<ul>\n\n						<li>\n\n						<ion-list class="card" no-lines>\n\n						<ion-item-sliding #item>\n\n								\n\n						<ion-item>\n\n							<img class="pagee" src="assets/img/file-logo.png" alt="">\n\n								\n\n									Laporan Keuangan\n\n						</ion-item>\n\n						<ion-item-options side="right">\n\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n\n						</ion-item-options>\n\n\n\n						</ion-item-sliding>\n\n						</ion-list>\n\n						</li>\n\n\n\n						<li>\n\n						<ion-list class="card" no-lines>\n\n						<ion-item-sliding #item>\n\n								\n\n						<ion-item>\n\n							<img class="pagee" src="assets/img/file-logo.png" alt="">\n\n								\n\n									Laporan Keuangan\n\n						</ion-item>\n\n						<ion-item-options side="right">\n\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n\n						</ion-item-options>\n\n\n\n						</ion-item-sliding>\n\n						</ion-list>\n\n						</li>\n\n					</ul>\n\n						</div>\n\n		</div>\n\n\n\n	</div>\n\n\n\n	<ion-footer>\n\n	  <ion-toolbar>\n\n	    <button class="submit" (click)="scanFolder(document)">Lanjut</button>\n\n	  </ion-toolbar>\n\n	</ion-footer>'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\asignfolder\asignfolder.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], AsignfolderPage);
    return AsignfolderPage;
}());

//# sourceMappingURL=asignfolder.js.map

/***/ }),

/***/ 327:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AsignboxPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__asignbox2a_asignbox2a__ = __webpack_require__(241);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_document_document__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the AsignboxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AsignboxPage = /** @class */ (function () {
    function AsignboxPage(navCtrl, navParams, DocumentProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.DocumentProvider = DocumentProvider;
        console.log('e');
        this.getData();
    }
    AsignboxPage.prototype.asignbox2a = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__asignbox2a_asignbox2a__["a" /* Asignbox2aPage */]);
    };
    AsignboxPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AsignboxPage');
    };
    AsignboxPage.prototype.getData = function () {
        var _this = this;
        this.DocumentProvider.getDocument().then(function (res) {
            console.log(res);
            _this.document = res;
        }, function (err) {
            console.log("Failure");
        });
    };
    AsignboxPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-asignbox',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\asignbox\asignbox.html"*/'	\n	\n\n	<div class="body-wrapper menu-wrapper">\n			\n		<div class="content">\n			\n			<div class="box" data-aos="fade-down" data-aos-duration="1000">\n				<div class="search-box">\n					<input type="text" placeholder="Search">\n					<span class="icon">\n						<i class="fas fa-search"></i>\n					</span>\n\n				</div>\n\n			</div>\n					<div class="card-wrapper">\n					<ul>\n						<li>\n						<ion-list class="card" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<img class="pagee" src="assets/img/file-logo.png" alt="">\n								\n									Laporan Keuangan\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n\n						<li>\n						<ion-list class="card" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<img class="pagee" src="assets/img/file-logo.png" alt="">\n								\n									Laporan Keuangan\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n					</ul>\n						</div>\n\n			<div class="footer">\n				<button class="submit" (click)="asignbox2a()">Berkas</button>\n			</div>\n\n\n\n		</div>\n\n	</div>'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\asignbox\asignbox.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_3__providers_document_document__["a" /* DocumentProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_document_document__["a" /* DocumentProvider */]])
    ], AsignboxPage);
    return AsignboxPage;
}());

//# sourceMappingURL=asignbox.js.map

/***/ }),

/***/ 328:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Entry2Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__entry3_entry3__ = __webpack_require__(243);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the Entry2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Entry2Page = /** @class */ (function () {
    function Entry2Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    Entry2Page.prototype.entry3 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__entry3_entry3__["a" /* Entry3Page */]);
    };
    Entry2Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Entry2Page');
    };
    Entry2Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-entry2',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\entry2\entry2.html"*/'\n\n	<div class="body-wrapper menu-wrapper">\n\n		<div class="content">\n			\n			<div class="box-view" data-aos="fade-up" data-aos-duration="500">\n				<div class="search-box">\n					<input type="text" placeholder="Search">\n					<span class="icon">\n						<i class="fas fa-search"></i>\n					</span>\n				</div>\n			</div>\n\n					<div class="list-wrapper">\n					<ul>\n						<li class="slideTrigger">\n						<div class="list">\n							<!-- <img src="../source/img/file-logo.png" alt="" class="logo-icon"> -->\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</div>\n						<div class="behind">\n							<span class="icon"><i class="fas fa-check"></i></span>\n						</div>\n					</li>\n					<li>\n						<ion-list class="list" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n					</li>\n						<li>\n						<ion-list class="list" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n						<li>\n						<ion-list class="list" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n						<li>\n						<ion-list class="list" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n						<li>\n						<ion-list class="list" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n						<li>\n						<ion-list class="list" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n						<li>\n						<ion-list class="list" no-lines>\n						<ion-item-sliding #item>\n								\n						<ion-item>\n							<div class="title-list is-bold">\n								<div class="text">\n									Title Here\n									<span class="sub-title">Any Possible subtitle here</span>\n								</div>\n								<span class="circle-icon"></span>\n							</div>\n						</ion-item>\n						<ion-item-options side="right">\n						<ion-checkbox [(ngModel)]="pepperoni"></ion-checkbox>\n						</ion-item-options>\n\n						</ion-item-sliding>\n						</ion-list>\n						</li>\n					</ul>\n						</div>\n\n			<div class="footer">\n				<button class="submit" (click)="entry3()">Selesai</button>\n			</div>\n\n		</div>\n\n	</div>\n'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\entry2\entry2.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], Entry2Page);
    return Entry2Page;
}());

//# sourceMappingURL=entry2.js.map

/***/ }),

/***/ 329:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScanAssignBoxPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__entry3new_entry3new__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




/**
 * Generated class for the ScanAssignBoxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ScanAssignBoxPage = /** @class */ (function () {
    function ScanAssignBoxPage(navCtrl, navParams, barcode) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.barcode = barcode;
    }
    ScanAssignBoxPage.prototype.encodeData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.barcode.encode(this.barcode.Encode.TEXT_TYPE, this.dataToEncode)];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.error(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ScanAssignBoxPage.prototype.scanBarcode = function () {
        return __awaiter(this, void 0, void 0, function () {
            var options, _a, error_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        options = {
                            prompt: 'Point your camera at a barcode',
                            torchOn: false
                        };
                        _a = this;
                        return [4 /*yield*/, this.barcode.scan(options)];
                    case 1:
                        _a.result = _b.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_2 = _b.sent();
                        console.error(error_2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ScanAssignBoxPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ScanAssignBoxPage');
    };
    ScanAssignBoxPage.prototype.finish = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__entry3new_entry3new__["a" /* Entry3newPage */]);
    };
    ScanAssignBoxPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    ScanAssignBoxPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-scan-assign-box',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\scan-assign-box\scan-assign-box.html"*/'<div class="scanner-page" data-aos="fade-up" data-aos-duration="500">\n\n		\n\n	<div class="scanner-header" data-aos="fade-down" data-aos-duration="1500">\n\n		<div class="upper">\n\n			<ion-row>\n\n		  <button class="back__button" ion-button block (click)="back()">\n\n				<img src="assets/img/left-arrow.png" alt="">\n\n			</button>\n\n		</ion-row>\n\n		<div class="under">\n\n			<ion-row>\n\n				<p>Point your camera towards the QR code <br> to scan</p>\n\n			</ion-row>\n\n		</div>\n\n		</div>\n\n	</div>\n\n\n\n	<ion-content padding>\n\n		\n\n		<ion-card *ngIf="result">\n\n		  <ion-card-content>\n\n			<p>Text: {{result.text}}</p>\n\n			<p>Format: {{result.format}}</p>\n\n			<p>Cancelled: {{result.cancelled}}</p>\n\n		  </ion-card-content>\n\n		</ion-card>\n\n	  \n\n		<ion-row>\n\n		  <button ion-button block (click)="scanBarcode()">SCAN BOX</button>\n\n		</ion-row>\n\n		\n\n</ion-content>\n\n\n\n	<div class="scanner-footer">\n\n		<div class="under">\n\n			<button class="submit" (click)="finish()">Selesai</button>\n\n		</div>\n\n	</div>\n\n</div>'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\scan-assign-box\scan-assign-box.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__["a" /* BarcodeScanner */]])
    ], ScanAssignBoxPage);
    return ScanAssignBoxPage;
}());

//# sourceMappingURL=scan-assign-box.js.map

/***/ }),

/***/ 330:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScanLemariPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ScanLemariPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ScanLemariPage = /** @class */ (function () {
    function ScanLemariPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ScanLemariPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ScanLemariPage');
    };
    ScanLemariPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-scan-lemari',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\scan-lemari\scan-lemari.html"*/'<!--\n\n  Generated template for the ScanLemariPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>scanLemari</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\scan-lemari\scan-lemari.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], ScanLemariPage);
    return ScanLemariPage;
}());

//# sourceMappingURL=scan-lemari.js.map

/***/ }),

/***/ 330:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LemariPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_lemari_lemari__ = __webpack_require__(244);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__lemari_box_lemari_box__ = __webpack_require__(117);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the LemariPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LemariPage = /** @class */ (function () {
    function LemariPage(navCtrl, navParams, LemariProvider, lc) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.LemariProvider = LemariProvider;
        this.lc = lc;
        this.loader = this.lc.create();
        this.loader.present();
        this.getData();
    }
    LemariPage.prototype.getData = function () {
        var _this = this;
        this.LemariProvider.getLemari().then(function (res) {
            _this.cupboards = res;
            _this.loader.dismiss();
        }, function (err) {
            console.log("Failure");
        });
    };
    LemariPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LemariPage');
    };
    LemariPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    LemariPage.prototype.next = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__lemari_box_lemari_box__["a" /* LemariBoxPage */]);
    };
    LemariPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-lemari',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\lemari\lemari.html"*/'<ion-content>\n\n\n\n  <div class="body-wrapper menu-wrapper">\n\n\n\n	<div class="content">\n\n		\n\n		<div class="box-view" data-aos="fade-up" data-aos-duration="500">\n\n				<button class="back__button" ion-button block (click)="back()">\n\n					<img src="assets/img/left-arrow.png" alt="">\n\n				</button>\n\n				<ion-searchbar type="text" placeholder="Search" (ionInput)="getItems($event)" (ionClear)="reset($event)" (ngModel)="searchKey"></ion-searchbar>\n\n		</div>\n\n\n\n		<div class="list-wrapper">\n\n			<ion-list>\n\n				<ion-item *ngFor="let cupboard of cupboards" (click) ="next(cupboard.id)" >\n\n					<ion-label class="label__list">\n\n						<!-- <div class="icon__wrapper">\n\n					  		<img src="assets/img/file.png" alt="">\n\n					  	</div> -->\n\n						<div class="icon__text">\n\n							<h2>Lemari : {{ (cupboard.code==null) ? \'--\' : cupboard.code}}</h2>\n\n							<!-- <h3>Record Center : {{ cupboard.record_center }}</h3> -->\n\n						</div>\n\n						<!-- <button ion-button class="downloadBtn" color="primary" (click)="download(document)" *ngIf="(document.dokumen_file!=null||document.dokumen_file!=\'\')">\n\n							<ion-icon name="document"></ion-icon>\n\n						</button> -->\n\n						<br class="clear">\n\n					</ion-label>\n\n				</ion-item>\n\n			</ion-list>\n\n		</div>\n\n<!-- \n\n		<div class="footer">\n\n			<button class="submit" (click)="scanFolder(document)">Lanjut</button>\n\n		</div> -->\n\n\n\n	</div>\n\n\n\n</div>\n\n<!-- <ion-fab right bottom>\n\n	<button ion-fab color="blue" (click)="scan()"><ion-icon name="camera"></ion-icon></button>\n\n</ion-fab> -->\n\n</ion-content>\n\n'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\lemari\lemari.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__providers_lemari_lemari__["a" /* LemariProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_lemari_lemari__["a" /* LemariProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], LemariPage);
    return LemariPage;
}());

//# sourceMappingURL=lemari.js.map

/***/ }),

/***/ 330:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LemariPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_lemari_lemari__ = __webpack_require__(244);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__lemari_box_lemari_box__ = __webpack_require__(117);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the LemariPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LemariPage = /** @class */ (function () {
    function LemariPage(navCtrl, navParams, LemariProvider, lc) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.LemariProvider = LemariProvider;
        this.lc = lc;
        this.loader = this.lc.create();
        this.loader.present();
        this.getData();
    }
    LemariPage.prototype.getData = function () {
        var _this = this;
        this.LemariProvider.getLemari().then(function (res) {
            _this.cupboards = res;
            _this.loader.dismiss();
        }, function (err) {
            console.log("Failure");
        });
    };
    LemariPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LemariPage');
    };
    LemariPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    LemariPage.prototype.next = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__lemari_box_lemari_box__["a" /* LemariBoxPage */]);
    };
    LemariPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-lemari',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\lemari\lemari.html"*/'<ion-content>\n\n\n\n  <div class="body-wrapper menu-wrapper">\n\n\n\n	<div class="content">\n\n		\n\n		<div class="box-view" data-aos="fade-up" data-aos-duration="500">\n\n				<button class="back__button" ion-button block (click)="back()">\n\n					<img src="assets/img/left-arrow.png" alt="">\n\n				</button>\n\n				<ion-searchbar type="text" placeholder="Search" (ionInput)="getItems($event)" (ionClear)="reset($event)" (ngModel)="searchKey"></ion-searchbar>\n\n		</div>\n\n\n\n		<div class="list-wrapper">\n\n			<ion-list>\n\n				<ion-item *ngFor="let cupboard of cupboards" (click) ="next(cupboard.id)" >\n\n					<ion-label class="label__list">\n\n						<!-- <div class="icon__wrapper">\n\n					  		<img src="assets/img/file.png" alt="">\n\n					  	</div> -->\n\n						<div class="icon__text">\n\n							<h2>Lemari : {{ (cupboard.code==null) ? \'--\' : cupboard.code}}</h2>\n\n							<!-- <h3>Record Center : {{ cupboard.record_center }}</h3> -->\n\n						</div>\n\n						<!-- <button ion-button class="downloadBtn" color="primary" (click)="download(document)" *ngIf="(document.dokumen_file!=null||document.dokumen_file!=\'\')">\n\n							<ion-icon name="document"></ion-icon>\n\n						</button> -->\n\n						<br class="clear">\n\n					</ion-label>\n\n				</ion-item>\n\n			</ion-list>\n\n		</div>\n\n<!-- \n\n		<div class="footer">\n\n			<button class="submit" (click)="scanFolder(document)">Lanjut</button>\n\n		</div> -->\n\n\n\n	</div>\n\n\n\n</div>\n\n<!-- <ion-fab right bottom>\n\n	<button ion-fab color="blue" (click)="scan()"><ion-icon name="camera"></ion-icon></button>\n\n</ion-fab> -->\n\n</ion-content>\n\n'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\lemari\lemari.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__providers_lemari_lemari__["a" /* LemariProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_lemari_lemari__["a" /* LemariProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], LemariPage);
    return LemariPage;
}());

//# sourceMappingURL=lemari.js.map

/***/ }),

/***/ 34:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobalVariable; });
// export const GlobalVariable = Object.freeze({
//      BASE_API_LOGIN: 'https://dox.pgnmas.co.id/rest/',
//      BASE_API_URL: 'https://dox.pgnmas.co.id/api/',
//     //  ... more of your variables
//  });
var GlobalVariable = Object.freeze({
    BASE_API_LOGIN: 'http://dekodr.co.id/dox-backend/rest/',
    BASE_API_URL: 'http://dekodr.co.id/dox-backend/api/',
});
//# sourceMappingURL=global.js.map

/***/ }),

/***/ 48:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Entry3newPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the Entry3newPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Entry3newPage = /** @class */ (function () {
    function Entry3newPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    Entry3newPage.prototype.homesweethome = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    Entry3newPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Entry3newPage');
    };
    Entry3newPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-entry3new',template:/*ion-inline-start:"c:\xampp\htdocs\dox-mobile\src\pages\entry3new\entry3new.html"*/'\n\n	<div class="confirmation-page">\n\n\n\n		<div class="upper">\n\n			\n\n			<div class="big-card" data-aos="fade-up" data-aos-duration="1500">\n\n				\n\n				<!-- <img src="assets/img/box-logo.png" alt=""> -->\n\n				<img src="assets/img/check-logo.png" alt="">\n\n\n\n				<div class="big-card-title">\n\n					<span>Great !</span>\n\n					<br>\n\n					Folder & Box telah ditambahkan\n\n				</div>\n\n\n\n			</div>\n\n\n\n		</div>\n\n\n\n		<div class="under">\n\n			<button class="submit" (click)="homesweethome()">Selesai</button>\n\n		</div>\n\n\n\n	</div>'/*ion-inline-end:"c:\xampp\htdocs\dox-mobile\src\pages\entry3new\entry3new.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], Entry3newPage);
    return Entry3newPage;
}());

//# sourceMappingURL=folder.js.map

/***/ }),

/***/ 58:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FolderProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_global__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
  Generated class for the FolderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var FolderProvider = /** @class */ (function () {
    function FolderProvider(client, http, storage) {
        this.client = client;
        this.http = http;
        this.storage = storage;
        this.apiUrl = __WEBPACK_IMPORTED_MODULE_3__app_global__["a" /* GlobalVariable */].BASE_API_URL;
        this.token = '';
        console.log('Hello FolderProvider Provider');
    }
    FolderProvider.prototype.getFolder = function (page, q) {
        var _this = this;
        if (page === void 0) { page = 0; }
        if (q === void 0) { q = ''; }
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                // let token = this.storage.get('token');
                //let token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6IndpZG9kbyIsImlkX3VzZXIiOiIzNSIsIm5hbWUiOiJXaWRvZG8gUmlzbmFudHlvIiwiaWRfcm9sZSI6IjMiLCJpZF9kaXZpc2lvbiI6IjEiLCJpZF9yYyI6IjEiLCJpYXQiOjE1NDk0MzUzMzcsImV4cCI6MTU1MDA0MDEzN30.aHF8R3lfyqgnffsNWtKEn7HxVoOvL0fQpthQABy9A6k';
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                // headers.append('', );
                // headers.append('Access-Control-Allow-Origin' , '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
                _this.http.get(_this.apiUrl + 'folder/page/' + page + '/code/' + q, { headers: headers })
                    .subscribe(function (res) {
                    resolve(res);
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    FolderProvider.prototype.getFolderByBox = function (box, page, q) {
        var _this = this;
        if (page === void 0) { page = 0; }
        if (q === void 0) { q = ''; }
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                // let token = this.storage.get('token');
                //let token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6IndpZG9kbyIsImlkX3VzZXIiOiIzNSIsIm5hbWUiOiJXaWRvZG8gUmlzbmFudHlvIiwiaWRfcm9sZSI6IjMiLCJpZF9kaXZpc2lvbiI6IjEiLCJpZF9yYyI6IjEiLCJpYXQiOjE1NDk0MzUzMzcsImV4cCI6MTU1MDA0MDEzN30.aHF8R3lfyqgnffsNWtKEn7HxVoOvL0fQpthQABy9A6k';
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                // headers.append('', );
                // headers.append('Access-Control-Allow-Origin' , '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
                _this.http.get(_this.apiUrl + 'folder/box/' + box + '/page/' + page + '/code/' + q, { headers: headers })
                    .subscribe(function (res) {
                    resolve(res);
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    FolderProvider.prototype.postEntry = function (data) {
        var _this = this;
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                headers.append('Access-Control-Allow-Origin', '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
                var listIdFolder = [];
                data.folder.forEach(function (item) {
                    if (item.value) {
                        listIdFolder.push(item.id);
                    }
                });
                var data_send = { id_folder: listIdFolder, id_box: data.box };
                _this.http.post(_this.apiUrl + 'folder', JSON.stringify(data_send), { headers: headers })
                    .subscribe(function (res) {
                    resolve(res);
                }, function (err) {
                    // console.log(JSON.stringify(err));
                    // console.log(JSON.stringify(this.rows));
                    reject(err);
                });
            });
        });
    };
    FolderProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClientModule */],
            __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
    ], FolderProvider);
    return FolderProvider;
}());

//# sourceMappingURL=folder.js.map

/***/ }),

/***/ 59:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BoxProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_global__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
  Generated class for the BoxProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var BoxProvider = /** @class */ (function () {
    function BoxProvider(client, http, storage) {
        this.client = client;
        this.http = http;
        this.storage = storage;
        this.apiUrl = __WEBPACK_IMPORTED_MODULE_3__app_global__["a" /* GlobalVariable */].BASE_API_URL;
        this.token = '';
        console.log('Hello BoxProvider Provider');
    }
    BoxProvider.prototype.getBox = function (page, q) {
        var _this = this;
        if (page === void 0) { page = 0; }
        if (q === void 0) { q = ''; }
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                headers.append('Access-Control-Allow-Origin', '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin');
                _this.http.get(_this.apiUrl + 'box/page/' + page + '/code/' + q, { headers: headers })
                    .subscribe(function (res) {
                    console.log(res);
                    resolve(res);
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    BoxProvider.prototype.getBoxNew = function (cabinet) {
        var _this = this;
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                headers.append('Access-Control-Allow-Origin', '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin');
                _this.http.get(_this.apiUrl + 'box/cabinet/' + cabinet, { headers: headers })
                    .subscribe(function (res) {
                    console.log(res);
                    resolve(res);
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    BoxProvider.prototype.postEntry = function (box, cabinet, level) {
        var _this = this;
        return this.storage.get('session').then(function (val) {
            console.log(box, cabinet, level);
            return new Promise(function (resolve, reject) {
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                headers.append('Access-Control-Allow-Origin', '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
                var listIdBox = [];
                box.forEach(function (item) {
                    if (item.value) {
                        listIdBox.push(item.id);
                    }
                });
                var data_send = { id_box: listIdBox, id_cabinet: cabinet, level: level };
                console.log(data_send);
                _this.http.post(_this.apiUrl + 'box', JSON.stringify(data_send), { headers: headers })
                    .subscribe(function (res) {
                    console.log(JSON.stringify(res));
                    resolve(res);
                }, function (err) {
                    console.log(JSON.stringify(err));
                    // console.log(JSON.stringify(this.rows));
                    reject(err);
                });
            });
        });
    };
    BoxProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClientModule */],
            __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
    ], BoxProvider);
    return BoxProvider;
}());

//# sourceMappingURL=box.js.map

/***/ }),

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BerkasProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_global__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
  Generated class for the BerkasProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var BerkasProvider = /** @class */ (function () {
    function BerkasProvider(client, http, storage) {
        this.client = client;
        this.http = http;
        this.storage = storage;
        this.apiUrl = __WEBPACK_IMPORTED_MODULE_2__app_global__["a" /* GlobalVariable */].BASE_API_URL;
        this.token = '';
        console.log('Hello BerkasProvider Provider');
    }
    BerkasProvider.prototype.getBerkas = function () {
        var _this = this;
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                headers.append('Access-Control-Allow-Origin', '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin, authorization');
                _this.http.get(_this.apiUrl + 'berkas/id_user/' + val.user.id_user, { headers: headers })
                    .subscribe(function (res) {
                    console.log(res);
                    resolve(res);
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    BerkasProvider.prototype.archiving = function (data) {
        var _this = this;
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                headers.append('Access-Control-Allow-Origin', '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
                var listIdDokumen = [];
                data.document.forEach(function (item) {
                    if (item.value) {
                        listIdDokumen.push(item.id);
                    }
                });
                var data_send = { id_dokumen: listIdDokumen, id_folder: data.id_folder, id_box: data.id_box, id_berkas: data.id_berkas };
                _this.http.post(_this.apiUrl + 'berkas', JSON.stringify(data_send), { headers: headers })
                    .subscribe(function (res) {
                    console.log(res);
                    resolve(res);
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    BerkasProvider.prototype.getBerkasAll = function () {
        var _this = this;
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                // let token = this.storage.get('token');
                // let token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6IndpZG9kbyIsImlkX3VzZXIiOiIzNSIsIm5hbWUiOiJXaWRvZG8gUmlzbmFudHlvIiwiaWRfcm9sZSI6IjMiLCJpZF9kaXZpc2lvbiI6IjEiLCJpZF9yYyI6IjEiLCJpYXQiOjE1NDk0MzUzMzcsImV4cCI6MTU1MDA0MDEzN30.aHF8R3lfyqgnffsNWtKEn7HxVoOvL0fQpthQABy9A6k';
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                headers.append('Access-Control-Allow-Origin', '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin');
                _this.http.get(_this.apiUrl + 'berkas/' + val.user.id_user, { headers: headers })
                    .subscribe(function (res) {
                    console.log(res);
                    resolve(res);
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    BerkasProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClientModule */],
            __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], BerkasProvider);
    return BerkasProvider;
}());

//# sourceMappingURL=berkas.js.map

/***/ }),

/***/ 61:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BoxProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_global__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
  Generated class for the BoxProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var BoxProvider = /** @class */ (function () {
    function BoxProvider(client, http, storage) {
        this.client = client;
        this.http = http;
        this.storage = storage;
        this.apiUrl = __WEBPACK_IMPORTED_MODULE_3__app_global__["a" /* GlobalVariable */].BASE_API_URL;
        this.token = '';
        console.log('Hello BoxProvider Provider');
    }
    BoxProvider.prototype.getBox = function (page, q) {
        var _this = this;
        if (page === void 0) { page = 0; }
        if (q === void 0) { q = ''; }
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                headers.append('Access-Control-Allow-Origin', '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin');
                _this.http.get(_this.apiUrl + 'box/page/' + page + '/code/' + q, { headers: headers })
                    .subscribe(function (res) {
                    console.log(res);
                    resolve(res);
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    BoxProvider.prototype.getBoxNew = function (cabinet) {
        var _this = this;
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                headers.append('Access-Control-Allow-Origin', '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin');
                _this.http.get(_this.apiUrl + 'box/cabinet/' + cabinet, { headers: headers })
                    .subscribe(function (res) {
                    console.log(res);
                    resolve(res);
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    BoxProvider.prototype.postEntry = function (box, cabinet, level) {
        var _this = this;
        return this.storage.get('session').then(function (val) {
            console.log(box, cabinet, level);
            return new Promise(function (resolve, reject) {
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                headers.append('Access-Control-Allow-Origin', '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
                var listIdBox = [];
                box.forEach(function (item) {
                    if (item.value) {
                        listIdBox.push(item.id);
                    }
                });
                var data_send = { id_box: listIdBox, id_cabinet: cabinet, level: level };
                console.log(data_send);
                _this.http.post(_this.apiUrl + 'box', JSON.stringify(data_send), { headers: headers })
                    .subscribe(function (res) {
                    console.log(JSON.stringify(res));
                    resolve(res);
                }, function (err) {
                    console.log(JSON.stringify(err));
                    // console.log(JSON.stringify(this.rows));
                    reject(err);
                });
            });
        });
    };
    BoxProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClientModule */],
            __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
    ], BoxProvider);
    return BoxProvider;
}());

//# sourceMappingURL=box.js.map

/***/ }),

/***/ 61:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PeminjamanProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_global__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the PeminjamanProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var PeminjamanProvider = /** @class */ (function () {
    function PeminjamanProvider(http, storage) {
        this.http = http;
        this.storage = storage;
        this.apiUrl = __WEBPACK_IMPORTED_MODULE_3__app_global__["a" /* GlobalVariable */].BASE_API_URL;
        console.log('Hello PeminjamanProvider Provider');
    }
    PeminjamanProvider.prototype.proses = function (data) {
        var _this = this;
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                headers.append('Access-Control-Allow-Origin', '*');
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
                var listIdDokumen = [];
                data.documents.forEach(function (item) {
                    if (item.value) {
                        listIdDokumen.push(item.id);
                    }
                });
                var data_send = {
                    id_dokumen: listIdDokumen,
                    name: val.user.name,
                    id_divisi: val.user.id_divisi,
                    date: data.peminjaman.date,
                    keperluan: data.peminjaman.keperluan,
                    remark: data.peminjaman.remark,
                    id_user: val.user.id_user
                };
                _this.http.post(_this.apiUrl + 'peminjaman', JSON.stringify(data_send), { headers: headers })
                    .subscribe(function (res) {
                    resolve(res);
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    PeminjamanProvider.prototype.getPeminjaman = function () {
        var _this = this;
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
                _this.http.get(_this.apiUrl + 'peminjaman/id_user/' + val.user.id_user, { headers: headers })
                    .subscribe(function (res) {
                    console.log(res);
                    resolve(res);
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    PeminjamanProvider.prototype.getPeminjamanDetail = function (id) {
        var _this = this;
        return this.storage.get('session').then(function (val) {
            return new Promise(function (resolve, reject) {
                var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json',
                    'Authorization': val.token
                });
                headers.append('Access-Control-Allow-Headers', 'Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
                _this.http.get(_this.apiUrl + 'peminjaman/id/' + id, { headers: headers })
                    .subscribe(function (res) {
                    resolve(res);
                }, function (err) {
                    reject(err);
                });
            });
        });
    };
    PeminjamanProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
    ], PeminjamanProvider);
    return PeminjamanProvider;
}());

//# sourceMappingURL=peminjaman.js.map

/***/ })

},[246]);
//# sourceMappingURL=main.js.map