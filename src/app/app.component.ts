import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// import { PemberkasanPage } from '../pages/pemberkasan/pemberkasan';
import { HomePage } from '../pages/home/home';
import { AssignFolderPage } from '../pages/assign-folder/assign-folder';
import { AssignBoxPage } from '../pages/assign-box/assign-box';
import { UnarchivePage } from '../pages/unarchive/unarchive';
import { ScanAssignFolderPage } from '../pages/scan-assign-folder/scan-assign-folder';
import { ScanAssignBoxPage } from '../pages/scan-assign-box/scan-assign-box';
import { ScanBoxPage } from '../pages/scan-box/scan-box';
import { ScanFolderPage } from '../pages/scan-folder/scan-folder';
import { FinishAssignFolderPage } from '../pages/finish-assign-folder/finish-assign-folder';
import { FinishAssignBoxPage } from '../pages/finish-assign-box/finish-assign-box';
import { DokumenPage } from '../pages/dokumen/dokumen';
import { AsignfolderPage } from '../pages/asignfolder/asignfolder';
import { Asignfolder2Page } from '../pages/asignfolder2/asignfolder2';
import { Asignfolder3Page } from '../pages/asignfolder3/asignfolder3';
import { EntryPage } from '../pages/entry/entry';
import { LemariFolderPage } from '../pages/lemari-folder/lemari-folder';
import { LoginPage } from '../pages/login/login';
import { AsignboxPage } from '../pages/asignbox/asignbox';
import { TestPage } from '../pages/test/test';
import { Asignbox2Page } from '../pages/asignbox2/asignbox2';
import { Asignbox3Page } from '../pages/asignbox3/asignbox3';
import { Entry2Page } from '../pages/entry2/entry2';
import { PemberkasanNewPage } from '../pages/pemberkasan-new/pemberkasan-new';
import { Entry3Page } from '../pages/entry3/entry3';
import { PeminjamanPage } from '../pages/peminjaman/peminjaman';
import { FormPeminjamanPage } from '../pages/form-peminjaman/form-peminjaman';
import { Entry3newPage } from '../pages/entry3new/entry3new';
import { HttpClientModule } from '@angular/common/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { FolderProvider } from '../providers/folder/folder';
import { ListDokumenNewPage } from '../pages/list-dokumen-new/list-dokumen-new';
import { ListDokumenPage } from '../pages/list-dokumen/list-dokumen';
import { ScanFolderPemberkasanPage } from '../pages/scan-folder-pemberkasan/scan-folder-pemberkasan';
import { SearchByScanPage } from '../pages/search-by-scan/search-by-scan';
import { SearchFolderPage } from '../pages/search-folder/search-folder';
// import { route } from '@angular/route';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;
   @ViewChild(Nav) nav: Nav;
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

