import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, IonicPageModule  } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { TestPage } from '../pages/test/test';
import { HttpClientModule } from '@angular/common/http';
import { Asignfolder2Page } from '../pages/asignfolder2/asignfolder2';
import { Asignfolder2aPage } from '../pages/asignfolder2a/asignfolder2a';
import { Asignbox2Page } from '../pages/asignbox2/asignbox2';
import { Asignbox2aPage } from '../pages/asignbox2a/asignbox2a';
import { Asignfolder3Page } from '../pages/asignfolder3/asignfolder3';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { EntryPage } from '../pages/entry/entry';
import { LoginPage } from '../pages/login/login';
import { AsignfolderPage } from '../pages/asignfolder/asignfolder';
import { AsignboxPage } from '../pages/asignbox/asignbox';
import { Asignbox3Page } from '../pages/asignbox3/asignbox3';
import { Entry2Page } from '../pages/entry2/entry2';
import { Entry3Page } from '../pages/entry3/entry3';
import { PemberkasanPage } from '../pages/pemberkasan/pemberkasan';
import { Entry3newPage } from '../pages/entry3new/entry3new';
import { ListDokumenPage } from '../pages/list-dokumen/list-dokumen';
import { DocumentProvider } from '../providers/document/document';
import { DokumenPage } from '../pages/dokumen/dokumen';
import { AssignBoxPage } from '../pages/assign-box/assign-box';
import { AssignFolderPage } from '../pages/assign-folder/assign-folder';
import { UnarchivePage } from '../pages/unarchive/unarchive';
import { ScanBoxPage } from '../pages/scan-box/scan-box';
import { ScanFolderPage } from '../pages/scan-folder/scan-folder';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { UserProvider } from '../providers/user/user';
import { IonicStorageModule } from '@ionic/storage';
import { ScanAssignFolderPage } from '../pages/scan-assign-folder/scan-assign-folder';
import { FolderProvider } from '../providers/folder/folder';
import { PeminjamanPage } from '../pages/peminjaman/peminjaman';
import { PeminjamanDetailPage } from '../pages/peminjaman-detail/peminjaman-detail';
import { BoxProvider } from '../providers/box/box';
import { ScanAssignBoxPage } from '../pages/scan-assign-box/scan-assign-box';
import { FinishAssignFolderPage } from '../pages/finish-assign-folder/finish-assign-folder';
import { FinishAssignBoxPage } from '../pages/finish-assign-box/finish-assign-box';
import { BerkasProvider } from '../providers/berkas/berkas';
import { ScanFolderPemberkasanPage } from '../pages/scan-folder-pemberkasan/scan-folder-pemberkasan';
import { ScanBoxPemberkasanPage } from '../pages/scan-box-pemberkasan/scan-box-pemberkasan';
import { FormPeminjamanPage } from '../pages/form-peminjaman/form-peminjaman';
import { FinishPeminjamanPage } from '../pages/finish-peminjaman/finish-peminjaman';
import { ListDokumenNewPage } from '../pages/list-dokumen-new/list-dokumen-new';
import { BerkasPage } from '../pages/berkas/berkas';
//import { FileTransfer,  FileTransferObject } from '@ionic-native/file-transfer';
import { ScanLemariPage } from '../pages/scan-lemari/scan-lemari';
import { PeminjamanProvider } from '../providers/peminjaman/peminjaman';
// import { File } from '@ionic-native/file';
import { ScanAssignLemariPage } from '../pages/scan-assign-lemari/scan-assign-lemari';
import { ListDokumenBerkasPage } from '../pages/list-dokumen-berkas/list-dokumen-berkas';
import { LemariPage } from '../pages/lemari/lemari';
import { LemariProvider } from '../providers/lemari/lemari';
import { LemariBoxPage } from '../pages/lemari-box/lemari-box';
import { ListFolderByBoxPage } from '../pages/list-folder-by-box/list-folder-by-box';
import { ListDokumenByFolderPage } from '../pages/list-dokumen-by-folder/list-dokumen-by-folder';
import { SearchByScanPage } from '../pages/search-by-scan/search-by-scan';
import { SearchFolderPage } from '../pages/search-folder/search-folder';
import { LemariFolderPage } from '../pages/lemari-folder/lemari-folder';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    EntryPage,
    Asignfolder2Page,
    Asignfolder2aPage,
    AsignfolderPage,
    AsignboxPage,
    Asignbox2Page,
    Asignbox2aPage,
    PemberkasanPage,
    ScanLemariPage,
    TestPage,
    Entry2Page,
    ScanAssignLemariPage,
    Entry3Page,
    Asignfolder3Page,
    Asignbox3Page,
    Entry3newPage,
    UnarchivePage,
    ScanBoxPage,
    ScanFolderPage,
    AssignFolderPage,
    ScanAssignFolderPage,
    AssignBoxPage,
    ListDokumenNewPage,
    ScanAssignBoxPage,
    FinishAssignBoxPage,
    FinishAssignFolderPage,
    DokumenPage,
    ListDokumenPage,
    LemariBoxPage,
    LemariFolderPage,
    ScanFolderPemberkasanPage,
    ScanBoxPemberkasanPage,
    PeminjamanPage,
    FormPeminjamanPage,
    FinishPeminjamanPage,
    BerkasPage,
    LemariPage,
    ListDokumenBerkasPage,
    PeminjamanDetailPage,
    ListFolderByBoxPage,
    ListDokumenByFolderPage,
    SearchByScanPage,
    ScanFolderPage,
    SearchFolderPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {}, {
         links: [
            { component: LoginPage, name: 'LoginPage', segment: 'login' },
            { component: HomePage, name: 'Home', segment: 'home' },
            { component: UnarchivePage, name: 'Unarchive', segment: 'unarchive' },
            { component: AssignBoxPage, name: 'Assign Box', segment: 'assign-box' },
            { component: LemariPage, name: 'Lemari', segment: 'lemari' },
            { component: AssignFolderPage, name: 'Assign Folder', segment: 'assign-folder' },
            { component: ScanLemariPage, name: 'Scan Lemari', segment: 'scanLemari' },
            { component: PemberkasanPage, name: 'Pemberkasan', segment: 'pemberkasan' },
            { component: DokumenPage, name: 'Dokumen', segment: 'dokumen' },
            { component: BerkasPage, name: 'Berkas', segment: 'berkas' },
            { component: PeminjamanPage, name: 'Peminjaman', segment: 'peminjaman' },
            { component: PeminjamanDetailPage, name: 'Detail Peminjaman', segment: 'detailPeminjaman' },
            { component: FormPeminjamanPage, name: 'Form Peminjaman', segment: 'formPeminjaman' },
            { component: ScanFolderPage, name: 'Scan Folder', segment: 'scanfolder' },
            { component: ScanAssignFolderPage, name: 'Scan Assign Folder', segment: 'scanassignfolder',defaultHistory:[AssignFolderPage] },
            { component: ScanBoxPage, name: 'Scan Box', segment: 'scanbox', defaultHistory:[UnarchivePage]},
            { component: ListDokumenPage, name: 'List Unarchive Doccument', segment: 'unarchiveDocument', defaultHistory:[PemberkasanPage]},
            { component: ScanBoxPemberkasanPage, name: 'Scan Box Pemberkasan ', segment: 'scanboxpemberkasan', defaultHistory:[PemberkasanPage]},
            { component: ScanFolderPemberkasanPage, name: 'Scan Folder Pemberkasan ', segment: 'scanfolderpemberkasan', defaultHistory:[PemberkasanPage]},
            { component: SearchByScanPage, name: 'Search By Scan', segment: 'search-by-scan' },
            { component: SearchFolderPage, name: 'Search Folder', segment: 'search-folder', defaultHistory: [SearchFolderPage]},

         ]
         
    }),

    HttpClientModule,
    IonicStorageModule.forRoot(),
    IonicPageModule.forChild(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    EntryPage,
    UnarchivePage,
    Asignfolder2Page,
    Asignfolder2aPage,
    Asignbox3Page,
    Asignfolder3Page,
    AsignfolderPage,
    AsignboxPage,
    ScanAssignLemariPage,
    Asignbox2Page,
    Asignbox2aPage,
    TestPage,
    LemariFolderPage,
    LemariBoxPage,
    ScanLemariPage,
    ListDokumenNewPage,
    Entry2Page,
    ListDokumenBerkasPage,
    Entry3Page,
    Entry3newPage,
    ScanBoxPage,
    ScanFolderPage,
    AssignFolderPage,
    ScanAssignFolderPage,
    ListDokumenPage,
    AssignBoxPage,
    ScanAssignBoxPage,
    FinishAssignFolderPage,
    FinishAssignBoxPage,
    DokumenPage,
    ScanFolderPemberkasanPage,
    ScanBoxPemberkasanPage,
    PeminjamanPage,
    FormPeminjamanPage,
    PeminjamanDetailPage,
    FinishPeminjamanPage,    
    PemberkasanPage,
    BerkasPage,
    LemariPage,
    ListDokumenByFolderPage,
    ListFolderByBoxPage,
    SearchByScanPage,
    SearchFolderPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DocumentProvider,
    BarcodeScanner,
    UserProvider,
    FolderProvider,
    BoxProvider,
    BerkasProvider,
    PeminjamanProvider,
    //FileTransfer,
    // File,
    LemariProvider
  ]
})
export class AppModule {}
