import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Asignfolder2Page } from '../asignfolder2/asignfolder2';
/**
 * Generated class for the Asignfolder2aPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-asignfolder2a',
  templateUrl: 'asignfolder2a.html',
})
export class Asignfolder2aPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  asignfolder2(){
  	this.navCtrl.push(Asignfolder2Page);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Asignfolder2aPage');
  }

}
