import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Asignfolder2aPage } from './asignfolder2a';

@NgModule({
  declarations: [
    Asignfolder2aPage,
  ],
  imports: [
    IonicPageModule.forChild(Asignfolder2aPage),
  ],
})
export class Asignfolder2aPageModule {}
