import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Asignbox2aPage } from './asignbox2a';

@NgModule({
  declarations: [
    Asignbox2aPage,
  ],
  imports: [
    IonicPageModule.forChild(Asignbox2aPage),
  ],
})
export class Asignbox2aPageModule {}
