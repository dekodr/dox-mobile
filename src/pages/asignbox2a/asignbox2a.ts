import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Asignbox2Page } from '../asignbox2/asignbox2';
/**
 * Generated class for the Asignbox2aPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-asignbox2a',
  templateUrl: 'asignbox2a.html',
})
export class Asignbox2aPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  asignbox2(){
  	this.navCtrl.push(Asignbox2Page);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad Asignbox2aPage');
  }

}
