import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Asignbox2aPage } from '../asignbox2a/asignbox2a';
import { DocumentProvider } from '../../providers/document/document';
/**
 * Generated class for the AsignboxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-asignbox',
  templateUrl: 'asignbox.html',
  providers: [DocumentProvider]
})
export class AsignboxPage {
  document : any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public DocumentProvider: DocumentProvider) {
    console.log('e');
    this.getData();
  }
  asignbox2a(){
  	this.navCtrl.push(Asignbox2aPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AsignboxPage');
  }
  getData(){
    this.DocumentProvider.getDocument().then((res)=>{
      console.log(res);
      this.document = res;
    }, (err)=>{
      console.log("Failure");
    })
   }
}
