import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AsignboxPage } from './asignbox';

@NgModule({
  declarations: [
    AsignboxPage,
  ],
  imports: [
    IonicPageModule.forChild(AsignboxPage),
  ],
})
export class AsignboxPageModule {}
