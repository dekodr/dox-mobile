import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Entry3newPage } from './entry3new';

@NgModule({
  declarations: [
    Entry3newPage,
  ],
  imports: [
    IonicPageModule.forChild(Entry3newPage),
  ],
})
export class Entry3newPageModule {}
