import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
/**
 * Generated class for the Entry3newPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-entry3new',
  templateUrl: 'entry3new.html',
})
export class Entry3newPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  homesweethome(){
  	this.navCtrl.push(HomePage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad Entry3newPage');
  }

}
