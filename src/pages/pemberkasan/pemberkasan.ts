import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { BerkasProvider } from '../../providers/berkas/berkas';
import * as $ from "jquery";
import { ListDokumenPage } from '../list-dokumen/list-dokumen';
import { HomePage } from '../home/home';
/**
 * Generated class for the PemberkasanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pemberkasan',
  templateUrl: 'pemberkasan.html',
  providers: [BerkasProvider]
})
export class PemberkasanPage {
  berkas: any;
  page=0;
  q='';
   all:any;
   loader: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public berkasProvider: BerkasProvider,
    public lc: LoadingController) {
      this.loader = this.lc.create();
      this.loader.present();
    this.getData();
  }
  getData(){
    console.log('test');
    this.berkasProvider.getBerkas().then((res)=>{

      console.log(res);
        this.berkas = res;
        this.all = res;
        this.loader.dismiss();
    }, (err)=>{
      console.log(err);
    })
  }
  openDocumentList(id){
    this.navCtrl.push(ListDokumenPage, {
      id_berkas : id
    })
  }
  ngAfterViewInit(){
    $(document).ready(function(){
        $('.info-trigger-js').click(function(e) {
        	e.stopPropagation();
				$(this).toggleClass('active');
				$('.info-overlay').toggleClass('active');
				$('.overlay-bg').toggleClass('active');
        })
    });

    $('page-pemberkasan').mouseup(function(e) 
		{
		    var container = $('.info-overlay');

		    // if the target of the click isn't the container nor a descendant of the container
		    if (!container.is(e.target) && container.has(e.target).length === 0) 
		    {
		        container.removeClass('active');
				$('.overlay-bg').removeClass('active');
				$('.info-trigger-js').removeClass('active');
		    }
		});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PemberkasanPage');
  }
  back(){
    this.navCtrl.push(HomePage)
  }
 getItems(ev: any) {
    // Reset items back to all of the items
    // this.getData();
    this.berkas = this.all;
    let res = this.berkas;
    // set val to the value of the searchbar
    const val = ev.target.value;
    // if the value is an empty string don't filter the items

    if (val && val.trim() != '') {

      this.berkas = this.berkas.filter((item) => {

        return (item.indeks.toLowerCase().indexOf(val.toLowerCase()) > -1);
        // return false;
        // return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
      // this.folders = matches;

    }
  }
  reset(){
    this.getData();
  }
}
