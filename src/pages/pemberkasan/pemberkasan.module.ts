import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PemberkasanPage } from './pemberkasan';

@NgModule({
  declarations: [
    PemberkasanPage,
  ],
  imports: [
    IonicPageModule.forChild(PemberkasanPage),
  ],
  exports:[
  	PemberkasanPage
  ]
})
export class PemberkasanPageModule {}
