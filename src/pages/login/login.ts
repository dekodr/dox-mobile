import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import { HomePage } from '../home/home';
import { UserProvider } from '../../providers';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name:'Login',
  segment:'login'
})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  account={};
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public user : UserProvider,
    public toastCtrl : ToastController,
    private storage: Storage) {
  }
  login(){
    this.user.login(this.account).then((res : { token : any, user: any})=>{

       this.storage.set('session', res);
       // this.storage.set('user', res.user);
       let toast = this.toastCtrl.create({
        message: "Selamat Datang",
        duration: 3000,
        position: 'top'
      });
       this.navCtrl.push(HomePage);
    }, (err)=>{
      let toast = this.toastCtrl.create({
        message: "Invalid Username Or Password",
        duration: 3000,
        position: 'top'
      });
      toast.present();
    });
    
  }

	ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    this.storage.get('session').then((val) => {
      if(val!=null){
        this.navCtrl.push(HomePage);
      }
      
    });
  }

}