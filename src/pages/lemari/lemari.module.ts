import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LemariPage } from './lemari';

@NgModule({
  declarations: [
    LemariPage,
  ],
  imports: [
    IonicPageModule.forChild(LemariPage),
  ],
})
export class LemariPageModule {}
