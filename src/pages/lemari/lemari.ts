import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { LemariProvider } from '../../providers/lemari/lemari';
import { LemariBoxPage } from '../lemari-box/lemari-box';

/**
 * Generated class for the LemariPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lemari',
  templateUrl: 'lemari.html',
  providers: [LemariProvider]
})
export class LemariPage {
  cupboards: any;
  loader: any;
  all: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public LemariProvider: LemariProvider,
    public lc: LoadingController) {
      this.loader = this.lc.create();
      this.loader.present();
      this.getData();
  }
  getData(){
    this.LemariProvider.getLemaribyRC().then((res)=>{
      this.cupboards = res;
      this.loader.dismiss();
    }, (err)=>{
      console.log("Failure");
    })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LemariPage');
  }
  back(){
    this.navCtrl.pop()
 }
 next(id){
   this.navCtrl.push(LemariBoxPage)
 }
 getItems(ev: any) {
  this.cupboards = this.all;
  let res = this.cupboards;
  const val = ev.target.value;

  if (val && val.trim() != '') {
    this.cupboards = this.cupboards.filter((item) => {
      return (item.code.toLowerCase().indexOf(val.toLowerCase()) > -1);
    })
  }
}
reset(){
  this.getData();
  }
}
