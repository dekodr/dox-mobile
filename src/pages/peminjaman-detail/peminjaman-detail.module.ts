import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PeminjamanDetailPage } from './peminjaman-detail';

@NgModule({
  declarations: [
    PeminjamanDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(PeminjamanDetailPage),
  ],
})
export class PeminjamanDetailPageModule {}
