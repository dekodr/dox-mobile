import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PeminjamanProvider } from '../../providers/peminjaman/peminjaman';
/**
 * Generated class for the PeminjamanDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-peminjaman-detail',
  templateUrl: 'peminjaman-detail.html',
})
export class PeminjamanDetailPage {
	  detail: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public peminjamanProvider: PeminjamanProvider) {
  	this.getData();
  }
  getData(){
    this.peminjamanProvider.getPeminjamanDetail(this.navParams.get('id')).then((res)=>{
    	console.log(res);
        this.detail = res;
    }, (err)=>{
        console.log("Failure");
    })
  }
  back(){
    this.navCtrl.pop()
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad PeminjamanDetailPage');
  }

}
