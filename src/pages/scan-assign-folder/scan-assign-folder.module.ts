import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScanAssignFolderPage } from './scan-assign-folder';

@NgModule({
  declarations: [
    ScanAssignFolderPage,
  ],
  imports: [
    IonicPageModule.forChild(ScanAssignFolderPage),
  ],
})
export class ScanAssignFolderPageModule {}
