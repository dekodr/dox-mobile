import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { BarcodeScanner, BarcodeScanResult, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { FinishAssignFolderPage } from '../finish-assign-folder/finish-assign-folder';
import { Entry3newPage} from '../entry3new/entry3new';
import { FolderProvider } from '../../providers/folder/folder';

/**
 * Generated class for the ScanAssignFolderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-scan-assign-folder',
  templateUrl: 'scan-assign-folder.html',
})
export class ScanAssignFolderPage {
  folder : any[];
  result: BarcodeScanResult;
  dataToEncode: string;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    private barcode: BarcodeScanner,
     public lc:LoadingController,
     public FolderProvider: FolderProvider) {

    this.folder = this.navParams.get('data');
  }
  
  async encodeData(){
    try{
      await this.barcode.encode(this.barcode.Encode.TEXT_TYPE, this.dataToEncode);
    }
    catch(error){
      console.error(error);
    }
  }

  async scanBarcode() {
    try{
      const options: BarcodeScannerOptions = {
        prompt: 'Point your camera at a barcode',
        torchOn: false
      }
      this.result = await this.barcode.scan(options).then((res)=>{
        
         let loading = this.lc.create();
         let data = {'folder' : this.folder, 'box': res.text}
         loading.present();
        this.FolderProvider.postEntry(data).then((result)=>{
          loading.dismiss(); 
          this.navCtrl.push(Entry3newPage);
        }, (error)=>{
         
        })
        
        return res;
      },(err)=>{
        return err
      });
    }
    catch (error) {
      console.error(error);
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ScanAssignFolderPage');
  }
  finish(){
  	this.navCtrl.push(FinishAssignFolderPage);
  }
  back(){
  	this.navCtrl.pop();
  }
}
