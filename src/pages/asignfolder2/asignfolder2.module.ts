import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Asignfolder2Page } from './asignfolder2';

@NgModule({
  declarations: [
    Asignfolder2Page,
  ],
  imports: [
    IonicPageModule.forChild(Asignfolder2Page),
  ],
})
export class Asignfolder2PageModule {}
