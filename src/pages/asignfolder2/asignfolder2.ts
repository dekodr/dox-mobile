import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Asignfolder3Page } from '../asignfolder3/asignfolder3';

/**
 * Generated class for the Asignfolder2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-asignfolder2',
  templateUrl: 'asignfolder2.html',
})
export class Asignfolder2Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  asignfolder3(){
  	this.navCtrl.push(Asignfolder3Page);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad Asignfolder2Page');
  }

}
