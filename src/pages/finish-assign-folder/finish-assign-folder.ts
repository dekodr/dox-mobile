import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

/**
 * Generated class for the FinishAssignFolderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-finish-assign-folder',
  templateUrl: 'finish-assign-folder.html',
})
export class FinishAssignFolderPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
 backToHome(){
  	this.navCtrl.push(HomePage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad FinishAssignFolderPage');
  }

}
