import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinishAssignFolderPage } from './finish-assign-folder';

@NgModule({
  declarations: [
    FinishAssignFolderPage,
  ],
  imports: [
    IonicPageModule.forChild(FinishAssignFolderPage),
  ],
})
export class FinishAssignFolderPageModule {}
