import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListFolderByBoxPage } from './list-folder-by-box';

@NgModule({
  declarations: [
    ListFolderByBoxPage,
  ],
  imports: [
    IonicPageModule.forChild(ListFolderByBoxPage),
  ],
})
export class ListFolderByBoxPageModule {}
