import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
/**
 * Generated class for the Asignfolder3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-asignfolder3',
  templateUrl: 'asignfolder3.html',
})
export class Asignfolder3Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  homesweethome(){
    this.navCtrl.push(HomePage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad Asignfolder3Page');
  }

}
