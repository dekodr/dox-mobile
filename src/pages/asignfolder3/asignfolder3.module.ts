import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Asignfolder3Page } from './asignfolder3';

@NgModule({
  declarations: [
    Asignfolder3Page,
  ],
  imports: [
    IonicPageModule.forChild(Asignfolder3Page),
  ],
})
export class Asignfolder3PageModule {}
