import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScanBoxPage } from './scan-box';

@NgModule({
  declarations: [
   ScanBoxPage,
  ],
  imports: [
    IonicPageModule.forChild(ScanBoxPage),
  ],
})
export class ScanBoxPageModule {}
