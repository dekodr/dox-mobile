import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { BarcodeScanner, BarcodeScannerOptions, BarcodeScanResult } from '@ionic-native/barcode-scanner';
import { Entry3newPage } from '../entry3new/entry3new';
import { DocumentProvider } from '../../providers/document/document';
import { UnarchivePage } from '../unarchive/unarchive';
/**
 * Generated class for the ScanBoxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-scan-box',
  templateUrl: 'scan-box.html',
})
export class ScanBoxPage {

  box: BarcodeScanResult;
  data = {'documents': {},  'folder':{}, 'box':this.box};
  dataToEncode: string;
   isCapture: boolean = true;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private barcode: BarcodeScanner,  
    public lc:LoadingController, 
    public DocumentProvider: DocumentProvider) {
     let data = this.navParams.get('data');
     if(typeof data!='undefined'){
       this.data.documents= data.documents;
       this.data.folder   = data.folder;
       // console.log(JSON.stringify(this.data));
     }else{
       this.navCtrl.push(UnarchivePage);
     }
     
  }
  
  async encodeData(){
    try{
      await this.barcode.encode(this.barcode.Encode.TEXT_TYPE, this.dataToEncode);
    }
    catch(error){
      console.error(error);
    }
  }

  async scanBarcode() {
    try{
      const options: BarcodeScannerOptions = {
        prompt: 'Point your camera at a barcode',
        torchOn: false
      }
      await this.barcode.scan(options).then((res)=>{
        this.data.box = res;
         let loading = this.lc.create();
         loading.present();
         console.log(this.data);
        this.DocumentProvider.postEntry(this.data).then((result)=>{
          loading.dismiss(); 
          this.navCtrl.push(Entry3newPage);
        }, (error)=>{
          console.log(JSON.stringify(this.data));
        })
        
        return res;
      },(err)=>{

         let loading = this.lc.create();
         loading.present();
        this.DocumentProvider.postEntry(this.data).then((result)=>{
          loading.dismiss(); 
          this.navCtrl.push(Entry3newPage);
        }, (error)=>{
          console.log(error);
        })
        return err
      });
    }
    catch (error) {
      // this.data.box.text = '1';
      this.isCapture = true;
      
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ScanBoxPage');
  }

  back(){
  	this.navCtrl.pop();
  }
  selesai2(){
    this.navCtrl.push(Entry3newPage)
  }
}
