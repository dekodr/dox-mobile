import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner, BarcodeScannerOptions, BarcodeScanResult } from '@ionic-native/barcode-scanner';
import { ScanBoxPage } from '../scan-box/scan-box';

/**
 * Generated class for the ScanFolderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-scan-folder',
  templateUrl: 'scan-folder.html',
})
export class ScanFolderPage {

  folder: BarcodeScanResult;
  data = {'documents': {}, 'folder':this.folder};
  dataToEncode: string;
  isCapture: boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, private barcode: BarcodeScanner) {

    this.data.documents = this.navParams.get('data');
    // console.log(JSON.stringify(this.data));
  }
  
  async encodeData(){
    try{
      await this.barcode.encode(this.barcode.Encode.TEXT_TYPE, this.dataToEncode);
    }
    catch(error){
      console.error(error);
    }
  }

  async scanBarcode() {
    try{
      const options: BarcodeScannerOptions = {
        prompt: 'Point your camera at a barcode',
        torchOn: false
      }
      await this.barcode.scan(options).then((res)=>{
        // this.folder = res;
        this.data.folder = res;
        this.isCapture = true;
        return res;
      },(err)=>{
        console.log('Gagal Scan');
        return err
      });
    }
    catch (error) {
      console.error(error);
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ScanFolderPage');
  }

  scanBox(){
    this.navCtrl.push(ScanBoxPage, {
        data: this.data
    });
  }
  back(){
  	this.navCtrl.pop();
  }
}
