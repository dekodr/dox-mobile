import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScanFolderPage } from './scan-folder';

@NgModule({
  declarations: [
   ScanFolderPage,
  ],
  imports: [
    IonicPageModule.forChild(ScanFolderPage),
  ],
})
export class ScanFolderPageModule {}
