import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Entry2Page } from './entry2';

@NgModule({
  declarations: [
    Entry2Page,
  ],
  imports: [
    IonicPageModule.forChild(Entry2Page),
  ],
})
export class Entry2PageModule {}
