import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Entry3Page } from '../entry3/entry3';

/**
 * Generated class for the Entry2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-entry2',
  templateUrl: 'entry2.html',
})
export class Entry2Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  entry3(){
    this.navCtrl.push(Entry3Page);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Entry2Page');
  }

}
