import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { BerkasProvider } from '../../providers/berkas/berkas';
import { ListDokumenBerkasPage } from '../list-dokumen-berkas/list-dokumen-berkas';

/**
 * Generated class for the BerkasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-berkas',
  templateUrl: 'berkas.html',
  providers: [BerkasProvider]
})
export class BerkasPage {
  archive: any;
  loader: any;
  all: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public BerkasProvider: BerkasProvider,
    public lc: LoadingController) {
      this.loader = this.lc.create();
      this.loader.present();
    this.getData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BerkasPage');
  }
  getData(){
		this.BerkasProvider.getBerkasAll().then((res)=>{
		  	console.log(res);
        this.archive = res;
        this.all = res;
        this.loader.dismiss();
		}, (err)=>{
		  	console.log("Failure");
		})
  }
  back(){
    this.navCtrl.pop()
  }
  next(id){
    this.navCtrl.push(ListDokumenBerkasPage,{
      id:id
    })
  }
  
  getItems(ev: any){
    this.archive = this.all;
    let res = this.archive;
    const val = ev.target.value;

    if(val && val.trim() != '') {
      this.archive = this.archive.filter((item) =>{
        if(item.klasifikasi != null && item.klasifikasi.toLowerCase().indexOf(val.toLowerCase()) > -1){
          return true;
        }else if(item.indeks != null && item.indeks.toLowerCase().indexOf(val.toLowerCase()) > -1){
          return true;
        }
        return false;
      })
    }else{
      this.archive = this.all;
    }
  }
  reset(){
    this.getData();
  }
}
