import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BerkasPage } from './berkas';

@NgModule({
  declarations: [
    BerkasPage,
  ],
  imports: [
    IonicPageModule.forChild(BerkasPage),
  ],
  exports: [
  	BerkasPage,
  ]
})
export class BerkasPageModule {}
