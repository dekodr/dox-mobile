import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner, BarcodeScanResult, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { Entry3newPage } from '../entry3new/entry3new';

/**
 * Generated class for the ScanAssignBoxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-scan-assign-box',
  templateUrl: 'scan-assign-box.html',
})
export class ScanAssignBoxPage {

  result: BarcodeScanResult;
  dataToEncode: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private barcode: BarcodeScanner) {}

  async encodeData(){
    try{
      await this.barcode.encode(this.barcode.Encode.TEXT_TYPE, this.dataToEncode);
    }
    catch(error){
      console.error(error);
    }
  }
  async scanBarcode() {
    try{
      const options: BarcodeScannerOptions = {
        prompt: 'Point your camera at a barcode',
        torchOn: false
      }
      this.result = await this.barcode.scan(options);
    }
    catch (error) {
      console.error(error);
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ScanAssignBoxPage');
  }
  finish(){
  	this.navCtrl.push(Entry3newPage);
  }
  back(){
  	this.navCtrl.pop();
  }
}
