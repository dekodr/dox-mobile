import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScanAssignBoxPage } from './scan-assign-box';

@NgModule({
  declarations: [
    ScanAssignBoxPage,
  ],
  imports: [
    IonicPageModule.forChild(ScanAssignBoxPage),
  ],
})
export class ScanAssignBoxPageModule {}
