import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinishEntryPage } from './finish-entry';

@NgModule({
  declarations: [
    FinishEntryPage,
  ],
  imports: [
    IonicPageModule.forChild(FinishEntryPage),
  ],
})
export class FinishEntryPageModule {}
