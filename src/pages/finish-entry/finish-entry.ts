import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

/**
 * Generated class for the FinishEntryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-finish-entry',
  templateUrl: 'finish-entry.html',
})
export class FinishEntryPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  backToHome(){
  	this.navCtrl.push(HomePage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad FinishEntryPage');
  }

}
