import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScanLemariPage } from './scan-lemari';

@NgModule({
  declarations: [
    ScanLemariPage,
  ],
  imports: [
    IonicPageModule.forChild(ScanLemariPage),
  ],
})
export class ScanLemariPageModule {}
