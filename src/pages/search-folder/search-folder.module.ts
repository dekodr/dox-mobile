import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchFolderPage } from './search-folder';

@NgModule({
  declarations: [
    SearchFolderPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchFolderPage),
  ],
})
export class SearchFolderPageModule {}
