import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Asignbox3Page } from '../asignbox3/asignbox3';
/**
 * Generated class for the Asignbox2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-asignbox2',
  templateUrl: 'asignbox2.html',
})
export class Asignbox2Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  asignbox3(){
  	this.navCtrl.push(Asignbox3Page);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Asignbox2Page');
  }

}
