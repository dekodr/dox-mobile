import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Asignbox2Page } from './asignbox2';

@NgModule({
  declarations: [
    Asignbox2Page,
  ],
  imports: [
    IonicPageModule.forChild(Asignbox2Page),
  ],
})
export class Asignbox2PageModule {}
