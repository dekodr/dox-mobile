import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { DocumentProvider } from '../../providers/document/document';
// import { FileTransfer,  FileTransferObject } from '@ionic-native/file-transfer';
// import { File } from '@ionic-native/file';
// import { GlobalVariable } from '../../app/global';
/**
 * Generated class for the DokumenPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dokumen',
  templateUrl: 'dokumen.html',
  providers: [DocumentProvider]
})
export class DokumenPage {
  documents=[];
  page=0;
  q='';
  loader: any;
  all: any;

  file_url = 'http://dox.pgnmas.co.id/assets/lampiran/dokumen_file/';
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public DocumentProvider: DocumentProvider, 
    // private transfer: FileTransfer, 
    // private file: File,
    public lc : LoadingController) {
      this.loader = this.lc.create();
      this.loader.present();
    this.getData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DokumenPage');
  }
  getData(){
		this.DocumentProvider.getAllDokumen(this.page, this.q).then((res)=>{
       Object.keys(res).forEach((value)=> {
           this.documents.push(res[value]); 
           ++this.page;
          });
          this.all = res;
        this.loader.dismiss();
		}, (err)=>{
		  	console.log("Failure");
		})
  }
  back(){
    this.navCtrl.pop()
  }
  doInfinite(infiniteScroll) {
      this.DocumentProvider.getAllDokumen(this.page, this.q).then((res)=>{
         Object.keys(res).forEach((value)=> {
          this.documents.push(res[value]); 
           ++this.page;
        });
          
           infiniteScroll.complete();
           
      }, (err)=>{
          console.log("Failure");
      })

     

  }
  // download(document){
  //   const fileTransfer: FileTransferObject = this.transfer.create();
    
  //   const url = this.file_url+document.dokumen_file;
    
  //   fileTransfer.download(url, this.file.dataDirectory + 'file.pdf').then((entry) => {
  //        console.log('download complete: ' + entry.toURL());
  //       }, (error) => {
  //         console.log(url);
  //         console.log(error);
  //       });
  //     }
      getItems(ev: any){
        this.documents = this.all;
        let res = this.documents;
        const val = ev.target.value;

        if(val && val.trim() != '') {
          this.DocumentProvider.getAllDokumen(0, val).then((res)=>{
            this.documents = [];
            this.page = 0;
            Object.keys(res).forEach((value)=> {
             this.documents.push(res[value]); 
             ++this.page;
            });
            // this.all = res;
            this.q = val;
            this.loader.dismiss();
        }, (err)=>{
            console.log("Failure");
        })
        }else{
           this.page = 0;
      this.q = '';
        }
      }
      reset(){
        this.getData();
      }
}
