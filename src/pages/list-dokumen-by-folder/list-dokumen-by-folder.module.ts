import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListDokumenByFolderPage } from './list-dokumen-by-folder';

@NgModule({
  declarations: [
    ListDokumenByFolderPage,
  ],
  imports: [
    IonicPageModule.forChild(ListDokumenByFolderPage),
  ],
})
export class ListDokumenByFolderPageModule {}
