import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { BoxProvider } from '../../providers/box/box';
import { ScanAssignLemariPage } from '../scan-assign-lemari/scan-assign-lemari';

/**
 * Generated class for the AssignBoxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-assign-box',
  templateUrl: 'assign-box.html',
  providers: [BoxProvider]
})
export class AssignBoxPage {
  boxes= [];
  all:any;
    page=0;
    q='';
    loader: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public BoxProvider: BoxProvider,
    public lc:LoadingController) {
      let loader = this.lc.create();
      loader.present();
    this.getData(loader);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AssignBoxPage');
  }
  getData(lc){
    this.BoxProvider.getBox(this.page, this.q).then((res)=>{
       Object.keys(res).forEach((value)=> {
           this.boxes.push(res[value]); 
           ++this.page;
          }); 
          this.all = res;
          // this.loader.dismiss();
           lc.dismiss();
    }, (err)=>{
        console.log("Failure");
      })
    }
    scanAssignBox(){
    this.navCtrl.push(ScanAssignLemariPage, {
        data: this.boxes
    });
  }
  back(){
  	this.navCtrl.pop();
  }
  doInfinite(infiniteScroll) {
      this.BoxProvider.getBox(this.page, this.q).then((res)=>{
        Object.keys(res).forEach((value)=> {
          this.boxes.push(res[value]); 
           ++this.page;
        });
          
        // this.page++;
           infiniteScroll.complete();
           
      }, (err)=>{
          console.log("Failure");
      })

     

  }
  getItems(ev: any) {
    // Reset items back to all of the items
    // this.getData();
    // this.folders = this.all;
    let res = this.boxes;
    // set val to the value of the searchbar
    const val = ev.target.value;
    // if the value is an empty string don't filter the items

    if (val && val.trim() != '') {
      this.BoxProvider.getBox(0, val).then((res)=>{
          this.boxes = [];
          this.page = 0;
          Object.keys(res).forEach((value)=> {
           this.boxes.push(res[value]); 
           ++this.page;
          });
          // this.all = res;
          this.q = val;
          this.loader.dismiss();
      }, (err)=>{
          console.log("Failure");
      })
      /*this.folders = this.folders.filter((item) => {

        return (item.code.toLowerCase().indexOf(val.toLowerCase()) > -1);
        // return false;
        // return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })*/
      // this.folders = matches;

    }else{
      this.page = 0;
      this.q = '';
    }
  }
  reset(){
    this.getData(this.lc);
  }
}
