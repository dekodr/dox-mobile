import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AssignBoxPage } from './assign-box';

@NgModule({
  declarations: [
    AssignBoxPage,
  ],
  imports: [
    IonicPageModule.forChild(AssignBoxPage),
  ],
})
export class AssignBoxPageModule {}
