import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AssignFolderPage } from './assign-folder';

@NgModule({
  declarations: [
    AssignFolderPage,
  ],
  imports: [
    IonicPageModule.forChild(AssignFolderPage),
  ],
})
export class AssignFolderPageModule {}
