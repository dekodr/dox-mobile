import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { FolderProvider } from '../../providers/folder/folder';
import { ScanAssignFolderPage } from '../scan-assign-folder/scan-assign-folder';

/**
 * Generated class for the AssignFolderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-assign-folder',
  templateUrl: 'assign-folder.html',
  providers: [FolderProvider]
})
export class AssignFolderPage {
    folders=[];
    all:any;
    page=0;
    q='';
    loader: any;
    constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public FolderProvider: FolderProvider,
      public lc:LoadingController) {
        this.loader = this.lc.create();
        this.loader.present();
      this.getData();
    }

    ionViewDidLoad() {
      console.log('ionViewDidLoad AssignFolderPage');
    }
    getData(){
      this.FolderProvider.getFolder(this.page, this.q).then((res)=>{
          
          Object.keys(res).forEach((value)=> {
           this.folders.push(res[value]); 
           ++this.page;
          });
          this.all = res;
          this.loader.dismiss();
      }, (err)=>{
          console.log("Failure");
      })
    }
    doInfinite(infiniteScroll) {
      this.FolderProvider.getFolder(this.page, this.q).then((res)=>{
        Object.keys(res).forEach((value)=> {
          this.folders.push(res[value]); 
           ++this.page;
        });
          
        // this.page++;
           infiniteScroll.complete();
           
      }, (err)=>{
          console.log("Failure");
      })

     

  }
    scanAssignFolder(){
    this.navCtrl.push(ScanAssignFolderPage, {
        data: this.folders
    });
  }
  back(){
  	this.navCtrl.pop();
  }
  getItems(ev: any) {
    // Reset items back to all of the items
    // this.getData();
    // this.folders = this.all;
    let res = this.folders;
    // set val to the value of the searchbar
    const val = ev.target.value;
    // if the value is an empty string don't filter the items

    if (val && val.trim() != '') {
      this.FolderProvider.getFolder(0, val).then((res)=>{
          this.folders = [];
          this.page = 0;
          Object.keys(res).forEach((value)=> {
           this.folders.push(res[value]); 
           ++this.page;
          });
          // this.all = res;
          this.q = val;
          this.loader.dismiss();
      }, (err)=>{
          console.log("Failure");
      })
      /*this.folders = this.folders.filter((item) => {

        return (item.code.toLowerCase().indexOf(val.toLowerCase()) > -1);
        // return false;
        // return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })*/
      // this.folders = matches;

    }else{
      this.page = 0;
      this.q = '';
    }
  }
  reset(){
    this.getData();
  }
}
