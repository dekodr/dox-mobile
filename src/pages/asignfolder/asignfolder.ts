import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Asignfolder2aPage } from '../asignfolder2a/asignfolder2a';

/**
 * Generated class for the AsignfolderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-asignfolder',
  templateUrl: 'asignfolder.html',
})
export class AsignfolderPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  asignfolder2a(){
  	this.navCtrl.push(Asignfolder2aPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AsignfolderPage');
  }

}
