import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AsignfolderPage } from './asignfolder';

@NgModule({
  declarations: [
    AsignfolderPage,
  ],
  imports: [
    IonicPageModule.forChild(AsignfolderPage),
  ],
})
export class AsignfolderPageModule {}
