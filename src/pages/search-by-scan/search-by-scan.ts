import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner, BarcodeScanResult, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { HomePage } from '../home/home';

/**
 * Generated class for the SearchByScanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-by-scan',
  templateUrl: 'search-by-scan.html',
})
export class SearchByScanPage {

  result: BarcodeScanResult;
  dataToEncode: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private barcode: BarcodeScanner) {}

  async encodeData(){
    try{
      await this.barcode.encode(this.barcode.Encode.TEXT_TYPE, this.dataToEncode);
    }
    catch(error){
      console.error(error);
    }
  }

  async scanBarcode() {
    try{
      const options: BarcodeScannerOptions = {
        prompt: 'Point your camera at a barcode',
        torchOn: false
      }
      this.result = await this.barcode.scan(options);
    }
    catch (error) {
      console.error(error);
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchByScanPage');
  }

  finish(){
  	this.navCtrl.push(HomePage);
  }

  back(){
  	this.navCtrl.pop();
  }
}
