import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchByScanPage } from './search-by-scan';

@NgModule({
  declarations: [
    SearchByScanPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchByScanPage),
  ],
})
export class SearchByScanPageModule {}
