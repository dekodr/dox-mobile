import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArchiveFolderPage } from './archive-folder';

@NgModule({
  declarations: [
    ArchiveFolderPage,
  ],
  imports: [
    IonicPageModule.forChild(ArchiveFolderPage),
  ],
})
export class ArchiveFolderPageModule {}
