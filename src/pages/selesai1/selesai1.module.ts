import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Selesai1Page } from './selesai1';

@NgModule({
  declarations: [
    Selesai1Page,
  ],
  imports: [
    IonicPageModule.forChild(Selesai1Page),
  ],
})
export class Selesai1PageModule {}
