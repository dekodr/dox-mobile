import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinishPeminjamanPage } from './finish-peminjaman';

@NgModule({
  declarations: [
    FinishPeminjamanPage,
  ],
  imports: [
    IonicPageModule.forChild(FinishPeminjamanPage),
  ],
})
export class FinishPeminjamanPageModule {}
