import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { PemberkasanPageModule } from '../pemberkasan/pemberkasan.module';
import { LoginPageModule } from '../login/login.module';



@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    PemberkasanPageModule
  ],
  exports: [
  	HomePage,
  ]
})
export class HomePageModule {}
