import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner, BarcodeScanResult, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { UnarchivePage } from '../unarchive/unarchive';
import { AssignFolderPage } from '../assign-folder/assign-folder';
import { AssignBoxPage } from '../assign-box/assign-box';
import { Storage } from '@ionic/storage';
import { DokumenPage } from '../dokumen/dokumen';
import { BerkasPage } from '../berkas/berkas';
import { PemberkasanPage } from '../pemberkasan/pemberkasan';
import { PeminjamanPage } from '../peminjaman/peminjaman';
import { LoginPage } from '../login/login';
import { ScanLemariPage } from '../scan-lemari/scan-lemari';
import { LemariPage } from '../lemari/lemari';
import { LemariBoxPage } from '../lemari-box/lemari-box';
import { SearchByScanPage } from '../search-by-scan/search-by-scan';
import { SearchFolderPage } from '../search-folder/search-folder';


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  private user : any;
  result: BarcodeScanResult;
  dataToEncode: string;

  constructor(public navCtrl: NavController,
              private storage: Storage, 
              public navParams: NavParams, 
              private barcode: BarcodeScanner) {
                
    this.storage.get('session').then((val) => {
      
       if(val==null){
         this.navCtrl.push(LoginPage);
       }else{
         console.log(val);
         this.user = val.user;
       }

    });
  }
  async encodeData(){
    try{
      await this.barcode.encode(this.barcode.Encode.TEXT_TYPE, this.dataToEncode);
    }
    catch(error){
      console.error(error);
    }
  }
  async scanBarcode() {
    try{
      const options: BarcodeScannerOptions = {
        prompt: 'Point your camera at a barcode',
        torchOn: false
      }
      this.result = await this.barcode.scan(options).then((res)=>{
        
         this.navCtrl.push(LemariBoxPage, {
        lemari: res.text
      })
        
        return res;
      },(err)=>{
        return err
      });
      
    }
    catch (error) {
      console.error(error);
    }
  }
  lemari(){
   this.scanBarcode();
  } 
  search(){
  	this.navCtrl.push(SearchByScanPage);
  }
  entry(){
  	this.navCtrl.push(UnarchivePage);
  }
  asignfolder(){
  	this.navCtrl.push(AssignFolderPage);
  }
  asignbox(){
  	this.navCtrl.push(AssignBoxPage);
  }
  pemberkasan(){
  	this.navCtrl.push(PemberkasanPage);
  }
  dokumen(){
  	this.navCtrl.push(DokumenPage);
  }
  berkas(){
  	this.navCtrl.push(BerkasPage);
  }
  peminjaman(){
  	this.navCtrl.push(PeminjamanPage);
  }
  searchfolder(){
    this.navCtrl.push(SearchFolderPage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }
  logout(){
    this.storage.clear()
      this.navCtrl.push(LoginPage);
  }
}
