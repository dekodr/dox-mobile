import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { DocumentProvider } from '../../providers/document/document';
import { ScanBoxPage } from '../scan-box/scan-box';
import { ScanFolderPage } from '../scan-folder/scan-folder';
/**
 * Generated class for the UnarchivePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-unarchive',
  templateUrl: 'unarchive.html',
   providers: [DocumentProvider]
})
export class UnarchivePage {
	documents=[];
  page=0;
  q='';
  all:any;
  loader: any;
  	constructor(
      public navCtrl: NavController, 
      public navParams: NavParams, 
      public DocumentProvider: DocumentProvider,
      public lc:LoadingController) {
        this.loader = this.lc.create();
        this.loader.present();
  		this.getData();
  	}
  	ionViewDidLoad() {
    	console.log('ionViewDidLoad UnarchivePage');
  	}
  	scanFolder(document){
      // console.log(JSON.stringify(this.documents));
  		this.navCtrl.push(ScanFolderPage, {
      		data: this.documents
    	});
  	}
     doInfinite(infiniteScroll) {
      this.DocumentProvider.getUnarchive(this.page,this.q).then((res)=>{
        Object.keys(res).forEach((value)=> {
          this.documents.push(res[value]); 
           ++this.page;
        });
          
        this.page++;
           infiniteScroll.complete();
           
      }, (err)=>{
          console.log("Failure");
      })
    }
  	getData(){
  		this.DocumentProvider.getUnarchive(this.page,this.q).then((res)=>{
  		  	Object.keys(res).forEach((value)=> {
            this.documents.push(res[value]); 
            ++this.page;
          });
          this.loader.dismiss();
  		}, (err)=>{
  		  	console.log("Failure");
  		})
    }
	  back(){
		this.navCtrl.pop();

	}
  getItems(ev: any) {
    // Reset items back to all of the items
    // this.getData();
    this.documents = this.all;
    let res = this.documents;
    // set val to the value of the searchbar
    const val = ev.target.value;
    // if the value is an empty string don't filter the items

    if (val && val.trim() != '') {
       this.documents = [];
            this.page = 0;
      this.DocumentProvider.getUnarchive(this.page,val).then((res)=>{
        
            Object.keys(res).forEach((value)=> {
             this.documents.push(res[value]); 
             ++this.page;
            });
            // this.all = res;
            this.q = val;
          this.loader.dismiss();
      }, (err)=>{
          console.log("Failure");
      })
      /*this.documents = this.documents.filter((item) => {
        if(item.indeks !=null && item.indeks.toLowerCase().indexOf(val.toLowerCase()) > -1){
          return true;
         }else if(item.no !=null && item.no.toLowerCase().indexOf(val.toLowerCase()) > -1){
           return true;
         }else if(item.uraian !=null && item.uraian.toLowerCase().indexOf(val.toLowerCase()) > -1){
           return true;
         }
         console.log(item.uraian);
         console.log(item.uraian.toLowerCase().indexOf(val.toLowerCase()));
        //  typeof item.no=== 'string'&&typeof item.uraian=== 'string'){
        //   return ( || item.uraian.toLowerCase().indexOf(val.toLowerCase()) > -1||item.indeks.toLowerCase().indexOf(val.toLowerCase()) > -1);
        // }// return false;
        // return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
        return false;
      })*/
      // this.folders = matches;

    }else{
      this.documents = [];
      this.page = 0;
      this.q = '';
      this.getData();
    }
  }
  reset(){
    this.getData();
  }
}
