import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Entry2Page } from '../entry2/entry2';
import { DocumentProvider } from '../../providers/document/document';
/**
 * Generated class for the EntryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-entry',
  templateUrl: 'entry.html',
  providers: [DocumentProvider]
})
export class EntryPage {
  document : any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public DocumentProvider: DocumentProvider) {
    this.getData();
  }

  openPage(page){
    this.navCtrl.push(page);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad EntryPage');
  }

  getData(){
    this.DocumentProvider.getDocument().then((res)=>{
      console.log(res);
      this.document = res;
    }, (err)=>{
      console.log("Failure");
    })
  }
}
