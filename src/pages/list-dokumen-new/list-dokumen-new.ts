import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DocumentProvider } from '../../providers/document/document';
import { PeminjamanProvider } from '../../providers/peminjaman/peminjaman';
import { FinishPeminjamanPage } from '../finish-peminjaman/finish-peminjaman';



/**
 * Generated class for the ListDokumenNewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-dokumen-new',
  templateUrl: 'list-dokumen-new.html',
  providers: [DocumentProvider]
})
export class ListDokumenNewPage {
  documents: any;
  all: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public DocumentProvider: DocumentProvider, public peminjamanProvider:PeminjamanProvider) {
    this.getData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListDokumenNewPage');
  }
  getData(){
		this.DocumentProvider.getDocumentAll().then((res)=>{
		  	console.log(res);
		  	this.documents = res;
        this.all = res;
		}, (err)=>{
		  	console.log("Failure");
		})
  }
  back(){
    this.navCtrl.pop()
  }
  submit(){
    let data = {
      peminjaman: this.navParams.get('peminjaman'),
      documents:this.documents
    }
    
    this.peminjamanProvider.proses(data).then((res)=>{
      this.navCtrl.push(FinishPeminjamanPage)
    }, (err)=>{

    })
    
  }
  getItems(ev: any) {
    // Reset items back to all of the items
    // this.getData();
    this.documents = this.all;
    let res = this.documents;
    // set val to the value of the searchbar
    const val = ev.target.value;
    // if the value is an empty string don't filter the items

    if (val && val.trim() != '') {

      this.documents = this.documents.filter((item) => {

        if(item.keterangan !=null && item.keterangan.toLowerCase().indexOf(val.toLowerCase()) > -1){
          return true;
         }else if(item.no !=null && item.no.toLowerCase().indexOf(val.toLowerCase()) > -1){
           return true;
         }else if(item.uraian !=null && item.uraian.toLowerCase().indexOf(val.toLowerCase()) > -1){
           return true;
         }
         console.log(item.uraian);
         console.log(item.uraian.toLowerCase().indexOf(val.toLowerCase()));
        //  typeof item.no=== 'string'&&typeof item.uraian=== 'string'){
        //   return ( || item.uraian.toLowerCase().indexOf(val.toLowerCase()) > -1||item.indeks.toLowerCase().indexOf(val.toLowerCase()) > -1);
        // }// return false;
        // return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
        return false;
        // return false;
        // return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
      // this.folders = matches;

    }
  }
  reset(){
    this.getData();
  }
}
