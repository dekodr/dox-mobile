import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListDokumenNewPage } from './list-dokumen-new';

@NgModule({
  declarations: [
    ListDokumenNewPage,
  ],
  imports: [
    IonicPageModule.forChild(ListDokumenNewPage),
  ],
  exports:[
  	ListDokumenNewPage
  ]
})
export class ListDokumenNewPageModule {}
