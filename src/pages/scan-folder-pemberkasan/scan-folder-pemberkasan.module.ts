import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScanFolderPemberkasanPage } from './scan-folder-pemberkasan';

@NgModule({
  declarations: [
    ScanFolderPemberkasanPage,
  ],
  imports: [
    IonicPageModule.forChild(ScanFolderPemberkasanPage),
  ],
})
export class ScanFolderPemberkasanPageModule {}
