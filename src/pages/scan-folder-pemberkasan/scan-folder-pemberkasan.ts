import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner, BarcodeScannerOptions, BarcodeScanResult } from '@ionic-native/barcode-scanner';
import { ScanBoxPemberkasanPage } from '../scan-box-pemberkasan/scan-box-pemberkasan';

/**
 * Generated class for the ScanFolderPemberkasanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-scan-folder-pemberkasan',
  templateUrl: 'scan-folder-pemberkasan.html',
})
export class ScanFolderPemberkasanPage {

  result: BarcodeScanResult;
  dataToEncode: string;
  isCapture: boolean = true; 
  constructor(public navCtrl: NavController, public navParams: NavParams, private barcode: BarcodeScanner) {
    // console.log(JSON.stringify(this.navParams));
  }

  async encodeData(){
    try{
      await this.barcode.encode(this.barcode.Encode.TEXT_TYPE, this.dataToEncode);
    }
    catch(error){
      console.error(error);
    }
  }

  async scanBarcode() {
    try{
      const options: BarcodeScannerOptions = {
        prompt: 'Point your camera at a barcode',
        torchOn: false
      }
      this.result = await this.barcode.scan(options).then((res)=>{
        this.isCapture = true;
        this.result = res;
        return res;
      },(err)=>{
        this.isCapture = true;
        return err
      });
    }
    catch (error) {
      console.error(error);
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ScanFolderPemberkasanPage');
  }
  back(){
  	this.navCtrl.pop();
  }
  scanBox(){
    let data = {
      id_folder: this.result.text,
      id_berkas : this.navParams.get('id_berkas'),
      document : this.navParams.get('document')
    };
    console.log(data);
    this.navCtrl.push(ScanBoxPemberkasanPage,data);
  }
  
}
