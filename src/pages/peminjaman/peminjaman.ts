import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { FormPeminjamanPage } from '../form-peminjaman/form-peminjaman';
import { PeminjamanDetailPage } from '../peminjaman-detail/peminjaman-detail';
import { HomePage } from '../home/home';
import { PeminjamanProvider } from '../../providers/peminjaman/peminjaman';

/**
 * Generated class for the PeminjamanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-peminjaman',
  templateUrl: 'peminjaman.html',
})
export class PeminjamanPage {
  peminjaman: any;
  all: any;
  loader: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public peminjamanProvider: PeminjamanProvider,
    public lc: LoadingController) {
      this.loader = this.lc.create();
      this.loader.present();
    this.getData();
  }
  getData(){
    this.peminjamanProvider.getPeminjaman().then((res)=>{
        this.peminjaman = res;
        this.all = res;
        this.loader.dismiss();
    }, (err)=>{
        console.log("Failure");
    })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad PeminjamanPage');
  }
  back(){
    this.navCtrl.push(HomePage)
  }
  form(){
    this.navCtrl.push(FormPeminjamanPage)
  }
  openDetail(id){
    this.navCtrl.push(PeminjamanDetailPage,{
      id: id
    })
  }
  getItems(ev: any){
    this.peminjaman = this.all;
    let res = this.peminjaman;
    const val = ev.target.value;

    if(val && val.trim() != '') {
      this.peminjaman = this.peminjaman.filter((item) =>{
        if(item.keperluan != null && item.keperluan.toLowerCase().indexOf(val.toLowerCase()) > -1){
          return true;
        }else if(item.date != null && item.date.toLowerCase().indexOf(val.toLowerCase()) > -1){
          return true;
        }else if(item.return_date != null && item.return_date.toLowerCase().indexOf(val.toLowerCase()) > -1){
          return true;
        }else if(item.remark != null && item.remark.toLowerCase().indexOf(val.toLowerCase()) > -1){
          return true;
        }
        return false;
      })
    }else{
      this.peminjaman = this.all;
    }
  }
  reset(){
    this.getData();
  }
}
