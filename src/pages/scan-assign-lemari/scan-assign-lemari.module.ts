import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScanAssignLemariPage } from './scan-assign-lemari';

@NgModule({
  declarations: [
    ScanAssignLemariPage,
  ],
  imports: [
    IonicPageModule.forChild(ScanAssignLemariPage),
  ],
})
export class ScanAssignLemariPageModule {}
