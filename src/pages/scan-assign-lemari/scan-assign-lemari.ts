import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController  } from 'ionic-angular';
import { BarcodeScanner, BarcodeScanResult, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { FinishAssignBoxPage } from '../finish-assign-box/finish-assign-box';
import { BoxProvider } from '../../providers/box/box';
/**
 * Generated class for the ScanAssignLemariPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-scan-assign-lemari',
  templateUrl: 'scan-assign-lemari.html',
})
export class ScanAssignLemariPage {
    
    boxes :any[];
  result: BarcodeScanResult;
  dataToEncode: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private barcode: BarcodeScanner, private alertCtrl: AlertController, private boxProvider :BoxProvider) {
   this.boxes = this.navParams.get('data');
  }
  async encodeData(){
    try{
      await this.barcode.encode(this.barcode.Encode.TEXT_TYPE, this.dataToEncode);
    }
    catch(error){
      console.error(error);
    }
  }
  async scanBarcode() {
    try{
      const options: BarcodeScannerOptions = {
        prompt: 'Point your camera at a barcode',
        torchOn: false
      }
      this.result = await this.barcode.scan(options).then((res)=>{
        this.presentPrompt(res);
        return res;
      },(err)=>{
        // this.presentPrompt(err);
        // console.log(err);
        return err
      });
      
    }
    catch (error) {
      console.error(error);
    }
  }
  presentPrompt(res) { 
    let alert = this.alertCtrl.create({
      title: 'Masukkan Level Box',
      inputs: [
        {
          name: 'level',
          placeholder: 'Level'
        }
      ],
      buttons: [
        {
          text: 'Submit',
          handler: _data => {
            // console.log(_data);
            this.boxProvider.postEntry(this.boxes, res.text, _data.level).then((result)=>{
              
              this.navCtrl.push(FinishAssignBoxPage);
            }, (err)=>{
             // console.log(JSON.stringify(err));
             return err;
            })
          }
        }
      ]
    });
    alert.present();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ScanAssignLemariPage');
  }
  finish(){
  	this.navCtrl.push(FinishAssignBoxPage);
  }
  back(){
  	this.navCtrl.pop();
  }
}
