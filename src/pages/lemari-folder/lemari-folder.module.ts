import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LemariFolderPage } from './lemari-folder';

@NgModule({
  declarations: [
    LemariFolderPage,
  ],
  imports: [
    IonicPageModule.forChild(LemariFolderPage),
  ],
})
export class LemariFolderPageModule {}
