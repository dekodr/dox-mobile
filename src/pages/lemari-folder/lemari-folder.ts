import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { FolderProvider } from '../../providers/folder/folder';

/**
 * Generated class for the LemariFolderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lemari-folder',
  templateUrl: 'lemari-folder.html',
  providers: [FolderProvider]
})
export class LemariFolderPage {
  folders: any;
  loader: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public FolderProvider: FolderProvider,
    public lc: LoadingController
    ) {
      this.loader = this.lc.create();
      this.loader.present();
      this.getData();
  }

  getData(){
    this.FolderProvider.getFolderbyIdBox().then((res)=>{
      this.folders = res;
      this.loader.dismiss();
    }, (err) =>{
      console.log("Failure");
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LemariFolderPage');
  }
  back(){
    this.navCtrl.pop()
  }
}
