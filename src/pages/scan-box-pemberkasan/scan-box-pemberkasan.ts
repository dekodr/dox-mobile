import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BerkasProvider } from '../../providers/berkas/berkas';
import { BarcodeScanner, BarcodeScannerOptions, BarcodeScanResult } from '@ionic-native/barcode-scanner';
import { Entry3newPage } from '../entry3new/entry3new';

/**
 * Generated class for the ScanBoxPemberkasanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-scan-box-pemberkasan',
  templateUrl: 'scan-box-pemberkasan.html',
})
export class ScanBoxPemberkasanPage {

  result: BarcodeScanResult;
  dataToEncode: string;
    // isCapture: boolean = true;
  constructor(public navCtrl: NavController, public navParams: NavParams, private barcode: BarcodeScanner, public berkasProvider: BerkasProvider) {
  }
  async encodeData(){
    try{
      await this.barcode.encode(this.barcode.Encode.TEXT_TYPE, this.dataToEncode);
    }
    catch(error){
      console.error(error);
    }
  }

  async scanBarcode() {
    try{
      const options: BarcodeScannerOptions = {
        prompt: 'Point your camera at a barcode',
        torchOn: false
      }
      // console.log(JSON.stringify(this.navParams));
      this.result = await this.barcode.scan(options).then((res)=>{
        let data = {
          id_box: res.text,
          id_folder: this.navParams.get('id_folder'),
          id_berkas : this.navParams.get('id_berkas'),
          document : this.navParams.get('document')
        };

        this.berkasProvider.archiving(data).then((res)=>{
           this.navCtrl.push(Entry3newPage)
        }, (err)=>{
          console.log(JSON.stringify(err));
            console.log("Failure");
        })

      },(err)=>{
        return err
      });
    }
    catch (error) {

    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ScanBoxPemberkasanPage');
  }
  back(){
  	this.navCtrl.pop();
  }
  finish(){
    
  }
}
