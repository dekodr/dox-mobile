import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScanBoxPemberkasanPage } from './scan-box-pemberkasan';

@NgModule({
  declarations: [
    ScanBoxPemberkasanPage,
  ],
  imports: [
    IonicPageModule.forChild(ScanBoxPemberkasanPage),
  ],
})
export class ScanBoxPemberkasanPageModule {}
