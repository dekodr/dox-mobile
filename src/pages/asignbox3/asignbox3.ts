import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
/**
 * Generated class for the Asignbox3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-asignbox3',
  templateUrl: 'asignbox3.html',
})
export class Asignbox3Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  homesweethomee(){
  	this.navCtrl.push(HomePage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad Asignbox3Page');
  }

}
