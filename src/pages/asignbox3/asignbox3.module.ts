import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Asignbox3Page } from './asignbox3';

@NgModule({
  declarations: [
    Asignbox3Page,
  ],
  imports: [
    IonicPageModule.forChild(Asignbox3Page),
  ],
})
export class Asignbox3PageModule {}
