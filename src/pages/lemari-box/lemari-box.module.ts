import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LemariBoxPage } from './lemari-box';

@NgModule({
  declarations: [
    LemariBoxPage,
  ],
  imports: [
    IonicPageModule.forChild(LemariBoxPage),
  ],
})
export class LemariBoxPageModule {}
