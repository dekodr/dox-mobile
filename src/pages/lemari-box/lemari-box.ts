import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { BoxProvider } from '../../providers/box/box';
import { ListFolderByBoxPage } from '../list-folder-by-box/list-folder-by-box';
/**
 * Generated class for the LemariBoxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lemari-box',
  templateUrl: 'lemari-box.html',
  providers: [BoxProvider]
})
export class LemariBoxPage {
  boxes: any;
  loader: any;
  lemari :any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public BoxProvider: BoxProvider,
    public lc: LoadingController
    ) {
   this.lemari= this.navParams.get('lemari');
      this.loader = this.lc.create();
      this.loader.present();
      this.getData();
  }
  next(id){
    this.navCtrl.push(ListFolderByBoxPage,{
      lemari: id
    });
  }
  getData(){
    console.log(this.lemari);
    this.BoxProvider.getBoxNew(this.lemari).then((res)=>{
      this.boxes = res;
      this.loader.dismiss();
    },(err)=>{
      console.log(JSON.stringify(err)); 
      console.log("Failure");
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LemariBoxPage');
  }
  back(){
    this.navCtrl.pop()
  }
  next(id){
    this.navCtrl.push(LemariFolderPage)
  }
}
