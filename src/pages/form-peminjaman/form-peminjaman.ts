import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FinishPeminjamanPage } from '../finish-peminjaman/finish-peminjaman';
import { ListDokumenNewPage } from '../list-dokumen-new/list-dokumen-new';

/**
 * Generated class for the FormPeminjamanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form-peminjaman',
  templateUrl: 'form-peminjaman.html',
})
export class FormPeminjamanPage {
  peminjaman= {};
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormPeminjamanPage');
  }
  back(){
    this.navCtrl.pop()
  }
  submit(){
    this.navCtrl.push(FinishPeminjamanPage)
  }
  next(){
    this.navCtrl.push(ListDokumenNewPage,{
      peminjaman : this.peminjaman
    })
  }
}
