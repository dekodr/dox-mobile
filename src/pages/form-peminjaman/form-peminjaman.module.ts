import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormPeminjamanPage } from './form-peminjaman';

@NgModule({
  declarations: [
    FormPeminjamanPage,
  ],
  imports: [
    IonicPageModule.forChild(FormPeminjamanPage),
  ],
})
export class FormPeminjamanPageModule {}
