import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListDokumenBerkasPage } from './list-dokumen-berkas';

@NgModule({
  declarations: [
    ListDokumenBerkasPage,
  ],
  imports: [
    IonicPageModule.forChild(ListDokumenBerkasPage),
  ],
})
export class ListDokumenBerkasPageModule {}
