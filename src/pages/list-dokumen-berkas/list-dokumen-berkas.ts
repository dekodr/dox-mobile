import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { DocumentProvider } from '../../providers/document/document';
// import { FileTransfer,  FileTransferObject } from '@ionic-native/file-transfer';
//import { File } from '@ionic-native/file';

/**
 * Generated class for the ListDokumenBerkasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-dokumen-berkas',
  templateUrl: 'list-dokumen-berkas.html',
  providers: [DocumentProvider]
})
export class ListDokumenBerkasPage {
  documents: any;
  Loader: any;
  file_url = 'http://dox.pgnmas.co.id/assets/lampiran/dokumen_file/';
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public DocumentProvider: DocumentProvider, 
    // private transfer: FileTransfer, 
    // private file: File,
    public lc: LoadingController) {
      this.Loader = this.lc.create();
      this.Loader.present();
    this.getData();
  }
  getData(){

		this.DocumentProvider.getListDokumenByBerkas(this.navParams.get('id')).then((res)=>{
		  	console.log(res);
        this.documents = res;
        this.Loader.dismiss();
		}, (err)=>{
		  	console.log("Failure");
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListDokumenBerkasPage');
  }
  back(){
  this.navCtrl.pop()
  }
  download(document){
    // const fileTransfer: FileTransferObject = this.transfer.create();
    
    // const url = this.file_url+document.dokumen_file;
    
    // fileTransfer.download(url, this.file.dataDirectory + 'file.pdf').then((entry) => {
    //      console.log('download complete: ' + entry.toURL());
    //     }, (error) => {
    //       console.log(url);
    //       console.log(error);
    //     });
      }
}
