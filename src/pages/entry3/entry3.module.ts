import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Entry3Page } from './entry3';

@NgModule({
  declarations: [
    Entry3Page,
  ],
  imports: [
    IonicPageModule.forChild(Entry3Page),
  ],
})
export class Entry3PageModule {}
