import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner, BarcodeScannerOptions, BarcodeScanResult } from '@ionic-native/barcode-scanner';
import { EntryPage } from '../entry/entry';

@IonicPage()
@Component({
  selector: 'page-entry3',
  templateUrl: 'entry3.html',
})
export class Entry3Page {

  result: BarcodeScanResult;
  dataToEncode: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private barcode: BarcodeScanner) {}
  
  async encodeData(){
    try{
      await this.barcode.encode(this.barcode.Encode.TEXT_TYPE, this.dataToEncode);
    }
    catch(error){
      console.error(error);
    }
  }

  async scanBarcode() {
    try{
      const options: BarcodeScannerOptions = {
        prompt: 'Point your camera at a barcode',
        torchOn: true
      }
      this.result = await this.barcode.scan(options);
    }
    catch (error) {
      console.error(error);
    }
  }

  entry(){
  	this.navCtrl.push(EntryPage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad Entry3Page');
  }

}
