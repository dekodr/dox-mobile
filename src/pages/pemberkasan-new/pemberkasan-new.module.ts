import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PemberkasanNewPage } from './pemberkasan-new';

@NgModule({
  declarations: [
    PemberkasanNewPage,
  ],
  imports: [
    IonicPageModule.forChild(PemberkasanNewPage),
  ],
})
export class PemberkasanNewPageModule {}
