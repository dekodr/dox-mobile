import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PemberkasanPage } from '../pemberkasan/pemberkasan';
import { FolderProvider } from '../../providers/folder/folder';
/**
 * Generated class for the PemberkasanNewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pemberkasan-new',
  templateUrl: 'pemberkasan-new.html',
  providers: [FolderProvider]
})
export class PemberkasanNewPage {
  folders :any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public folder : FolderProvider) {
  }
  pemberkasan(id){
  	this.navCtrl.push(PemberkasanPage,{id : id});
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad PemberkasanNewPage');
    this.getData();
  }
  getData(){
    this.folder.getFolder().then((res)=>{
        console.log(res);
        this.folders = res;
    }, (err)=>{
        console.log("Failure");
    })
  }
}
