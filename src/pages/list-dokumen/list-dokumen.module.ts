import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListDokumenPage } from './list-dokumen';

@NgModule({
  declarations: [
    ListDokumenPage,
  ],
  imports: [
    IonicPageModule.forChild(ListDokumenPage),
  ],
})
export class ListDokumenPageModule {}
