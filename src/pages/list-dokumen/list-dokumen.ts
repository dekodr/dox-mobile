import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { DocumentProvider } from '../../providers/document/document';
import { ScanFolderPemberkasanPage } from '../scan-folder-pemberkasan/scan-folder-pemberkasan';

/**
 * Generated class for the ListDokumenPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-dokumen',
  templateUrl: 'list-dokumen.html',
  providers: [DocumentProvider]
})
export class ListDokumenPage {
	documents=[];
  all: any;
  page=0;
    q='';
  loader: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public DocumentProvider: DocumentProvider,
    public lc : LoadingController) {
      this.loader = this.lc.create();
      this.loader.present();
    this.getData();

  }
  getData(){

		this.DocumentProvider.getListDokumen(this.page, this.q).then((res)=>{
		  	console.log(res);
		  	Object.keys(res).forEach((value)=> {
           this.documents.push(res[value]); 
           ++this.page;
          });
          this.all = res;
        this.loader.dismiss();
		}, (err)=>{
		  	console.log("Failure");
    })
  }
   doInfinite(infiniteScroll) {
      this.DocumentProvider.getListDokumen(this.page, this.q).then((res)=>{
        Object.keys(res).forEach((value)=> {
          this.documents.push(res[value]); 
           ++this.page;
        });
          
        // this.page++;
           infiniteScroll.complete();
           
      }, (err)=>{
          console.log("Failure");
      })

     

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ListDokumenPage');
  }
  scanFolder(){
    let data = {
      id_berkas : this.navParams.get('id_berkas'),
      document : this.documents
    }
    console.log(data);
    this.navCtrl.push(ScanFolderPemberkasanPage,data)
  }
  getItems(ev: any) {
    // Reset items back to all of the items
    // this.getData();
    this.documents = this.all;
    let res = this.documents;
    // set val to the value of the searchbar
    const val = ev.target.value;
    // if the value is an empty string don't filter the items

    if (val && val.trim() != '') {
      this.DocumentProvider.getListDokumen(0, val).then((res)=>{
          this.documents = [];
          this.page = 0;
          Object.keys(res).forEach((value)=> {
           this.documents.push(res[value]); 
           ++this.page;
          });
          // this.all = res;
          this.q = val;
          this.loader.dismiss();
      }, (err)=>{
          console.log("Failure");
      })
      /*this.documents = this.documents.filter((item) => {

        if(item.keterangan !=null && item.keterangan.toLowerCase().indexOf(val.toLowerCase()) > -1){
          return true;
         }else if(item.no !=null && item.no.toLowerCase().indexOf(val.toLowerCase()) > -1){
           return true;
         }else if(item.uraian !=null && item.uraian.toLowerCase().indexOf(val.toLowerCase()) > -1){
           return true;
         }
         console.log(item.uraian);
         console.log(item.uraian.toLowerCase().indexOf(val.toLowerCase()));
        //  typeof item.no=== 'string'&&typeof item.uraian=== 'string'){
        //   return ( || item.uraian.toLowerCase().indexOf(val.toLowerCase()) > -1||item.indeks.toLowerCase().indexOf(val.toLowerCase()) > -1);
        // }// return false;
        // return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
        return false;
        // return false;
        // return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
      // this.folders = matches;*/

    }else{
       this.page = 0;
      this.q = '';
    }
  }
  reset(){
    this.getData();
  }
back(){
  this.navCtrl.pop()
  }
}