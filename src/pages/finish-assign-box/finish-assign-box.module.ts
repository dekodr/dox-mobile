import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinishAssignBoxPage } from './finish-assign-box';

@NgModule({
  declarations: [
    FinishAssignBoxPage,
  ],
  imports: [
    IonicPageModule.forChild(FinishAssignBoxPage),
  ],
})
export class FinishAssignBoxPageModule {}
