import { HttpClientModule,HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { GlobalVariable } from '../../app/global';

import 'rxjs/add/operator/map';
/*
  Generated class for the DocumentProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
interface DataSend{id_dokumen?: any[],id_folder?:String, id_box?:String}
@Injectable()

export class DocumentProvider {
	apiUrl = GlobalVariable.BASE_API_URL;
	rows: DataSend[] = [];
	token = '';
	constructor(
		public client: HttpClientModule, 
		public http: HttpClient,
		private storage: Storage) {
    	console.log('Hello DocumentProvider Provider');
    	
  	}
  	getDocument(q=null) {
  		return this.storage.get('session').then((val) => {
  			return new Promise((resolve, reject) => {
			let headers = new HttpHeaders(
		        {'Content-Type'	: 	'application/json',
		         'Authorization': 	val.token
		    	}
		      );
					// headers.append('', );
		      headers.append('Access-Control-Allow-Origin' , '*');
		      headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
		      this.http.get(this.apiUrl+'dokumen/user/'+val.user.id_user, {headers: headers})
		        .subscribe(res => {
		          resolve(res);
		        }, (err) => {
		        	console.log(err);
		          reject(err);
		        })
		    });
  		})
	    
  	}
  	getDokumenByFolder(folder, q=null) {
  		return this.storage.get('session').then((val) => {
  			return new Promise((resolve, reject) => {
			let headers = new HttpHeaders(
		        {'Content-Type'	: 	'application/json',
		         'Authorization': 	val.token
		    	}
		      );
					// headers.append('', );
		      headers.append('Access-Control-Allow-Origin' , '*');
		      headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
		      this.http.get(this.apiUrl+'dokumen/id_folder/'+folder, {headers: headers})
		        .subscribe(res => {
		          resolve(res);
		        }, (err) => {
		        	console.log(err);
		          reject(err);
		        })
		    });
  		})
	    
  	}
  	getUnarchive(page, q='') {
  		return this.storage.get('session').then((val) => {
  			return new Promise((resolve, reject) => {
			let headers = new HttpHeaders(
		        {'Content-Type'	: 	'application/json',
		         'Authorization': 	val.token
		    	}
		      );
					// headers.append('', );
		      headers.append('Access-Control-Allow-Origin' , '*');
		      headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
		      let url = this.apiUrl+'newDocument/user/'+val.user.id_user+'/page/'+page;
		      if(q!=''){
		      	url += '/no/'+q;
		      	url += '/uraian/'+q;
		      }
		      this.http.get(url, {headers: headers})
		        .subscribe(res => {
		          resolve(res);
		        }, (err) => {
		        	console.log(err);
		          reject(err);
		        })
		    });
  		})
	    
  	}
  	getDocumentAll() {
  		return this.storage.get('session').then((val) => {
  			return new Promise((resolve, reject) => {
			let headers = new HttpHeaders(
		        {'Content-Type'	: 	'application/json',
		         'Authorization': 	val.token
		    	}
		      );
					// headers.append('', );
		      headers.append('Access-Control-Allow-Origin' , '*');
		      headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
		      this.http.get(this.apiUrl+'dokumen/', {headers: headers})
		        .subscribe(res => {
		          resolve(res);
		        }, (err) => {
		        	console.log(err);
		          reject(err);
		        })
		    });
  		})
	    
  	}
  	postEntry(data){
  		return this.storage.get('session').then((val) => {
  			return new Promise((resolve, reject) => {
		      	let headers = new HttpHeaders(
			        {'Content-Type'	: 	'application/json',
			         'Authorization': 	val.token
			    	}
			      );
			      headers.append('Access-Control-Allow-Origin' , '*');
			      headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
			      
		      	let listIdDokumen = [];
		      	// console.log(data);
		      	data.documents.forEach(item=>{

		      		if(item.value==true){
		      			listIdDokumen.push(item.id);
		      		}
		      	});
		      	// console.log(JSON.stringify(data.documents)) ;
		      	let data_send = {id_dokumen: listIdDokumen, id_folder: data.folder.text, id_box: data.box.text};
		      	// console.log(JSON.stringify(listIdDokumen)); 
		      	this.http.post(this.apiUrl+'dokumen', JSON.stringify(data_send), {headers: headers})
		        .subscribe(res => {
		        	// console.log(JSON.stringify(res));
		        	// console.log(JSON.stringify(this.rows));
		          resolve(res);
		        }, (err) => {
		        	// console.log(JSON.stringify(err));
		          reject(err);
		        })
		    });
  		})
  	}
		getAllDokumen(page, q){
			return this.storage.get('session').then((val) => {
  			return new Promise((resolve, reject) => {
			let headers = new HttpHeaders(
		        {'Content-Type'	: 	'application/json',
		         'Authorization': 	val.token
		    	}
		    );
					// headers.append('', );
		    headers.append('Access-Control-Allow-Origin' , '*');
		    headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
		   	let url = this.apiUrl+'dokumen/page/'+page+'/user/'+val.user.id_user;
		       if(q!=''){
		      	url += '/no/'+q;
		      	url += '/uraian/'+q;
		      	url += '/id_folder/'+q;
		      	url += '/tahun/'+q;
		      	url += '/id_berkas/'+q;
		      	url += '/name/'+q;
		      	url += '/id_box/'+q;
		      	url += '/id_cabinet/'+q;
		      }
		    this.http.get(url, {headers: headers})
		        .subscribe(res => {
		        	// console.log(res);
		          resolve(res);
		        }, (err) => {
		          reject(err);
		        })
		    });
  		})
		}
		
		getListDokumen(page, q)
		// {
			
		// 	return this.storage.get('session').then((val) => {
  		// 	return new Promise((resolve, reject) => {
		// 			// let token = this.storage.get('token');
        //   //let token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6IndpZG9kbyIsImlkX3VzZXIiOiIzNSIsIm5hbWUiOiJXaWRvZG8gUmlzbmFudHlvIiwiaWRfcm9sZSI6IjMiLCJpZF9kaXZpc2lvbiI6IjEiLCJpZF9yYyI6IjEiLCJpYXQiOjE1NDk0MzUzMzcsImV4cCI6MTU1MDA0MDEzN30.aHF8R3lfyqgnffsNWtKEn7HxVoOvL0fQpthQABy9A6k';
		//       let headers = new HttpHeaders(
		//         {'Content-Type'	: 	'application/json',
		//          'Authorization': 	val.token
		//     	}
		//       );
		// 			// headers.append('', );
		//       headers.append('Access-Control-Allow-Origin' , '*');
		//       headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
		//      	let url = this.apiUrl+'dokumen/page/'+page;
		//        if(q!=''){
		// 		url += '/no/'+q;
		// 	  	url += '/indeks/'+q;
		//       	url += '/uraian/'+q;
		//       }
		//       this.http.get(url, {headers: headers})
		//         .subscribe(res => {
		//         	console.log(res);
		//           resolve(res);
		//         }, (err) => {
		//           reject(err);
		//         })
		//     });
  		// })
		// }
		{
			return this.storage.get('session').then((val) => {
  			return new Promise((resolve, reject) => {
			let headers = new HttpHeaders(
		        {'Content-Type'	: 	'application/json',
		         'Authorization': 	val.token
		    	}
		    );
					// headers.append('', );
		    headers.append('Access-Control-Allow-Origin' , '*');
		    headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
		   	let url = this.apiUrl+'dokumen/page/'+page+'/user/'+val.user.id_user;
		       if(q!=''){
		      	url += '/no/'+q;
		      	url += '/uraian/'+q;
		      	url += '/id_folder/'+q;
		      	url += '/tahun/'+q;
		      	url += '/id_berkas/'+q;
		      	url += '/name/'+q;
		      	url += '/id_box/'+q;
		      	url += '/id_cabinet/'+q;
		      }
		    this.http.get(url, {headers: headers})
		        .subscribe(res => {
		        	// console.log(res);
		          resolve(res);
		        }, (err) => {
		          reject(err);
		        })
		    });
  		})
		}
		getListDokumenByBerkas(id){
			return this.storage.get('session').then((val) => {
  			return new Promise((resolve, reject) => {
					// let token = this.storage.get('token');
          //let token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6IndpZG9kbyIsImlkX3VzZXIiOiIzNSIsIm5hbWUiOiJXaWRvZG8gUmlzbmFudHlvIiwiaWRfcm9sZSI6IjMiLCJpZF9kaXZpc2lvbiI6IjEiLCJpZF9yYyI6IjEiLCJpYXQiOjE1NDk0MzUzMzcsImV4cCI6MTU1MDA0MDEzN30.aHF8R3lfyqgnffsNWtKEn7HxVoOvL0fQpthQABy9A6k';
		      let headers = new HttpHeaders(
		        {'Content-Type'	: 	'application/json',
		         'Authorization': 	val.token
		    	}
		      );
					// headers.append('', );
		      headers.append('Access-Control-Allow-Origin' , '*');
		      headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
		      this.http.get(this.apiUrl+'dokumen/id_berkas/'+id, {headers: headers})
		        .subscribe(res => {
		        	console.log(res);
		          resolve(res);
		        }, (err) => {
		          reject(err);
		        })
		    });
  		})
		}
	}