import { HttpClientModule,HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalVariable } from '../../app/global';
import 'rxjs/add/operator/map';
/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {
	apiUrl = GlobalVariable.BASE_API_LOGIN;
  constructor(public http: HttpClient) {
    
  }
  login(account) {
  	console.log(account);
	    return new Promise((resolve, reject) => {

		    let headers = new HttpHeaders(
		        {'Content-Type'	: 	'application/json'}
		    );
	      	this.http.post(this.apiUrl+'login', account, {headers: headers})
	        .subscribe(res => {
	          	resolve(res);
	        }, (err) => {
	          	reject(err);
	        })
	    });
  	}
}
