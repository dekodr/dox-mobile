import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalVariable } from '../../app/global';
import { Storage } from '@ionic/storage';

import 'rxjs/add/operator/map';
/*
  Generated class for the BerkasProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()

export class BerkasProvider {
  apiUrl = GlobalVariable.BASE_API_URL;

	token = '';
  constructor(
    public client: HttpClientModule,
    public http: HttpClient,
		private storage: Storage) {
      console.log('Hello BerkasProvider Provider');
  }

  getBerkas() {
    return this.storage.get('session').then((val) => {
      return new Promise((resolve, reject) => {
        let headers = new HttpHeaders(
          {'Content-Type'	: 	'application/json',
           'Authorization': 	val.token
          }
        );
        headers.append('Access-Control-Allow-Origin' , '*');
        headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin, authorization');
        this.http.get(this.apiUrl+'berkas/id_user/'+val.user.id_user, {headers: headers})
          .subscribe(res => {
            console.log(res);
            resolve(res);
          }, (err) => {
            reject(err);
          })
        });
      })
    
  }
  archiving(data){
    return this.storage.get('session').then((val) => {
      return new Promise((resolve, reject) => {
          let headers = new HttpHeaders(
            {'Content-Type'  :   'application/json',
             'Authorization':   val.token
          }
          );
          headers.append('Access-Control-Allow-Origin' , '*');
          headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
          
          let listIdDokumen = [];
          data.document.forEach(item=>{
            if(item.value){
              listIdDokumen.push(item.id);
            }
          });

          let data_send = {id_dokumen: listIdDokumen, id_folder: data.id_folder, id_box: data.id_box, id_berkas: data.id_berkas};
          this.http.post(this.apiUrl+'berkas', JSON.stringify(data_send), {headers: headers})
          .subscribe(res => {
            console.log(res);
            resolve(res);
          }, (err) => {

            reject(err);
          })
      });
    })
  }
  getBerkasAll() {
      return this.storage.get('session').then((val) => {
        return new Promise((resolve, reject) => {
          // let token = this.storage.get('token');
          // let token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6IndpZG9kbyIsImlkX3VzZXIiOiIzNSIsIm5hbWUiOiJXaWRvZG8gUmlzbmFudHlvIiwiaWRfcm9sZSI6IjMiLCJpZF9kaXZpc2lvbiI6IjEiLCJpZF9yYyI6IjEiLCJpYXQiOjE1NDk0MzUzMzcsImV4cCI6MTU1MDA0MDEzN30.aHF8R3lfyqgnffsNWtKEn7HxVoOvL0fQpthQABy9A6k';
          let headers = new HttpHeaders(
            {'Content-Type'  :   'application/json',
            'Authorization':   val.token
            }
          );
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin');
      this.http.get(this.apiUrl+'berkas/'+val.user.id_user, {headers: headers})
        .subscribe(res => {
          console.log(res);
          resolve(res);
        }, (err) => {
          reject(err);
        })
        });
    })
  }
}
