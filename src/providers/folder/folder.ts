import { HttpClientModule,HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { GlobalVariable } from '../../app/global';

import 'rxjs/add/operator/map';
/*
  Generated class for the FolderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FolderProvider {
  apiUrl = GlobalVariable.BASE_API_URL;
	token = '';
  constructor(
    public client: HttpClientModule, 
    public http: HttpClient,
    private storage: Storage) {
      console.log('Hello FolderProvider Provider');

    }
    getFolder(page=0, q='') {
      return this.storage.get('session').then((val) => {
        return new Promise((resolve, reject) => {
          // let token = this.storage.get('token');
          //let token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6IndpZG9kbyIsImlkX3VzZXIiOiIzNSIsIm5hbWUiOiJXaWRvZG8gUmlzbmFudHlvIiwiaWRfcm9sZSI6IjMiLCJpZF9kaXZpc2lvbiI6IjEiLCJpZF9yYyI6IjEiLCJpYXQiOjE1NDk0MzUzMzcsImV4cCI6MTU1MDA0MDEzN30.aHF8R3lfyqgnffsNWtKEn7HxVoOvL0fQpthQABy9A6k';
          let headers = new HttpHeaders(
            {'Content-Type'  :   'application/json',
            'Authorization':   val.token
        }
        );
        // headers.append('', );
        // headers.append('Access-Control-Allow-Origin' , '*');
        headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
        this.http.get(this.apiUrl+'folder/page/'+page+'/code/'+q, {headers: headers})
          .subscribe(res => {
            
            resolve(res);
          }, (err) => {
            reject(err);
          })
      });
    });
  }
  getFolderByBox(box, page=0, q='') {
      return this.storage.get('session').then((val) => {
        return new Promise((resolve, reject) => {
          // let token = this.storage.get('token');
          //let token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6IndpZG9kbyIsImlkX3VzZXIiOiIzNSIsIm5hbWUiOiJXaWRvZG8gUmlzbmFudHlvIiwiaWRfcm9sZSI6IjMiLCJpZF9kaXZpc2lvbiI6IjEiLCJpZF9yYyI6IjEiLCJpYXQiOjE1NDk0MzUzMzcsImV4cCI6MTU1MDA0MDEzN30.aHF8R3lfyqgnffsNWtKEn7HxVoOvL0fQpthQABy9A6k';
          let headers = new HttpHeaders(
            {'Content-Type'  :   'application/json',
            'Authorization':   val.token
        }
        );
        // headers.append('', );
        // headers.append('Access-Control-Allow-Origin' , '*');
        headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
        this.http.get(this.apiUrl+'folder/box/'+box+'/page/'+page+'/code/'+q, {headers: headers})
          .subscribe(res => {
            
            resolve(res);
          }, (err) => {
            reject(err);
          })
      });
    });
  }
  postEntry(data){
      return this.storage.get('session').then((val) => {
        return new Promise((resolve, reject) => {
            let headers = new HttpHeaders(
              {'Content-Type'  :   'application/json',
               'Authorization':   val.token
            }
            );
            headers.append('Access-Control-Allow-Origin' , '*');
            headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
            
            let listIdFolder = [];
            data.folder.forEach(item=>{
              if(item.value){
                listIdFolder.push(item.id);
              }
            });

            let data_send = {id_folder: listIdFolder, id_box: data.box};
            this.http.post(this.apiUrl+'folder', JSON.stringify(data_send), {headers: headers})
            .subscribe(res => {
              
              resolve(res);
            }, (err) => {
              // console.log(JSON.stringify(err));
              // console.log(JSON.stringify(this.rows));
              reject(err);
            })
        });
      })
    }
    getFolderbyIdBox() {
      return this.storage.get('session').then((val) => {
        return new Promise((resolve, reject) => {
          // let token = this.storage.get('token');
          //let token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6IndpZG9kbyIsImlkX3VzZXIiOiIzNSIsIm5hbWUiOiJXaWRvZG8gUmlzbmFudHlvIiwiaWRfcm9sZSI6IjMiLCJpZF9kaXZpc2lvbiI6IjEiLCJpZF9yYyI6IjEiLCJpYXQiOjE1NDk0MzUzMzcsImV4cCI6MTU1MDA0MDEzN30.aHF8R3lfyqgnffsNWtKEn7HxVoOvL0fQpthQABy9A6k';
          let headers = new HttpHeaders(
            {'Content-Type'  :   'application/json',
            'Authorization':   val.token
        }
        );
        // headers.append('', );
        // headers.append('Access-Control-Allow-Origin' , '*');
        headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
        this.http.get(this.apiUrl+'folder/box', {headers: headers})
          .subscribe(res => {
            
            resolve(res);
          }, (err) => {
            reject(err);
          })
      });
    });
  }
}
