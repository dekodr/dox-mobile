import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { GlobalVariable } from '../../app/global';

import 'rxjs/add/operator/map';
/*
  Generated class for the BoxProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BoxProvider {
  apiUrl = GlobalVariable.BASE_API_URL;
	token = '';
  constructor(
    public client: HttpClientModule, 
    public http: HttpClient,
		private storage: Storage) {
      console.log('Hello BoxProvider Provider');
  }
  getBox(page=0, q='') {
    return this.storage.get('session').then((val) => {
      return new Promise((resolve, reject) => {
        let headers = new HttpHeaders(
          {'Content-Type'	: 	'application/json',
            'Authorization': 	val.token
      }
      );
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin');
      this.http.get(this.apiUrl+'box/page/'+page+'/code/'+q, {headers: headers})
        .subscribe(res => {
          console.log(res);
          resolve(res);
        }, (err) => {
          reject(err);
        })
      });
    })
  }

  getBoxNew(cabinet) {
    return this.storage.get('session').then((val) => {
      return new Promise((resolve, reject) => {
        let headers = new HttpHeaders(
          {'Content-Type'	: 	'application/json',
            'Authorization': 	val.token
      }
      );
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin');
      this.http.get(this.apiUrl+'box/cabinet/'+cabinet, {headers: headers})
        .subscribe(res => {
          console.log(res);
          resolve(res);
        }, (err) => {
          reject(err);
        })
      });
    })
  }

  postEntry(box, cabinet, level){
      return this.storage.get('session').then((val) => {
         console.log(box, cabinet, level)
        return new Promise((resolve, reject) => {
            let headers = new HttpHeaders(
              {'Content-Type'  :   'application/json',
               'Authorization':   val.token
            }
            );
            headers.append('Access-Control-Allow-Origin' , '*');
            headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
            
            let listIdBox = [];
            box.forEach(item=>{
              if(item.value){
                listIdBox.push(item.id);
              }
            });

            let data_send = {id_box: listIdBox, id_cabinet: cabinet, level: level};
            console.log(data_send);
            this.http.post(this.apiUrl+'box', JSON.stringify(data_send), {headers: headers})
            .subscribe(res => {
              console.log(JSON.stringify(res));
              resolve(res);
            }, (err) => {
              console.log(JSON.stringify(err));
              // console.log(JSON.stringify(this.rows));
              reject(err);
            })
        });
      })
    }
}