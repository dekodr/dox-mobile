import { HttpClientModule,HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { GlobalVariable } from '../../app/global';

import 'rxjs/add/operator/map';
/*
  Generated class for the LemariProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LemariProvider {
  apiUrl = GlobalVariable.BASE_API_URL;
	token = '';
  constructor(
    public client: HttpClientModule,
    public http: HttpClient,
    private storage: Storage) {
      console.log('Hello LemariProvider Provider');
  
    }
    getLemari(){
       return this.storage.get('session').then((val) => {
        return new Promise((resolve, reject) => {
          // let token = this.storage.get('token');
          //let token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6IndpZG9kbyIsImlkX3VzZXIiOiIzNSIsIm5hbWUiOiJXaWRvZG8gUmlzbmFudHlvIiwiaWRfcm9sZSI6IjMiLCJpZF9kaXZpc2lvbiI6IjEiLCJpZF9yYyI6IjEiLCJpYXQiOjE1NDk0MzUzMzcsImV4cCI6MTU1MDA0MDEzN30.aHF8R3lfyqgnffsNWtKEn7HxVoOvL0fQpthQABy9A6k';
          let headers = new HttpHeaders(
            {'Content-Type'  :   'application/json',
            'Authorization':   val.token
        }
        );
        // headers.append('', );
        // headers.append('Access-Control-Allow-Origin' , '*');
        headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
        this.http.get(this.apiUrl+'lemari/'+val.user.id_user, {headers: headers})
          .subscribe(res => {
            
            resolve(res);
          }, (err) => {
            reject(err);
          })
      });
    });
  }
  getLemaribyRC(){
    return this.storage.get('session').then((val) => {
     return new Promise((resolve, reject) => {
       // let token = this.storage.get('token');
       //let token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6IndpZG9kbyIsImlkX3VzZXIiOiIzNSIsIm5hbWUiOiJXaWRvZG8gUmlzbmFudHlvIiwiaWRfcm9sZSI6IjMiLCJpZF9kaXZpc2lvbiI6IjEiLCJpZF9yYyI6IjEiLCJpYXQiOjE1NDk0MzUzMzcsImV4cCI6MTU1MDA0MDEzN30.aHF8R3lfyqgnffsNWtKEn7HxVoOvL0fQpthQABy9A6k';
       let headers = new HttpHeaders(
         {'Content-Type'  :   'application/json',
         'Authorization':   val.token
     }
     );
     // headers.append('', );
     // headers.append('Access-Control-Allow-Origin' , '*');
     headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
     this.http.get(this.apiUrl+'lemari/rc/1', {headers: headers})
       .subscribe(res => {
         
         resolve(res);
       }, (err) => {
         reject(err);
       })
   });
 });
}
}
