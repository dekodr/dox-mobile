import { HttpClientModule,HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { GlobalVariable } from '../../app/global';
/*
  Generated class for the PeminjamanProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PeminjamanProvider {
	apiUrl = GlobalVariable.BASE_API_URL;
  constructor(public http: HttpClient, private storage: Storage) {
    console.log('Hello PeminjamanProvider Provider');
  }
  proses(data){
  	return this.storage.get('session').then((val) => {
		return new Promise((resolve, reject) => {
	      	let headers = new HttpHeaders(
		        {'Content-Type'	: 	'application/json',
		         'Authorization': 	val.token
		    	}
		      );
		      headers.append('Access-Control-Allow-Origin' , '*');
		      headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
		      
	      	let listIdDokumen = [];
	      	data.documents.forEach(item=>{
	      		if(item.value){
	      			listIdDokumen.push(item.id);
	      		}
	      	});

	      	let data_send = {
	      		id_dokumen: listIdDokumen, 
	      		name: val.user.name, 
	      		id_divisi: val.user.id_divisi,
	      		date: data.peminjaman.date,
	      		keperluan: data.peminjaman.keperluan,
	      		remark: data.peminjaman.remark,
	      		id_user: val.user.id_user
	      	};

	      	this.http.post(this.apiUrl+'peminjaman', JSON.stringify(data_send), {headers: headers})
	        .subscribe(res => {
	          resolve(res);
	        }, (err) => {
	          reject(err);
	        })
	    });
	})

  }
   getPeminjaman() {
      return this.storage.get('session').then((val) => {
        return new Promise((resolve, reject) => {
           let headers = new HttpHeaders(
            {'Content-Type'  :   'application/json',
            'Authorization':   val.token
        }
        );

        headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
        this.http.get(this.apiUrl+'peminjaman/id_user/'+val.user.id_user, {headers: headers})
          .subscribe(res => {
            console.log(res);
            resolve(res);
          }, (err) => {
            reject(err);
          })
      });
    });
  }
  getPeminjamanDetail(id) {
      return this.storage.get('session').then((val) => {
        return new Promise((resolve, reject) => {
           let headers = new HttpHeaders(
            {'Content-Type'  :   'application/json',
            'Authorization':   val.token
        }
        );

        headers.append('Access-Control-Allow-Headers','Origin ,X-Requested-With ,Content-Type ,Accept ,Access-Control-Request-Method ,Access-Control-Allow-Origin,Authorization, Access-Control-Allow-Headers');
        this.http.get(this.apiUrl+'peminjaman/id/'+id, {headers: headers})
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          })
      });
    });
  }
}
