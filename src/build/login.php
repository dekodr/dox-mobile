<!DOCTYPE html>
<html lang="en">
<head>
    <title>DOX - Login</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    
    <link rel="stylesheet" href="../source/scss/main.css" />
    <!-- <link rel="stylesheet" href="assets/css/vendors/jquery-ui.css" /> -->
    <link rel="stylesheet" href="../source/vendors/font-awesome/css/all.css" />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

</head>
<body>

	<div class="body-wrapper is-white">
		
		<div class="bg-shape-gradient" data-aos="fade-down" data-aos-duration="1500">
			<img src="../source/img/gradient-bg.png" alt="">
		</div>

		<div class="content">
			
			<div class="header-text" data-aos="fade-down" data-aos-duration="1500">
				
				<img src="../source/img/xpert-logo.png" alt="" class="logo">

			</div>

			<div class="form-wrapper" data-aos="fade-down" data-aos-duration="1500">
				<div class="title">LOG <span>IN</span></div>

				<form action="index.php" class="form">
					<fieldset class="form-group">
						<input type="text" placeholder="Username">
						<span class="icon"><i class="fas fa-user"></i></span>
					</fieldset>
					<fieldset class="form-group">
						<input type="text" placeholder="Password">
						<span class="icon"><i class="fas fa-key"></i></span>
					</fieldset>
					<!-- <fieldset class="form-group wrap">
							<div class="checkbox-wrapper">
								<input type="checkbox">
								<span class="checkbox-caption">Remember <span class="is-bold">me</span></span>
								<a href="#">forgot <span class="is-bold">Password</span></a>
							</div>
					</fieldset> -->
					<fieldset class="form-group is-centerize">
						<button class="submit" onclick="location.href='index.php'">Login</button>
					</fieldset>
				</form>
			</div>

			<div class="footer is-transparent">
				<p>Don't have an account? <a href="#">Sign Up</a></p>
			</div>

		</div>

		<div class="bg-shape-half" data-aos="fade-up" data-aos-duration="1500">
			<img src="../source/img/half-bg.png" alt="">
		</div>

	</div>

	<?php include "_js.php" ?>
</body>

</html>