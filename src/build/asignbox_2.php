<!DOCTYPE html>
<html lang="en">
<head>
    <title>DOX - Scanner</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    
    <?php include '_style.php' ?>

    <!-- <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.2/jquery.mobile-1.4.2.min.css" /> --> 

</head>
<body>

	<div class="scanner-page" data-aos="fade-up" data-aos-duration="500">
		
		<div class="scanner-header" data-aos="fade-down" data-aos-duration="1500">
			<div class="upper">
				<a class="button-back">
					<span class="icon"><i class="fas fa-arrow-left"></i></span>
					Back
				</a>
				<div class="button-group">
					<a href="#">
						<span class="icon"><i class="fas fa-image"></i></span>
						<span class="icon"><i class="fas fa-bolt"></i></span>
					</a>
				</div>
			</div>
			<div class="under">
				<p>Point your camera towards the QR code <br> to scan</p>
			</div>
		</div>

		<div class="scanner-footer">
			<div class="upper">
				Scanning <span class="is-bold">Rak</span>
			</div>
			<div class="under">
				<button class="submit" onclick="location.href='asignbox_3.php'">Selanjutnya</button>
			</div>
		</div>

	</div>

	<?php include "_js.php" ?>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.19/jquery.touchSwipe.js"></script>

	<script>
		$(function() {      
	      //Keep track of how many swipes
	      var count=0;
	      //Enable swiping...
	      $(".slideTrigger").swipe( {
	        //Single swipe handler for left swipes
	        swipeLeft:function(event, direction, distance, duration, fingerCount) {
	          $(this).addClass('swipeleft');  
	        },
	        swipeRight:function(event, direction, distance, duration, fingerCount) {
	          $(this).removeClass('swipeleft');  
	        },
	        //Default is 75px, set to 0 for demo so any distance triggers swipe
	        threshold:75
	      });
	    });
	</script>
</body>

</html>