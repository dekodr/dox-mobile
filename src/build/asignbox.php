<!DOCTYPE html>
<html lang="en">
<head>
    <title>DOX - Asing Box</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    
    <?php include '_style.php' ?>

</head>
<body>

	<div class="body-wrapper menu-wrapper">

		<div class="content">
			
			<div class="box" data-aos="fade-down" data-aos-duration="1000">
				<div class="search-box">
					<input type="text" placeholder="Search">
					<span class="icon">
						<i class="fas fa-search"></i>
					</span>
				</div>
			</div>

			<div class="card-wrapper pull-up" data-aos="fade-up" data-aos-duration="1000">
				<ul>
					<?php for ($i=1; $i <= 10 ; $i++) { ?>
						<li class="cardSlider">
							<div class="card">
								<img src="../source/img/box-logo.png" alt="" class="logo-icon">
								<div class="title-card is-bold">
									<span>Box id. 0188</span>
									<span class="sub-title">
										PT Drakor
									</span>
								</div>
								<span class="circle-icon"></span>
							</div>
							<div class="behind">
								<span class="icon"><i class="fas fa-check"></i></span>
							</div>
						</li>
					<?php } ?>
				</ul>
			</div>

			<div class="footer">
				<button class="submit" onclick="location.href='asignbox_2.php'">Selanjutnya</button>
			</div>

		</div>

	</div>

	<?php include "_js.php" ?>
</body>

</html>