<!DOCTYPE html>
<html lang="en">
<head>
    <title>DOX - Menu</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    
    <?php include '_style.php' ?>

</head>
<body>

	<div class="body-wrapper menu-wrapper">
		
		<div class="bg-shape-yellow" data-aos="fade-down" data-aos-duration="1500">
			<img src="../source/img/yellow-bg.png" alt="">
		</div>

		<div class="content">
			
			<div class="header-text" data-aos="fade-down" data-aos-duration="1500">
				
				<span>Selamat Pagi</span>

				<br>

				<span class="is-bold">Apa yang ingin anda lakukan hari ini?</span>

			</div>

			<div class="card-wrapper">
				<div class="menu-card" onclick="location.href='entry.php'" data-aos="zoom-in-up" data-aos-duration="1000">
					<img src="../source/img/entry-logo.png" alt="" class="logo-icon">
					<div class="title-card is-bold">
						Entry
					</div>
				</div>
				<div class="menu-card" onclick="location.href='asignfolder.php'" data-aos="zoom-in-up" data-aos-duration="1000">
					<img src="../source/img/folder-logo.png" alt="" class="logo-icon">
					<div class="title-card is-bold">
						Assign Folder
					</div>
				</div>
				<div class="menu-card" onclick="location.href='asignbox.php'" data-aos="zoom-in-up" data-aos-duration="1000">
					<img src="../source/img/box-logo.png" alt="" class="logo-icon">
					<div class="title-card is-bold">
						Assign Box
					</div>
				</div>
				<div class="menu-card" onclick="location.href='pemberkasan_new.php'" data-aos="zoom-in-up" data-aos-duration="1000">
					<img src="../source/img/folder-logo.png" alt="" class="logo-icon">
					<div class="title-card is-bold">
						Pemberkasan
					</div>
				</div>
			</div>

		</div>

		<div class="bg-shape-green" data-aos="fade-left" data-aos-duration="1500">
			<img src="../source/img/green-bg.png" alt="">
		</div>

	</div>

	<?php include "_js.php" ?>
</body>

</html>