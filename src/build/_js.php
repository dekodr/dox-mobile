	<script type="text/javascript" src="../source/js/vendors/jquery-3.3.1.js">
	</script>
	<script type="text/javascript" src="../source/js/vendors/jquery-ui.js">
	</script>
	<script type="text/javascript" src="../source/js/app.js"></script>
	<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
	<script>
	  AOS.init();
	</script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.19/jquery.touchSwipe.js"></script>

	<script>
		$(function() {      
	      //Keep track of how many swipes
	      var count=0;
	      //Enable swiping...
	      $(".slideTrigger").swipe( {
	        //Single swipe handler for left swipes
	        swipeLeft:function(event, direction, distance, duration, fingerCount) {
	          $(this).addClass('swipeleft');  
	        },
	        swipeRight:function(event, direction, distance, duration, fingerCount) {
	          $(this).removeClass('swipeleft');  
	        },
	        //Default is 75px, set to 0 for demo so any distance triggers swipe
	        threshold:75
	      });
	    });
	</script>

	<script>
		$(function() {      
	      //Keep track of how many swipes
	      var count=0;
	      //Enable swiping...
	      $(".cardSlider").swipe( {
	        //Single swipe handler for left swipes
	        swipeLeft:function(event, direction, distance, duration, fingerCount) {
	          $(this).addClass('cardSwipe');  
	        },
	        swipeRight:function(event, direction, distance, duration, fingerCount) {
	          $(this).removeClass('cardSwipe');  
	        },
	        //Default is 75px, set to 0 for demo so any distance triggers swipe
	        threshold:75
	      });
	    });
	</script>
	
	