<!DOCTYPE html>
<html lang="en">
<head>
    <title>DOX - Entry</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    
    <?php include '_style.php' ?>

    <!-- <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.2/jquery.mobile-1.4.2.min.css" /> --> 

</head>
<body>

	<div class="body-wrapper menu-wrapper">

		<div class="content">
			
			<div class="box-view" data-aos="fade-up" data-aos-duration="500">
				<div class="search-box">
					<input type="text" placeholder="Search">
					<span class="icon">
						<i class="fas fa-search"></i>
					</span>
				</div>
			</div>

			<div class="list-wrapper pull-up" data-aos="fade-up" data-aos-duration="500">
				<ul>
				<?php for ($i=1; $i <= 10 ; $i++) { ?>
					<li class="slideTrigger">
						<div class="list">
							<!-- <img src="../source/img/file-logo.png" alt="" class="logo-icon"> -->
							<div class="title-list is-bold">
								<span>Title Here</span>
								<span class="sub-title">
									Any possible subtitle here
								</span>
							</div>
						</div>
						<div class="behind">
							<span class="icon"><i class="fas fa-check"></i></span>
						</div>
					</li>
				<?php } ?>
				</ul>
			</div>

			<div class="footer">
				<button class="submit">Selesai</button>
			</div>

		</div>

	</div>

	<?php include "_js.php" ?>

</body>

</html>