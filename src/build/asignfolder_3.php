<!DOCTYPE html>
<html lang="en">
<head>
    <title>DOX - Scanner</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    
    <?php include '_style.php' ?>

    <!-- <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.2/jquery.mobile-1.4.2.min.css" /> --> 

</head>

<body>

	<div class="confirmation-page">

		<div class="upper">
			
			<div class="big-card" data-aos="fade-up" data-aos-duration="1500">
				
				<img src="../source/img/folder-logo.png" alt="">
				<img src="../source/img/check-logo.png" alt="" class="check-icon">

				<div class="big-card-title">
					<span>Great !</span>
					<br>
					Folder telah ditambahkan
				</div>

			</div>

		</div>

		<div class="under">
			<button class="submit" onclick="location.href='index.php'">Selesai</button>
		</div>

	</div>
	
	<?php include "_js.php" ?>
	
</body>

</html>