<!DOCTYPE html>
<html lang="en">
<head>
    <title>DOX - Asing Box</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    
    <?php include '_style.php' ?>

</head>
<body>

	<div class="body-wrapper menu-wrapper">

		<div class="content">
			
			<div class="box" data-aos="fade-down" data-aos-duration="1000">
				<div class="search-box">
					<input type="text" placeholder="Search">
					<span class="icon">
						<i class="fas fa-search"></i>
					</span>
				</div>
			
			</div>

			<div class="card-wrapper pull-up">

				<div class="overlay-bg"></div>
				
				<ul>
					<?php for ($i=1; $i <= 10 ; $i++) { ?>
						<li>
							<div class="card info-trigger-js">
								<img src="../source/img/folder-logo.png" alt="" class="logo-icon">
								<div class="title-card is-bold">
									<span>
										Dok
										<span class="icon"><i class="fas fa-arrow-right"></i></span>
										Rak 001223
									</span>
									<span class="sub-title">
										Dokumen PT Drakor
									</span>
								</div>
							</div>
						</li>
					<?php } ?>
				</ul>

			</div>

			<div class="info-overlay">
				<ul>
					<li>
						<div class="context">
							Rak number <br>
							<span class="sub-context">From CV ai</span>
						</div>
						<div class="number">
							id. 0311
						</div>
					</li>
					<li>
						<div class="context">
							Kotak number <br>
							<span class="sub-context">From CV ai</span>
						</div>
						<div class="number">
							id. 0311
						</div>
					</li>
					<li>
						<div class="context">
							Folder number <br>
							<span class="sub-context">From CV ai</span>
						</div>
						<div class="number">
							id. 0311
						</div>
					</li>
				</ul>
				<div class="info-footer">
					<button class="submit" onclick="location.href='pemberkasan_2.php'">Pindahkan</button>
				</div>
			</div>

		</div>

	</div>

	<?php include "_js.php" ?>

	<script>
		$(document).ready(function() {
			$('.info-trigger-js').click(function(e) {
				e.stopPropagation();
				$(this).toggleClass('active');
				$('.info-overlay').toggleClass('active');
				$('.overlay-bg').toggleClass('active');
			})
		});

		$(document).mouseup(function(e) 
		{
		    var container = $('.info-overlay');

		    // if the target of the click isn't the container nor a descendant of the container
		    if (!container.is(e.target) && container.has(e.target).length === 0) 
		    {
		        container.removeClass('active');
				$('.overlay-bg').removeClass('active');
				$('.info-trigger-js').removeClass('active');
		    }
		});
	</script>
</body>

</html>